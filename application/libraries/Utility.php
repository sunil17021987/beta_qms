<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Sajal helper library
 *
 * @package     CodeIgniter
 * @subpackage  Libraries
 * @category    Utitlity Helper
 * @author      Sajal Das
 */

require_once (APPPATH.'/libraries/slim.php');

 class Utility extends Slim {
    
    public function __construct()
    {
        //parent::__construct();        
    }

    public function SlimHandleImageUpload($field, $path)
    {
        $uploadedImg=array();
        $images = $this->getImages($field);        
        if ($images == false) 
        {
            //$this->session->set_flashdata('success', '<div class="alert alert-warning"><button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>oops...there was something wrong. Plese upload image again...!</div>');
            return $uploadedImg;
        }
        else{            
            foreach ($images as $image) 
            {                
                if($image['output']['data']!='' && $image['input']['name']!='')
                {
                    $file = Slim::saveFile_admin($image['output']['data'], $image['input']['name'],FCPATH.$path."/");
                    if(isset($file))
                    {
                        array_push($uploadedImg, $path."/".$file['name']);
                    }
                }                                
            }
            return $uploadedImg;
        }
    }

    public function ShowSlimImageUploader($props)
    {
        //echo 'hey sajal! u are awesome!';
        $min=0;
        $width=0;
        $ratio='';
        $name='img';
        $uploader='';

        if(isset($props['height']))
        $height=$props['height'];

        if(isset($props['width']))
        $width=$props['width'];

        if(isset($props['ratio']))
        $ratio=$props['ratio'];

        if(isset($props['name']))
        $name=$props['name'];
        
        $dataAttr='';
        // if($width>0 && $height>0)
        // {
        //     $dataAttr.=' data-min-size="'.$width.','.$height.'"';
        // }
        
        // if($ratio!='')
        // $dataAttr.=' data-ratio="'.$ratio.'"';

        if(isset($props['imageUrl']) && $props['imageUrl']!="")
        {
            $uploader.='<style>
                .slim.'.$name.' .slim-file-hopper {
                    background:url("'.$props['imageUrl'].'")!important;
                    background-size: contain !important;
                }
            </style>';
        }
        

        $uploader.='<div class="slim '.$name.'" data-label="Drop your image here" '.$dataAttr.' >';
        $uploader.='<input type="file" name="'.$name.'[]" id="'.$name.'" multiple="multiple" />';
        $uploader.='<input type="hidden" name="'.$name.'" value="1">';
        $uploader.='</div>';

        return $uploader;
    }

    public function DeleteOldImage($oldpath)
    {
        if(strpos($oldpath, base_url()) !== false) 
        $old_img=str_replace(base_url(), '', $oldpath);
        else
        $old_img=$oldpath;						
        if(file_exists($old_img))
        unlink($old_img);
    }
 }
