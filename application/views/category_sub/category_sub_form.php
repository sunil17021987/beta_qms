<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Category_sub <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="bigint">Category Sub Main Id <?php echo form_error('category_sub_main_id') ?></label>
            <input type="text" class="form-control" name="category_sub_main_id" id="category_sub_main_id" placeholder="Category Sub Main Id" value="<?php echo $category_sub_main_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Category Sub Name <?php echo form_error('category_sub_name') ?></label>
            <input type="text" class="form-control" name="category_sub_name" id="category_sub_name" placeholder="Category Sub Name" value="<?php echo $category_sub_name; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Created By <?php echo form_error('category_sub_created_by') ?></label>
            <input type="text" class="form-control" name="category_sub_created_by" id="category_sub_created_by" placeholder="Category Sub Created By" value="<?php echo $category_sub_created_by; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Category Sub Time <?php echo form_error('category_sub_time') ?></label>
            <input type="text" class="form-control" name="category_sub_time" id="category_sub_time" placeholder="Category Sub Time" value="<?php echo $category_sub_time; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Level1 Urban <?php echo form_error('category_sub_level1_urban') ?></label>
            <input type="text" class="form-control" name="category_sub_level1_urban" id="category_sub_level1_urban" placeholder="Category Sub Level1 Urban" value="<?php echo $category_sub_level1_urban; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Level1 Rural <?php echo form_error('category_sub_level1_rural') ?></label>
            <input type="text" class="form-control" name="category_sub_level1_rural" id="category_sub_level1_rural" placeholder="Category Sub Level1 Rural" value="<?php echo $category_sub_level1_rural; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Level2 Urban <?php echo form_error('category_sub_level2_urban') ?></label>
            <input type="text" class="form-control" name="category_sub_level2_urban" id="category_sub_level2_urban" placeholder="Category Sub Level2 Urban" value="<?php echo $category_sub_level2_urban; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Level2 Rural <?php echo form_error('category_sub_level2_rural') ?></label>
            <input type="text" class="form-control" name="category_sub_level2_rural" id="category_sub_level2_rural" placeholder="Category Sub Level2 Rural" value="<?php echo $category_sub_level2_rural; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Level3 Urban <?php echo form_error('category_sub_level3_urban') ?></label>
            <input type="text" class="form-control" name="category_sub_level3_urban" id="category_sub_level3_urban" placeholder="Category Sub Level3 Urban" value="<?php echo $category_sub_level3_urban; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Level3 Rural <?php echo form_error('category_sub_level3_rural') ?></label>
            <input type="text" class="form-control" name="category_sub_level3_rural" id="category_sub_level3_rural" placeholder="Category Sub Level3 Rural" value="<?php echo $category_sub_level3_rural; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Level4 Urban <?php echo form_error('category_sub_level4_urban') ?></label>
            <input type="text" class="form-control" name="category_sub_level4_urban" id="category_sub_level4_urban" placeholder="Category Sub Level4 Urban" value="<?php echo $category_sub_level4_urban; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Level4 Rural <?php echo form_error('category_sub_level4_rural') ?></label>
            <input type="text" class="form-control" name="category_sub_level4_rural" id="category_sub_level4_rural" placeholder="Category Sub Level4 Rural" value="<?php echo $category_sub_level4_rural; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Level1 Urban Time <?php echo form_error('category_sub_level1_urban_time') ?></label>
            <input type="text" class="form-control" name="category_sub_level1_urban_time" id="category_sub_level1_urban_time" placeholder="Category Sub Level1 Urban Time" value="<?php echo $category_sub_level1_urban_time; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Level1 Rural Time <?php echo form_error('category_sub_level1_rural_time') ?></label>
            <input type="text" class="form-control" name="category_sub_level1_rural_time" id="category_sub_level1_rural_time" placeholder="Category Sub Level1 Rural Time" value="<?php echo $category_sub_level1_rural_time; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Level2 Urban Time <?php echo form_error('category_sub_level2_urban_time') ?></label>
            <input type="text" class="form-control" name="category_sub_level2_urban_time" id="category_sub_level2_urban_time" placeholder="Category Sub Level2 Urban Time" value="<?php echo $category_sub_level2_urban_time; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Level2 Rural Time <?php echo form_error('category_sub_level2_rural_time') ?></label>
            <input type="text" class="form-control" name="category_sub_level2_rural_time" id="category_sub_level2_rural_time" placeholder="Category Sub Level2 Rural Time" value="<?php echo $category_sub_level2_rural_time; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Level3 Urban Time <?php echo form_error('category_sub_level3_urban_time') ?></label>
            <input type="text" class="form-control" name="category_sub_level3_urban_time" id="category_sub_level3_urban_time" placeholder="Category Sub Level3 Urban Time" value="<?php echo $category_sub_level3_urban_time; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Level3 Rural Time <?php echo form_error('category_sub_level3_rural_time') ?></label>
            <input type="text" class="form-control" name="category_sub_level3_rural_time" id="category_sub_level3_rural_time" placeholder="Category Sub Level3 Rural Time" value="<?php echo $category_sub_level3_rural_time; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Level4 Urban Time <?php echo form_error('category_sub_level4_urban_time') ?></label>
            <input type="text" class="form-control" name="category_sub_level4_urban_time" id="category_sub_level4_urban_time" placeholder="Category Sub Level4 Urban Time" value="<?php echo $category_sub_level4_urban_time; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Level4 Rural Time <?php echo form_error('category_sub_level4_rural_time') ?></label>
            <input type="text" class="form-control" name="category_sub_level4_rural_time" id="category_sub_level4_rural_time" placeholder="Category Sub Level4 Rural Time" value="<?php echo $category_sub_level4_rural_time; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Escalate Upto <?php echo form_error('escalate_upto') ?></label>
            <input type="text" class="form-control" name="escalate_upto" id="escalate_upto" placeholder="Escalate Upto" value="<?php echo $escalate_upto; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub App <?php echo form_error('category_sub_app') ?></label>
            <input type="text" class="form-control" name="category_sub_app" id="category_sub_app" placeholder="Category Sub App" value="<?php echo $category_sub_app; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Category Sub Status <?php echo form_error('category_sub_status') ?></label>
            <input type="text" class="form-control" name="category_sub_status" id="category_sub_status" placeholder="Category Sub Status" value="<?php echo $category_sub_status; ?>" />
        </div>
	    <input type="hidden" name="category_sub_id" value="<?php echo $category_sub_id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('category_sub') ?>" class="btn btn-default">Cancel</a>
	</form>
     <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>