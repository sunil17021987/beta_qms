<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Category_sub Read          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <table class="table">
	    <tr><td><b>Category Sub Main Id</b></td><td><?php echo $category_sub_main_id; ?></td></tr>
	    <tr><td><b>Category Sub Name</b></td><td><?php echo $category_sub_name; ?></td></tr>
	    <tr><td><b>Category Sub Created By</b></td><td><?php echo $category_sub_created_by; ?></td></tr>
	    <tr><td><b>Category Sub Time</b></td><td><?php echo $category_sub_time; ?></td></tr>
	    <tr><td><b>Category Sub Level1 Urban</b></td><td><?php echo $category_sub_level1_urban; ?></td></tr>
	    <tr><td><b>Category Sub Level1 Rural</b></td><td><?php echo $category_sub_level1_rural; ?></td></tr>
	    <tr><td><b>Category Sub Level2 Urban</b></td><td><?php echo $category_sub_level2_urban; ?></td></tr>
	    <tr><td><b>Category Sub Level2 Rural</b></td><td><?php echo $category_sub_level2_rural; ?></td></tr>
	    <tr><td><b>Category Sub Level3 Urban</b></td><td><?php echo $category_sub_level3_urban; ?></td></tr>
	    <tr><td><b>Category Sub Level3 Rural</b></td><td><?php echo $category_sub_level3_rural; ?></td></tr>
	    <tr><td><b>Category Sub Level4 Urban</b></td><td><?php echo $category_sub_level4_urban; ?></td></tr>
	    <tr><td><b>Category Sub Level4 Rural</b></td><td><?php echo $category_sub_level4_rural; ?></td></tr>
	    <tr><td><b>Category Sub Level1 Urban Time</b></td><td><?php echo $category_sub_level1_urban_time; ?></td></tr>
	    <tr><td><b>Category Sub Level1 Rural Time</b></td><td><?php echo $category_sub_level1_rural_time; ?></td></tr>
	    <tr><td><b>Category Sub Level2 Urban Time</b></td><td><?php echo $category_sub_level2_urban_time; ?></td></tr>
	    <tr><td><b>Category Sub Level2 Rural Time</b></td><td><?php echo $category_sub_level2_rural_time; ?></td></tr>
	    <tr><td><b>Category Sub Level3 Urban Time</b></td><td><?php echo $category_sub_level3_urban_time; ?></td></tr>
	    <tr><td><b>Category Sub Level3 Rural Time</b></td><td><?php echo $category_sub_level3_rural_time; ?></td></tr>
	    <tr><td><b>Category Sub Level4 Urban Time</b></td><td><?php echo $category_sub_level4_urban_time; ?></td></tr>
	    <tr><td><b>Category Sub Level4 Rural Time</b></td><td><?php echo $category_sub_level4_rural_time; ?></td></tr>
	    <tr><td><b>Escalate Upto</b></td><td><?php echo $escalate_upto; ?></td></tr>
	    <tr><td><b>Category Sub App</b></td><td><?php echo $category_sub_app; ?></td></tr>
	    <tr><td><b>Category Sub Status</b></td><td><?php echo $category_sub_status; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('category_sub') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
         <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>