<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Category
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-md-4">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <!--<h2 style="margin-top:0px">Category <?php echo $button ?></h2>-->
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Category Name <?php echo form_error('category_name') ?></label>
            <input type="text" class="form-control" name="category_name" id="category_name" placeholder="Category Name" value="<?php echo $category_name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Date <?php echo form_error('created_at') ?></label>
            <input type="text" class="form-control" name="created_at" id="created_at" placeholder="Created At" value="<?php echo $created_at; ?>" />
        </div>
	    <input type="hidden" name="category_id" value="<?php echo $category_id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('category') ?>" class="btn btn-default">Cancel</a>
	</form>
   <!-- ******************/master footer ****************** -->
                    </div>
                </div>
                <!-- /.col -->
            </div>

            <!-- /.row -->
    </section>
    <!-- /.content -->
    </div>