<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Agent_performance Read          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <table class="table">
	    <tr><td><b>Agents Id</b></td><td><?php echo $agents_id; ?></td></tr>
	    <tr><td><b>Agents Msd</b></td><td><?php echo $agents_msd; ?></td></tr>
	    <tr><td><b>Date Of</b></td><td><?php echo $date_of; ?></td></tr>
	    <tr><td><b>Inbound StaffedDuration</b></td><td><?php echo $Inbound_StaffedDuration; ?></td></tr>
	    <tr><td><b>Inbound ReadyDuration</b></td><td><?php echo $Inbound_ReadyDuration; ?></td></tr>
	    <tr><td><b>Inbound BreakDuration</b></td><td><?php echo $Inbound_BreakDuration; ?></td></tr>
	    <tr><td><b>Inbound IdleTime</b></td><td><?php echo $Inbound_IdleTime; ?></td></tr>
	    <tr><td><b>Inbound HoldTime</b></td><td><?php echo $Inbound_HoldTime; ?></td></tr>
	    <tr><td><b>Inbound NumberOfcallsOffered</b></td><td><?php echo $Inbound_NumberOfcallsOffered; ?></td></tr>
	    <tr><td><b>Inbound NumberofCallanswered</b></td><td><?php echo $Inbound_NumberofCallanswered; ?></td></tr>
	    <tr><td><b>Inbound AbandonedPer</b></td><td><?php echo $Inbound_AbandonedPer; ?></td></tr>
	    <tr><td><b>Inbound AHT</b></td><td><?php echo $Inbound_AHT; ?></td></tr>
	    <tr><td><b>Outbound StaffedDuration</b></td><td><?php echo $Outbound_StaffedDuration; ?></td></tr>
	    <tr><td><b>Outbound ReadyDuration</b></td><td><?php echo $Outbound_ReadyDuration; ?></td></tr>
	    <tr><td><b>Outbound BreakDuration</b></td><td><?php echo $Outbound_BreakDuration; ?></td></tr>
	    <tr><td><b>Outbound IdleTime</b></td><td><?php echo $Outbound_IdleTime; ?></td></tr>
	    <tr><td><b>Outbound HoldTime</b></td><td><?php echo $Outbound_HoldTime; ?></td></tr>
	    <tr><td><b>Outbound NumberOfCallsDial</b></td><td><?php echo $Outbound_NumberOfCallsDial; ?></td></tr>
	    <tr><td><b>Outbound NumberofCallsconnected</b></td><td><?php echo $Outbound_NumberofCallsconnected; ?></td></tr>
	    <tr><td><b>Outbound ConnectivityPer</b></td><td><?php echo $Outbound_ConnectivityPer; ?></td></tr>
	    <tr><td><b>Outbound AHT</b></td><td><?php echo $Outbound_AHT; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('agent_performance') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
         <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>