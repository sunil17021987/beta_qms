<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Agents List          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <?php echo form_open(base_url() . 'Agents/index'); ?>
        <div class="row">



            <div class="col-md-4">
                <div class="form-group">
                    <label>Status:</label>
                    <select id="status" class="form-control" name="status">
                        <option value="">---  Select Name ---</option>
                        <option  <?php echo ($status=='1')?'selected':'';?> value="1">Active</option>
                        <option <?php echo ($status=='0')?'selected':'';?> value="0"> Close</option>
                    </select>  

                </div>
            </div>






            <div class="col-md-2" style="margin-top: 20px;">
                <button type="submit" class="btn btn-info" style="width: 100%;">
                    Search</button>
            </div>


        </div>
        <?php echo form_close(); ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-4">

                            </div>
                            <div class="col-md-4 text-center">
                                <div style="margin-top: 4px"  id="message">
                                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                                </div>
                            </div>
                            <div class="col-md-4 text-right">
                                <?php echo anchor(site_url('agents/create'), 'Create', 'class="btn btn-primary"'); ?>
                                <?php echo anchor(site_url('agents/excel'), 'Excel', 'class="btn btn-primary"'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <table class="table table-bordered table-striped" id="mytable">
                                <thead>
                                    <tr>
                                        <th><input type="checkbox" id="selectallbox"/>All</th>
                                        <th width="80px">No</th>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>TL</th>
                                        <th>AM</th>
                                        <th>Shift</th>
                                        <th>DOJ</th>
                                        <th>Process</th>
                                        <th>Gender</th>
                                        <th>Batch</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $start = 0;
                                    foreach ($agents_data as $agents) {
                                        ?>
                                        <tr>
                                            <td><input type="checkbox" class="casebox" name="id" value="<?php echo $agents->id ?>"/></td>
                                            <td><?php echo ++$start ?></td>
                                            <td><?php echo $agents->emp_id ?></td>
                                            <td><?php echo $agents->name ?></td>
                                            <td><?php echo $agents->employee_id_TL ?></td>
                                            <td><?php echo $agents->employee_id_AM ?></td>
                                            <td><?php echo $agents->shift_id ?></td>
                                            <td><?php echo date('d-m-Y', strtotime($agents->DOJ)) ?></td>
                                            <td><?php echo $agents->process ?></td>
                                            <td><?php echo $agents->gender ?></td>
                                            <td><?php echo $agents->batch_no ?></td>
                                            <td><small class="label  <?php echo ($agents->status) ? 'bg-green' : 'bg-red'; ?>"><?php echo ($agents->status) ? 'Active' : 'Close'; ?></small></td>
                                            <td style="text-align:center" width="200px">
                                                <?php
                                                echo anchor(site_url('agents/read/' . $agents->id), '<i class="fa fa-eye"></i>');
                                                echo ' | ';
                                                echo anchor(site_url('agents/update/' . $agents->id), '<i class="fa fa-pencil-square-o"></i>');
                                                echo ' | ';
                                                echo anchor(site_url('agents/delete/' . $agents->id), '<i class="fa fa-trash-o" aria-hidden="true"></i>', 'onclick="javasciprt: return confirm(\'Are You Sure ?\')"');
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
<!--                        <div class="row">
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label for="int">Employee Id TL <?php echo form_error('employee_id_TL') ?></label>

                                    <select id="employee_id_TL" class="form-control" name="employee_id_TL">
                                        <option value="">---  Select TL ---</option>
                                        <?php
                                        if (!empty($employee_data_tl)) {
                                            foreach ($employee_data_tl as $rl) {
                                                ?>
                                                <option value="<?php echo $rl->emp_id ?>"><?php echo $rl->emp_name ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label for="int">Employee Id AM <?php echo form_error('employee_id_AM') ?></label>

                                    <select id="employee_id_AM" class="form-control" name="employee_id_AM" id="employee_id_AM">
                                        <option value="">---  Select AM ---</option>
                                        <?php
                                        if (!empty($employee_data_am)) {
                                            foreach ($employee_data_am as $rl) {
                                                ?>
                                                <option value="<?php echo $rl->emp_id ?>"><?php echo $rl->emp_name ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div> 
                            <div class="col-xs-3">
                                <div class="form-group">
                                    <label for="int">Shift <?php echo form_error('shift_id') ?></label>

                                    <select id="shift_id" class="form-control" name="shift_id" id="shift_id">

                                        <?php
                                        if (!empty($shifts_data)) {
                                            foreach ($shifts_data as $rl) {
                                                ?>
                                                <option value="<?php echo $rl->id ?>"><?php echo $rl->name ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>

                                </div>

                            </div>
                            <div class="col-xs-3">
                                <br>
                                <button type="button" id="Update" class="btn btn-primary">Update</button> 

                            </div>
                        </div>-->
                        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                        <script type="text/javascript">
                            $(document).ready(function () {
                                $("#mytable").dataTable({
                                    "pageLength": 500,
                                    'columnDefs': [{'orderable': false, 'targets': 0}], // hide sort icon on header of first column
                                    'aaSorting': [[1, 'asc']] // start to sort data in second column
                                });
                            });

                            $(function () {

                                // add multiple select / deselect functionality
                                $("#selectallbox").click(function () {
                                    $('.casebox').attr('checked', this.checked);
                                });

                                // if all checkbox are selected, check the selectall checkbox
                                // and viceversa
                                $(".casebox").click(function () {

                                    if ($(".casebox").length == $(".casebox:checked").length) {
                                        $("#selectallbox").attr("checked", "checked");
                                    } else {
                                        $("#selectallbox").removeAttr("checked");
                                    }

                                });
                            });
                            $('#Update').click(function (e) {
                                e.preventDefault();

                                var ids = new Array();
                                ;
                                $('.casebox:checked').each(function () {
                                    var values = $(this).val();

                                    ids.push(values);
                                });

                                var employee_id_TL = $("#employee_id_TL").val();
                                var employee_id_AM = $("#employee_id_AM").val();
                                var shift_id = $("#shift_id").val();


                                $.ajax({
                                    url: 'Agents/update_action_ajax',
                                    type: 'POST',
                                    data: {ids: ids, employee_id_TL: employee_id_TL, employee_id_AM: employee_id_AM, shift_id: shift_id},
                                    error: function () {
                                        alert('Something is wrong');
                                    },
                                    success: function (data) {

                                        alert(data);

                                        location.reload(true);
                                    }
                                });


                            });


                        </script>
                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>