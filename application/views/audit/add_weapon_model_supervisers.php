<div class="modal modal-warning fade" id="modal-warning">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add FeedBack..</h4>
            </div>
            <div class="modal-body">
                 <div class="form-group">
            <label for="weapon_primary_cat">FeedBack Given By Auditor</label>            
            <select class="form-control" name="Auditor" id="Auditor">
                <option value="Yes" >Yes</option>
                 <option value="No">No</option>                                
            </select>
        </div>
         
        <div class="form-group">
            <label for="weapon_primary_cat">FeedBack Accepted</label>            
            <select class="form-control" name="Accepted" id="Accepted">
                <option value="Yes" >Yes</option>
                 <option value="No">No</option>                                
            </select>
        </div>
	 
             </div>
          
            <div class="modal-footer">
                <input type="hidden" name="audit_id" id="audit_id" value=""/>
                <button type="button" id="closemodel" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline" id="add_weapon">Save</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>