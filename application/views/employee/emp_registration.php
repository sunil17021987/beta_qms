<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1> New Employee </h1>
        <?php echo validation_errors(); ?>
    </section>
    <div ng-app="myApp" ng-controller="courseCtrl"> 
        <!-- Main content -->
        <section class="content">
            <div class="row">

                <!-- /.box-header -->
                <div class="box-body"> 
                    <!-- ******************/master header end ****************** -->
                    <div class="col-xs-12 col-lg-12">
                        <div class="box-success box view-item col-xs-12 col-lg-12">
                            <div class="stu-master-form">
                                <p class="note">Fields with <span class="required"> <b style="color:red;">*</b></span> are required.</p>
                                <form action="<?php echo $action; ?>" method="post" id="frmemp" autocomplete="off">
                                  <!--<input type="hidden" name="_csrf" value="Z0lZUnFXSUEIexAnRDwzICUGFioIYC4UFB0qKwhkJgQ4fD8GGTgFBw==">-->

                                    <div class="box box-solid box-info col-xs-12 col-lg-12 no-padding">
                                        <div class="box-header with-border">
                                            <h4 class="box-title"><i class="fa fa-info-circle"></i> Personal Details</h4>
                                        </div>
                                        <div class="box-body">
                                            <input type="hidden" name="emp_id" value="<?php echo $emp_id; ?>" />
                                            <input type="hidden" name="created_at" value="<?php echo date('d/m/Y'); ?>" />

                                            <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                                                <div class="col-xs-12 col-sm-4 col-lg-4">
                                                    <div class="form-group">
                                                        <label for="varchar">Name <?php echo form_error('emp_name') ?></label>
                                                        <input type="text" class="form-control" name="emp_name" id="name" placeholder="Name" value="<?php echo $emp_name; ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-lg-4">
                                                    <div class="form-group">
                                                        <label for="varchar">Mobile No. <?php echo form_error('mobile') ?></label>
                                                        <input type="text" class="form-control" name="mobile" id="mob" placeholder="Mobile No." value="<?php echo $mobile; ?>" />
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">




                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">
                                                <div class="col-xs-12 col-sm-4 col-lg-4">
                                                    <div class="form-group">
                                                        <label for="msd_ID">MSD ID<?php echo form_error('msd_ID') ?></label>
                                                        <input type="text" class="form-control" name="msd_ID" id="password" placeholder="MSD ID" value="<?php echo$msd_ID; ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-4 col-lg-4">
                                                    <div class="form-group">
                                                        <label for="password">Password<?php echo form_error('password') ?></label>
                                                        <input type="password" class="form-control" name="password" id="password" placeholder="New Password" value="<?php echo$password; ?>" />
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-lg-12 no-padding">


                                                    <div class="col-xs-12 col-sm-4 col-lg-4">
                                                        <label for="varchar">Designation<?php echo form_error('designation') ?></label>

                                                        <select class="form-control" name="designation">
                                                            <option value="">---  Select Category ---</option>
                                                            <?php
                                                            if (!empty($designation_list)) {
                                                                foreach ($designation_list as $rl) {
                                                                    ?>
                                                                    <option value="<?php echo $rl->id ?>" <?php echo($designation == $rl->id) ? 'selected' : ''; ?>><?php echo $rl->name ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>

                                                      

                                                    </div>
                                                    <div class="col-xs-12 col-sm-4 col-lg-4">
                                                        <div class="form-group has-feedback">
                                                            <label for="varchar">Status <?php echo form_error('status') ?></label>
                                                            <select name="status" class="form-control select">
                                                                <option value="">--- Select Gender ---</option>
                                                                <option value="1" <?php if ($status == '1') echo 'selected'; ?>>Active</option>
                                                                <option value="0" <?php if ($status == '0') echo 'selected'; ?>>Close</option>
                                                            </select>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>



                                        </div>
                                        <!---./end box-body---> 
                                    </div>
                                    <!---./end box--->



                                    <div class="form-group col-xs-6 col-sm-6 col-lg-4 no-padding edusecArLangCss">
                                        <div class="col-xs-6">
                                            <button type="submit" class="btn btn-primary"><?php echo $button ?></button>
                                        </div>
                                        <div class="col-xs-6"> <a href="<?php echo site_url('employee') ?>" class="btn btn-default">Cancel</a> </div>
                                    </div>
                                </form>




                            </div>
                        </div>
                    </div>
                    <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script> 
                    <script>
                                                                $(document).ready(function () {

                                                                    $("#dob").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                                                                    //    $("#datepicker1").inputmask("dd/mm/yyyy", {"placeholder": "dd/mm/yyyy"});
                                                                    $('#admission_date').datepicker({
                                                                        format: 'dd/mm/yyyy',
                                                                        autoclose: true
                                                                    });
                                                                    $('#datepicker2').datepicker({
                                                                        format: 'dd/mm/yyyy',
                                                                        autoclose: true
                                                                    });
                                                                    $(".timepicker").timepicker({
                                                                        showInputs: false
                                                                    });
                                                                });
                                                                var app = angular.module('myApp', []);

                                                                app.controller('courseCtrl', function ($scope, $http) {
                                                                    //    $http.get("http://testdemosite.ml/hotel/room/room_api").then(function (response) {
                                                                    $scope.myData = {};
                                                                    $scope.myData.countryId = "<?php echo $department; ?>";
                                                                    $http.get("<?php echo base_url() ?>department/department_api").then(function (response) {
                                                                        $scope.myData.countries = response.data;
                                                                        //$scope.myData = $scope.options[1];
                                                                        //        $scope.room = $scope.myData[1].id;

                                                                    });
                                                                    $scope.design = {};
                                                                    $scope.design.countryId = "<?php echo $designation; ?>";
                                                                    $http.get("<?php echo base_url() ?>designation/designation_api").then(function (response) {
                                                                        $scope.design.countries = response.data;
                                                                        //        $scope.room = $scope.myData[1].id;

                                                                    });
                                                                    $scope.myFunc = function ($scope) {

                                                                        $http.get("<?php echo base_url() ?>designation/designation_api" + $scope.course_id)
                                                                                .then(function (response) {
                                                                                    $scope.related = response.data;
                                                                                });
                                                                    };
                                                                });</script> 
                    <!-- ******************/master footer ****************** --> 
                </div>
                <!--                </div>
                                           /.col 
                                      </div>--> 

                <!-- /.row --> 
            </div>
        </section>
        <!-- /.content --> 
    </div>
</div>

<script>
//    $(document).ready(function(e) {
//        $('#frmemp').submit(function(e) {
//            if($('#password').val()!="" && $('#confirm_password').val()=="")
//			{
//				alert('Please fill Confirm Password!');
//				e.stopPropagation();
//				e.preventDefault();
//			}
//			else if($('#password').val()!="" && $('#confirm_password').val()!="")
//			{
//				if($('#password').val()!=$('#confirm_password').val())
//				{
//					alert('Passwords do not match!');
//					e.stopPropagation();
//					e.preventDefault();
//				}
//			}
//        });
//    });
</script>