<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Not Login  Date: <?php echo date('d-m-Y');?>     
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-6">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-4">
                                Agents Not Login Today
                            </div>
                            <div class="col-md-4 text-center">
                                <div style="margin-top: 4px"  id="message">
                                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <table class="table table-bordered table-striped" id="mytable">
                                <thead>
                                    <tr>

                                        <th width="80px">Login</th>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>TL</th>
                                        <th>AM</th>
                                        <th>Shift</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $start = 0;
                                    foreach ($agents_data as $agents) {
                                        $trbgcolor='';
                                        if($agents->shiftid==3){
                                             $trbgcolor='#FF978A'; 
                                        }elseif($agents->shiftid==2){
                                              $trbgcolor='#A2DECC'; 
                                        }elseif($agents->shiftid==1){
                                              $trbgcolor='#D5B6DE'; 
                                        }
                                        ?>
                                    <tr style="background-color: <?php echo$trbgcolor;?>">

                                            <td><?php
                                            if($agents->lastLogin==date('Y-m-d')){?>
                                                <a href="#"><i class="fa fa-circle text-success">Yes</i></a>
                                            <?php }else{?>
                                                 <a href="#"><i class="fa fa-circle text-danger">No</i></a>
                                            <?php }
                                              ?></td>
                                            <td><?php echo $agents->emp_id ?></td>
                                            <td><?php echo $agents->name ?></td>
                                            <td><?php echo $agents->employee_id_TL ?></td>
                                            <td><?php echo $agents->employee_id_AM ?></td>
                                            <td><?php echo $agents->shift_id ?></td>


                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>

                        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                        <script type="text/javascript">
//                            $(document).ready(function () {
//                                $("#mytable").dataTable({
//                                    "pageLength": 500,
//                                    'columnDefs': [{'orderable': false, 'targets': 0}], // hide sort icon on header of first column
//                                    'aaSorting': [[1, 'asc']] // start to sort data in second column
//                                });
//                            });

                            $(function () {

                                // add multiple select / deselect functionality
                                $("#selectallbox").click(function () {
                                    $('.casebox').attr('checked', this.checked);
                                });

                                // if all checkbox are selected, check the selectall checkbox
                                // and viceversa
                                $(".casebox").click(function () {

                                    if ($(".casebox").length == $(".casebox:checked").length) {
                                        $("#selectallbox").attr("checked", "checked");
                                    } else {
                                        $("#selectallbox").removeAttr("checked");
                                    }

                                });
                            });
                            $('#Update').click(function (e) {
                                e.preventDefault();

                                var ids = new Array();
                                ;
                                $('.casebox:checked').each(function () {
                                    var values = $(this).val();

                                    ids.push(values);
                                });

                                var employee_id_TL = $("#employee_id_TL").val();
                                var employee_id_AM = $("#employee_id_AM").val();
                                var shift_id = $("#shift_id").val();


                                $.ajax({
                                    url: 'Agents/update_action_ajax',
                                    type: 'POST',
                                    data: {ids: ids, employee_id_TL: employee_id_TL, employee_id_AM: employee_id_AM, shift_id: shift_id},
                                    error: function () {
                                        alert('Something is wrong');
                                    },
                                    success: function (data) {

                                        alert(data);

                                        location.reload(true);
                                    }
                                });


                            });


                        </script>
                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>


            <div class="col-xs-6">


                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
                        <div class="row" style="margin-bottom: 10px">
                            <div class="col-md-6">
                             Employee Not Login
                            </div>


                        </div>
                        <div class="box-body table-responsive">
                            <table class="table table-bordered table-striped" id="mytable">
                                <thead>
                                    <tr>
                                        <th width="20px">No</th>
                                        <th width="20px">MSD ID</th>
                                        <th>Name</th>
                                        <th>Gender</th>


                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $start = 0;
                                    foreach ($employee_data as $employee) {
                                        ?>
                                        <tr>
                                            <td><?php echo ++$start ?></td>
                                            <td><?php echo $employee->msd_ID ?></td>
                                            <td><?php echo $employee->emp_name ?></td>
                                            <td><?php echo $employee->gender ?></td>

                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
                        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
                        <script type="text/javascript">
//                            $(document).ready(function () {
//                                $("#mytable").dataTable();
//                            });
                        </script>
                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
                <!-- /.col -->

            </div>
        </div>
    </section>
</div>