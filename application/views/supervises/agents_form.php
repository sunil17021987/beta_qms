<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Supervises <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
                        <form action="<?php echo $action; ?>" method="post">
                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="varchar">Emp Id <?php echo form_error('emp_id') ?></label>
                                        <input type="text" class="form-control" name="emp_id" id="emp_id" placeholder="Emp Id" value="<?php echo $emp_id; ?>" />
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="varchar">Name <?php echo form_error('name') ?></label>
                                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="int">Employee Id TL <?php echo form_error('employee_id_TL') ?></label>

                                        <select id="category" class="form-control" name="employee_id_TL">
                                            <option value="">---  Select TL ---</option>
                                            <?php
                                            if (!empty($employee_data_tl)) {
                                                foreach ($employee_data_tl as $rl) {
                                                    ?>
                                                    <option value="<?php echo $rl->emp_id ?>" <?php if ($employee_id_TL == $rl->emp_id) echo 'selected'; ?>><?php echo $rl->emp_name ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="int">Employee Id AM <?php echo form_error('employee_id_AM') ?></label>

                                        <select id="category" class="form-control" name="employee_id_AM" id="employee_id_AM">
                                            <option value="">---  Select AM ---</option>
                                            <?php
                                            if (!empty($employee_data_am)) {
                                                foreach ($employee_data_am as $rl) {
                                                    ?>
                                                    <option value="<?php echo $rl->emp_id ?>" <?php if ($employee_id_AM == $rl->emp_id) echo 'selected'; ?>><?php echo $rl->emp_name ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="int">Shift <?php echo form_error('shift_id') ?></label>

                                        <select id="category" class="form-control" name="shift_id" id="shift_id">
                                           
                                            <?php
                                            if (!empty($shifts_data)) {
                                                foreach ($shifts_data as $rl) {
                                                    ?>
                                                    <option value="<?php echo $rl->id ?>" <?php if ($shift_id == $rl->id) echo 'selected'; ?>><?php echo $rl->name ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>

                                    </div>

                                </div>
                                <div class="col-xs-6">
                                    <div class="form-group">
                                        <label for="date">DOJ <?php echo form_error('DOJ') ?></label>

                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input type="text" name="DOJ" id="DOJ" value="<?php echo $DOJ; ?>" class="form-control" data-inputmask="'alias': 'dd-mm-yyyy'" data-mask="">
                                        </div>


                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6">
                                         <div class="form-group">
                                <label for="varchar">Process <?php echo form_error('process') ?></label>
                                <input type="text" class="form-control" name="process" id="process" placeholder="Process" value="<?php echo $process; ?>" />
                            </div>
                                </div>
                                
                                 <div class="col-xs-6">
                                      <div class="form-group">
                                <label for="int">Gender <?php echo form_error('gender') ?></label>
                                
                                  <select name="gender" class="form-control select">
                           
                            <option value="1" <?php if($gender=='1') echo 'selected';  ?>>Male</option>
                            <option value="0" <?php if($gender=='0') echo 'selected';  ?>>Female</option>
                          </select>
                                
                            </div>
                                </div>
                            </div>    

                       
                           <div class="row">
                               <div class="col-xs-6">
                                     <div class="form-group">
                                <label for="int">Batch No <?php echo form_error('batch_no') ?></label>
                                <input type="number" class="form-control" name="batch_no" id="batch_no" placeholder="Batch No" value="<?php echo $batch_no; ?>" />
                            </div>
                               </div>
                               <div class="col-xs-6">
                                     <div class="form-group">
                                <label for="enum">Status <?php echo form_error('status') ?></label>
                              
                            <select name="status" class="form-control select">
                           
                            <option value="1" <?php if($status=='1') echo 'selected';  ?>>Active</option>
                            <option value="0" <?php if($status=='0') echo 'selected';  ?>>Close</option>
                          </select>
                                     </div>
                                   
                               </div>
                           </div>   
                          
                          
                            <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
                            <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                            <a href="<?php echo site_url('supervises') ?>" class="btn btn-default">Cancel</a>
                        </form>
                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>