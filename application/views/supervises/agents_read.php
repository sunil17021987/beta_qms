<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Agents Read          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <table class="table">
	    <tr><td><b>Emp Id</b></td><td><?php echo $emp_id; ?></td></tr>
	    <tr><td><b>Name</b></td><td><?php echo $name; ?></td></tr>
	    <tr><td><b>Employee Id TL</b></td><td><?php echo $employee_id_TL; ?></td></tr>
	    <tr><td><b>Employee Id AM</b></td><td><?php echo $employee_id_AM; ?></td></tr>
	    <tr><td><b>Shift Id</b></td><td><?php echo $shift_id; ?></td></tr>
	    <tr><td><b>DOJ</b></td><td><?php echo $DOJ; ?></td></tr>
	    <tr><td><b>Process</b></td><td><?php echo $process; ?></td></tr>
	    <tr><td><b>Gender</b></td><td><?php echo $gender; ?></td></tr>
	    <tr><td><b>Batch No</b></td><td><?php echo $batch_no; ?></td></tr>
	    <tr><td><b>Status</b></td><td><?php echo $status; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('agents') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
         <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>