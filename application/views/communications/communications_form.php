<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Communications <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
                        <form action="<?php echo $action; ?>" method="post">
                            <div class="form-group">
                                <label for="enum">Option Type <?php echo form_error('option_type') ?></label>
                                <select name="option_type" class="form-control select" id="option_type">
                                    <option value="">--- Select  ---</option>
                                    <option value="1" <?php if ($option_type == '1') echo 'selected'; ?>>Multi option</option>
                                    <option value="0" <?php if ($option_type == '0') echo 'selected'; ?>>single option</option>
                                </select>

                            </div>
                            <div class="form-group">
                                <label for="varchar">Title <?php echo form_error('title') ?></label>
                                <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?php echo $title; ?>" />
                            </div>
                            <div class="form-group">
                                <label for="options">Options(With comma separator) <?php echo form_error('options') ?></label>
                                <textarea class="form-control" rows="3" name="options" id="options" placeholder="Options"><?php echo $options; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label for="enum">Score <?php echo form_error('score') ?></label>
                                <input type="text" class="form-control" name="score" id="status" placeholder="Score" value="<?php echo $score; ?>" />
                            </div>
                            <div class="form-group">
                                <label for="enum">Status <?php echo form_error('status') ?></label>
                                <select name="status" class="form-control select">
                                    <option value="">--- Select  ---</option>
                                    <option value="1" <?php if ($status == '1') echo 'selected'; ?>>Active</option>
                                    <option value="0" <?php if ($status == '0') echo 'selected'; ?>>Inactive</option>
                                </select>

                            </div>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
                            <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                            <a href="<?php echo site_url('communications') ?>" class="btn btn-default">Cancel</a>
                        </form>
                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
</div>
<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<script type="text/javascript">

  $('#option_type').on('change', function() {
  var valueof=this.value;
  if(valueof=='0'){
    $('#options').val('Yes,No');  
  }else{
      $('#options').val('');  
  }
});
</script>     