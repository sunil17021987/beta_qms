<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Agentfeedback <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Audit Id <?php echo form_error('audit_id') ?></label>
            <input type="text" class="form-control" name="audit_id" id="audit_id" placeholder="Audit Id" value="<?php echo $audit_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="enum">Auditor <?php echo form_error('Auditor') ?></label>
            <input type="text" class="form-control" name="Auditor" id="Auditor" placeholder="Auditor" value="<?php echo $Auditor; ?>" />
        </div>
	    <div class="form-group">
            <label for="enum">Accepted <?php echo form_error('Accepted') ?></label>
            <input type="text" class="form-control" name="Accepted" id="Accepted" placeholder="Accepted" value="<?php echo $Accepted; ?>" />
        </div>
	    <div class="form-group">
            <label for="timestamp">Date Of <?php echo form_error('date_of') ?></label>
            <input type="text" class="form-control" name="date_of" id="date_of" placeholder="Date Of" value="<?php echo $date_of; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('agentfeedback') ?>" class="btn btn-default">Cancel</a>
	</form>
     <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>