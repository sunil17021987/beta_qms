<div class="modal modal-warning fade" id="modal-warning_<?=$value->id;?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add FeedBack.. Multi<?php echo$value->id; ?></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="weapon_primary_cat">FeedBack Given By Auditor</label>            
                    <select class="form-control" name="Auditor1" id="Auditor1">
                        <option value="Yes" >Yes</option>
                        <option value="No">No</option>                                
                    </select>
                </div>

                <div class="form-group">
                    <label for="weapon_primary_cat">FeedBack Accepted</label>            
                    <select class="form-control" name="Accepted1" id="Accepted1">
                        <option value="Yes" >Yes</option>
                        <option value="No">No</option>                                
                    </select>
                </div>

            </div>

            <div class="modal-footer">
                <input type="hidden" name="audit_id1" id="audit_id1" value="<?php echo$value->id; ?>"/>
                <button type="button" id="closemodel" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-outline add_weapon_2" id="add_weapon_<?php echo$value->id; ?>">Save</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>