<style>
    table {
        border-collapse: collapse;
    }
    td, th {
        border: 1px solid orange;
    }

    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
        border-top: 1px solid orange;
    }
    .tabletd{
        background-color:#f39c12;
        color: #FFF;
        font-size: 16px;
        font-weight: 700;
    }
    #quality_scored{
        width: 25%;
    }
</style>

<div class="content-wrapper" style="min-height: 946px;" id="auditForm">
    <form role="form" id="idForm" action="<?php echo site_url('Audit/save_records') ?>" autocomplete="off">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="row">
                <div class="col-md-2"> Reports:</div>





        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">

                <!-- /.col -->
                <div class="col-md-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#activity" data-toggle="tab">Audit</a></li>
                            <li><a href="#timeline" data-toggle="tab">Call duration</a></li>
                            <li><a href="#settings" data-toggle="tab">Settings</a></li>
                        </ul>
                        <div class="tab-content">

                            <div class="active tab-pane" id="activity">
                                <!--***********************************************-->
                                    <?php $this->load->view($call_duration_pattern); ?>
                                    <!------>
                                <!--***********************************************-->
                            </div>

                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="timeline">

                              
                                <!------>
                            </div>
                            <!-- /.tab-pane -->

                            <div class="tab-pane" id="settings">
                                cc
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </form>
</div>
<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>

<script type="text/javascript">
    $(document).ready(function () {

        $('#quality_scored').val(100);
        $('#Login_ID').on('keyup', function () {
            var Login_ID = this.value;

            $.ajax({
                type: "POST",
                url: "<?php echo site_url('Gateway/get_records') ?>",
                //dataType : "JSON",
                data: {Login_ID: Login_ID},
                success: function (data) {

                    var json = JSON.parse(data);
                    if (json.rows != '') {
                        // alert(json.rows.Login_ID);
                        $('#EMP_ID').val(json.rows.EMP_ID);
                        $('#CRM_ID').val(json.rows.CRM_ID);
                        $('span#TL_Name').html('').append(json.rows.employee_id_TL);
                        $('span#AM_Name').html('').append(json.rows.employee_id_AM);
                        $('span#CCR_Name').html('').append(json.rows.name);
                        $('#agents_id').val(json.rows.id);
                        $('#TL_id').val(json.rows.TL_id);
                        $('#AM_id').val(json.rows.AM_id);


                    } else {
                        $('#EMP_ID').val();
                        $('#CRM_ID').val();
                        $('span#TL_Name').html('').append();
                        $('span#AM_Name').html('').append();
                        $('span#CCR_Name').html('').append();
                        $('#agents_id').val();
                        $('#TL_id').val();
                        $('#AM_id').val();
                    }
                }
            });
            return false;
        });


        $(".Opening").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Opening[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 10);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 10);
                    }
                }
            }
        });

        $(".ActiveListening").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='ActiveListening[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 6);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 6);
                    }
                }
            }
        });

        $(".Probing").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Probing[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 10);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 10);
                    }
                }
            }
        });

        $(".Customer_Engagement").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Customer_Engagement[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 6);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 6);
                    }
                }
            }
        });

        $(".Empathy_where_required").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Empathy_where_required[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 5);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 5);
                    }
                }
            }
        });

        $(".Understanding").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Understanding[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 6);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 6);
                    }
                }
            }
        });
        $(".Professionalism").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Professionalism[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 10);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 10);
                    }
                }
            }
        });

        $(".Politeness").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Politeness[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 10);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 10);
                    }
                }
            }
        });


        $(".Hold_Procedure").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Hold_Procedure[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 5);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 5);
                    }
                }
            }
        });

        $(".Closing").click(function () {
            var quality_scored = parseInt($('#quality_scored').val());
            var Opening = [];
            $.each($("input[name='Closing[]']:checked"), function () {
                Opening.push($(this).val());
            });
            if (Opening.length === 0) {
                $('#quality_scored').val(quality_scored + 7);
            } else {
                if (Opening.length === 1) {
                    if ($(this).is(':checked')) {
                        $('#quality_scored').val(quality_scored - 7);
                    }
                }
            }
        });

        $(".Correct_smaller").click(function () {

            var quality_scored = parseInt($('#quality_scored').val());

            if ($(this).val() === '3' || $(this).val() === '6') {
                var check_true = $('#check_true').val();
                if (check_true === 'F') {
                    $('#check_true').val('T');
                    $('#quality_scored').val(quality_scored + 10);
                }
            } else {
                var check_true = $('#check_true').val();
                if (check_true === 'T') {
                    $('#check_true').val('F');
                    $('#quality_scored').val(quality_scored - 10);
                }

            }
        });

        $(".Accurate_Complete").click(function () {

            var quality_scored = parseInt($('#quality_scored').val());

            if ($(this).val() === '2') {
                $('#check_fatal').val(quality_scored);
                $('#quality_scored').val(0);

            } else {
                var check_fatal = $('#check_fatal').val();
                quality_scored = check_fatal;
                $('#check_true').val('F');
                $('#quality_scored').val(quality_scored);


            }
        });


        $('#category_main_id').on('change', function () {
            var category_main_id = $(this).val();

            if (category_main_id) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url(); ?>/Category_sub/get_subcategories',
                    data: {category_main_id: category_main_id},
                    success: function (html) {
                        $('#category_sub_id').html(html);
                    }
                });


            }

        });
        $('#category_main_id_crr').on('change', function () {
            var category_main_id_crr = $(this).val();

            if (category_main_id_crr) {
                $.ajax({
                    type: 'POST',
                    url: '<?php echo site_url(); ?>/Category_sub/get_subcategories',
                    data: {category_main_id: category_main_id_crr},
                    success: function (html) {
                        $('#category_sub_id_crr').html(html);
                    }
                });


            }

        });


        $("#save").click(function () {

            var quality_scored = parseInt($('#quality_scored').val());
            var agents_id = parseInt($('#agents_id').val());
            var Call_Date = $('#Call_Date').val();
            var Time_Of_Call = $('#Time_Of_Call').val();
            var Calling_Number = $('#Calling_Number').val();

            var CLI_Number = $('#CLI_Number').val();
            var Call_Dur_Min = $('#Call_Dur_Min').val();
            var Call_Dur_Sec = $('#Call_Dur_Sec').val();
            var Call_Type = $('#Call_Type').val();

            var Todays_Audit_Count = $('#Todays_Audit_Count').val();
            var category_main_id = $('#category_main_id').val();
            var category_sub_id = $('#category_sub_id').val();
            var category_main_id_crr = $('#category_main_id_crr').val();
            var category_sub_id_crr = $('#category_sub_id_crr').val();
            var Consumers_Concern = $('#Consumers_Concern').val();
            var r_g_y_a = $('#r_g_y_a').val();
            var QME_Remarks = $('#QME_Remarks').val();
            var p_s_if_a = $('#p_s_if_a').val();
            var c_t_a_p_p = $('#c_t_a_p_p').val();

            var Opening = $('.Opening').val();
            var ActiveListening = $('.ActiveListening').val();
            var Probing = $('.Probing').val();
            var Customer_Engagement = $('.Customer_Engagement').val();
            var Empathy_where_required = $('.Empathy_where_required').val();
            var Understanding = $('.Understanding').val();
            var Professionalism = $('.Professionalism').val();
            var Politeness = $('.Politeness').val();
            var Hold_Procedure = $('.Hold_Procedure').val();
            var Closing = $('.Closing').val();
            var Correct_smaller = $('.Correct_smaller').val();
            var Accurate_Complete = $('.Accurate_Complete').val();
            var Fatal_Reason = $('#Fatal_Reason').val();


        });


        $("#idForm").submit(function (e) {

            e.preventDefault(); // avoid to execute the actual submit of the form.

            var form = $(this);
            var url = form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(), // serializes the form's elements.
                success: function (data)
                {
                    if (data === '1') {
                        alert('Audit Done....');
                        // location.reload();

                        window.location.href = "<?php echo base_url('Gateway/login') ?>";
                    } else {
                        alert('Error...! ');
                    }
                }
            });


        });


    });



</script>

