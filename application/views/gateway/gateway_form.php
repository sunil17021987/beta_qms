<style>
    table {
        border-collapse: collapse;
    }
    td, th {
        border: 1px solid orange;
    }

    .table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
        border-top: 1px solid orange;
    }
    .tabletd{
        background-color:#f39c12;
        color: #FFF;
        font-size: 16px;
        font-weight: 700;
    }
    #quality_scored{
        width: 25%;
    }
    .error{
        color: red;
    }
</style>


<div class="content-wrapper" style="min-height: 946px;" id="auditForm">
    <form role="form" id="idForm" action="<?php echo site_url('Audit/save_records') ?>" method="post" autocomplete="off">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="row">
                <div class="col-md-2"> GateWay:</div>
                <div class="col-md-2">
                    <small class="label  bg-blue">Agent : &nbsp;&nbsp;<?php echo $name; ?></small>

                </div>
                <div class="col-md-2">
                    <small class="label  bg-yellow">TL : &nbsp;&nbsp;<?php echo $employee_id_TL; ?></small>
                </div>
                <div class="col-md-2">
                    <small class="label  bg-yellow"> AM : &nbsp;&nbsp;<?php echo $employee_id_AM; ?></small>

                </div>
                <div class="col-md-2">
                    <small class="label  bg-black-active">Today's : &nbsp;&nbsp;<?php echo $tdaysaudits; ?></small>
                </div>
                <div class="col-md-2">
                    <input type="text" name="score" readonly class="form-control input-sm" placeholder="Enter ..." id="quality_scored">
                    <input type="hidden" name="audit_time" value="<?php echo time() ?>" id="audit_time">
                </div>
            </div>




        </section>

        <!-- Main content -->
        <section class="content">

            <div class="row">

                <!-- /.col -->
                <div class="col-md-12">
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#activity" data-toggle="tab">Audit</a></li>
                            <li><a href="#timeline" data-toggle="tab">Performance</a></li>
                            <li><a href="#settings" data-toggle="tab">Settings</a></li>
                        </ul>
                        <div class="tab-content">

                            <div class="active tab-pane" id="activity">
                                <!--***********************************************-->

                                <div class="row">

                                    <div class="box box-warning">

                                        <div class="box-body">

                                            <!-- text input -->

                                            <div class="row">
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Call Date</label>
                                                        <div class="input-group date">
                                                            <!--                                                            <div class="input-group-addon">
                                                                                                                            <i class="fa fa-calendar"></i>
                                                                                                                        </div>-->
                                                            <input type="text" readonly name="Call_Date" class="form-control pull-right" id="Call_Date">
                                                        </div>


                                                    </div>
                                                </div>
                                                <div class="col-md-2">

                                                    <div class="bootstrap-timepicker">
                                                        <div class="form-group">
                                                            <label>Time Of Call:</label>

                                                            <div class="input-group">
                                                                <input type="text" name="Time_Of_Call" id="Time_Of_Call" class="form-control input-sm timepicker">
                                                                <div class="input-group-addon">
                                                                    <i class="fa fa-clock-o"></i>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>


                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Calling Number</label>

                                                        <div class="input-group">

                                                            <input type="text" name="Calling_Number" id="Calling_Number" class="form-control input-sm">
                                                        </div>

                                                    </div>




                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>CLI Number</label>
                                                        <input type="text" name="CLI_Number" id="CLI_Number" class="form-control input-sm" placeholder="Enter ...">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label>Call(M)</label>
                                                        <input type="text" name="Call_Dur_Min" id="Call_Dur_Min" class="form-control input-sm" placeholder="Enter ...">
                                                    </div>
                                                </div>
                                                <div class="col-md-1">
                                                    <div class="form-group">
                                                        <label>Sec.</label>
                                                        <input type="text" name="Call_Dur_Sec" id="Call_Dur_Sec" value="" class="form-control input-sm" placeholder="Enter ...">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group">
                                                        <label>Call Type</label>

                                                        <select name="Call_Type" class=" form-control select1 input-sm" id="Call_Type">
                                                            <option value="">Select Category</option>
                                                            <?php
                                                            if (!empty($call_types_data)) {
                                                                foreach ($call_types_data as $call_types) {
                                                                    ?>
                                                                    <option <?php echo ($call_types->id == $Call_Type) ? 'selected' : ''; ?>  value="<?php echo $call_types->id ?>"><?php echo $call_types->name ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>




                                            </div>



                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Category As Per Process</label>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <select name="category_main_id" class=" form-control select1 input-sm" id="category_main_id">
                                                            <option value="">Select Category</option>
                                                            <?php
                                                            if (!empty($category_main)) {
                                                                foreach ($category_main as $downloadtype) {
                                                                    ?>
                                                                    <option <?php echo ($downloadtype->category_main_id == $category_main_id) ? 'selected' : ''; ?>  value="<?php echo $downloadtype->category_main_id ?>"><?php echo $downloadtype->category_main_name ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <select name="category_sub_id" class=" form-control select1 input-sm" id="category_sub_id">
                                                            <option value="">Select Sub Category</option>

                                                        </select>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Category As Per CCR</label>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <select name="category_main_id_crr" class=" form-control select1 input-sm" id="category_main_id_crr">
                                                            <option value="">Select Category</option>
                                                            <?php
                                                            if (!empty($category_main)) {
                                                                foreach ($category_main as $downloadtype) {
                                                                    ?>
                                                                    <option <?php echo ($downloadtype->category_main_id == $category_main_id) ? 'selected' : ''; ?>  value="<?php echo $downloadtype->category_main_id ?>"><?php echo $downloadtype->category_main_name ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="form-group">

                                                        <select name="category_sub_id_crr" class=" form-control select1 input-sm" id="category_sub_id_crr">
                                                            <option value="">Select Sub Category</option>

                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Consumer's Concern</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">
                                                        <input type="text" name="Consumers_Concern" id="Consumers_Concern" value="" class="form-control input-sm" placeholder="Enter ..." >
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Resolution Given By Adviser</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">

                                                        <input type="text" name="r_g_y_a" id="r_g_y_a" value="" class="form-control input-sm" placeholder="Enter ..." >
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>QME Remarks</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">

                                                        <input type="text" name="QME_Remarks" id="QME_Remarks" value="" class="form-control input-sm" placeholder="Enter ..." >
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <label>Process Suggestion If Any</label>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="form-group">

                                                        <input type="text" name="p_s_if_a" id="p_s_if_a" class="form-control input-sm" placeholder="Enter ..." >
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <label>Disposition</label>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">


                                                        <select name="t_d_b_a" class=" form-control select1 input-sm" id="t_d_b_a">
                                                            <option value="">Select Dispositions</option>
                                                            <?php
                                                            if (!empty($dispositions_data)) {
                                                                foreach ($dispositions_data as $downloadtype) {
                                                                    ?>
                                                                    <option <?php echo ($downloadtype->id == $t_d_b_a) ? 'selected' : ''; ?>  value="<?php echo $downloadtype->name ?>"><?php echo $downloadtype->name ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>


                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <label> Correct Disposition</label>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <select name="c_t_a_p_p" class=" form-control select1 input-sm" id="c_t_a_p_p">
                                                            <option value="">Select Dispositions</option>
                                                            <?php
                                                            if (!empty($dispositions_data)) {
                                                                foreach ($dispositions_data as $downloadtype) {
                                                                    ?>
                                                                    <option <?php echo ($downloadtype->id == $c_t_a_p_p) ? 'selected' : ''; ?>  value="<?php echo $downloadtype->name ?>"><?php echo $downloadtype->name ?></option>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </select>


                                                    </div>
                                                </div>



                                            </div>

                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <label for="enum">Fatal <?php echo form_error('fatal') ?></label>
                                                        <select name="fatal" class="form-control select" id="fatal">
                                                            <option value="">--- Select  ---</option>
                                                            <option value="1" <?php if ($fatal ==1) echo 'selected'; ?>>Yes</option>
                                                            <option value="0" <?php if ($fatal ==0) echo 'selected'; ?>>No</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-md-8">
                                                      <div class="form-group">
                                                           <label for="enum">Fatal Reason <?php echo form_error('Fatal_Reason') ?></label>
                                                        <input type="text" name="Fatal_msg" id="Fatal_Reason" value="" class="form-control input-sm" placeholder="Enter ..." >
                                                    </div>
                                                    
                                                    
                                                </div>
                                            </div>


                                            <div class="row-border"></div>


                                        </div>
                                        <!-- /.box-body -->
                                    </div>

                                </div>



                                <!-- /.box-body -->






                                <div class="row">

                                    <?php
                                    $numOfCols = 3;
                                    $rowCount = 0;
                                    $bootstrapColWidth = 12 / $numOfCols;



                                    $c = 0;
                                    $cname = '';
                                    if (!empty($communications_data)) {
                                        foreach ($communications_data as $cdtata) {
                                            ++$c;
                                            $cname = 'c' . $c;
                                            ?>

                                            <div class="col-md-<?php echo $bootstrapColWidth; ?>">
                                                <div class="box box-warning box-solid">
                                                    <div class="box-header with-border">
                                                        <h3 class="box-title"><?php echo$cdtata->title ?></h3>
                                                        <div class="box-tools pull-right">
                                                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                                                            </button>
                                                        </div>
                                                        <!-- /.box-tools -->
                                                    </div>
                                                    <!-- /.box-header -->
                                                    <div class="box-body" style="">
                                                        <ul class="nav nav-stacked">
                                                            <?php
                                                            $Opening = explode(",", $cdtata->options);
                                                            $val = 0;
                                                            foreach ($Opening as $key => $value) {
                                                                $val = $key + 1;
                                                                ?>
                                                                <li>
                                                                    
                                                                    <?php
                                                                    if($cdtata->option_type=='1'){?>
                                                                         <input type="checkbox" <?php echo ($val == $Opening) ? 'checked' : ''; ?> name="c<?php echo$cdtata->id;?>[]" class="<?php echo$cname; ?>" score="<?php echo$cdtata->score; ?>" value="<?php echo$val; ?>" onclick="boxDisable(this)"/>
                                                                    <span> <?php echo $value; ?></span>
                                                                    <?php }
                                                                    else{?>
                                                                    <input type="radio" <?php echo ($val == $Opening) ? 'checked' : ''; ?> name="c<?php echo$cdtata->id;?>" class="<?php echo$cname; ?>" score="<?php echo$cdtata->score;?>" valueof="<?php echo$value;?>" value="<?php echo$val; ?>" onclick="boxDisableradio(this)"/>
                                                                          <span> <?php echo $value; ?></span>
                                                                   <?php }
                                                                    ?>
                                                                    
                                                                    
                                                                    
                                                                   
                                                                </li>
                                                            <?php }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                    <!-- /.box-body -->
                                                </div>

                                            </div>






                                            <?php
                                            $rowCount++;
                                            if ($rowCount % $numOfCols == 0)
                                                echo '</div><div class="row">';
                                        }
                                    }
                                    ?>



                                </div>



                                <div class="row">
                                    <div class="col-md-11"></div>
                                    <div class="col-md-1">
                                        <input type="hidden" name="agents_id" value="<?php echo$agents_id; ?>" id="agents_id">
                                        <input type="hidden" name="employee_id_TL" value="<?php echo$TL_id; ?>" id="TL_id"/>
                                        <input type="hidden" name="employee_id_AM" value="<?php echo$AM_id; ?>" id="AM_id"/>

                                        <button type="submit"  class="btn btn-primary">Save</button> 
                                    </div>
                                </div>



                                <!--***********************************************-->
                            </div>

                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="timeline">
                                <!------>
                                <?php $this->load->view($agentPerformanceform); ?>

                                <!------>
                            </div>
                            <!-- /.tab-pane -->

                            <div class="tab-pane" id="settings">
                                <?php $this->load->view($call_duration_pattern); ?>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.nav-tabs-custom -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->

        </section>
        <!-- /.content -->
    </form>
</div>

<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<!--<script src="<?php echo base_url('assets/timepicker/js/jquery.min.js') ?>"></script>-->
<!--<script src="<?php echo base_url('assets/timepicker/js/timepicki.js') ?>"></script>-->

<script type="text/javascript" src="<?php echo base_url('assets/dist/jquery.validate.js') ?>"></script>




<script type="text/javascript">
      $('#fatal').on('change', function() {
  var valueof=this.value;
  if(valueof=='1'){
       $('#quality_scored').val(0);
  
  }else{
      $('#quality_scored').val(100);
  }
});
    
    
    
    
                                                            function boxDisable(e) {
                                                                var quality_scored = parseInt($('#quality_scored').val());
                                                                var currentclass = $(e).attr('class');
                                                                var currentfieldname = $(e).attr('name');
                                                                var currentclassvalue = $(e).attr('score');
                                                                //  alert(currentfieldname);
                                                                //  alert(currentclass);
                                                                //   alert(currentclassvalue);
                                                                // var weightage = $(e).attr('weightage');

                                                                var $b = $("." + currentclass);
                                                                var Opening = [];
                                                                $.each($("input[name='" + currentfieldname + "']:checked"), function () {
                                                                    Opening.push(currentclassvalue);
                                                                });
                                                                // alert(Opening.length);
                                                                if (Opening.length === 1) {
                                                                    if ($(e).is(':checked')) {
                                                                        $('#quality_scored').val(quality_scored - parseInt(currentclassvalue));
                                                                    }

                                                                } else {
                                                                    if (Opening.length === 0) {
                                                                        $('#quality_scored').val(quality_scored + parseInt(currentclassvalue));
                                                                    }
                                                                }
                                                            }
                                                            
                                                            
                                                            
                                                            
                                                              function boxDisableradio(e) {
                                                                var quality_scored = parseInt($('#quality_scored').val());
                                                                var currentclass = $(e).attr('class');
                                                                var currentfieldname = $(e).attr('name');
                                                                var currentclassvalue = $(e).attr('score');
                                                                 var valueof = $(e).attr('valueof');
                                                                 
                                                                //  alert(currentclass);
                                                                //   alert(currentclassvalue);
                                                                // var weightage = $(e).attr('weightage');

                                                                var $b = $("." + currentclass);
                                                                var Opening = [];
//                                                                $.each($("input[name='" + currentfieldname + "']:checked"), function () {
//                                                                    Opening.push(currentclassvalue);
//                                                                });
                                                                // alert(Opening.length);
                                                                if (valueof === 'Yes') {
                                                                  
                                                                        $('#quality_scored').val(quality_scored - parseInt(currentclassvalue));
                                                                

                                                                } else {
                                                                    if (valueof === 'No') {
                                                                        $('#quality_scored').val(quality_scored + parseInt(currentclassvalue));
                                                                    }
                                                                }
                                                            } 
                                                            
                                                            
                                                            $(document).ready(function () {

                                                                $('#quality_scored').val(100);
                                                                $('#Login_ID').on('keyup', function () {
                                                                    var Login_ID = this.value;

                                                                    $.ajax({
                                                                        type: "POST",
                                                                        url: "<?php echo site_url('Gateway/get_records') ?>",
                                                                        //dataType : "JSON",
                                                                        data: {Login_ID: Login_ID},
                                                                        success: function (data) {

                                                                            var json = JSON.parse(data);
                                                                            if (json.rows != '') {
                                                                                // alert(json.rows.Login_ID);
                                                                                $('#EMP_ID').val(json.rows.EMP_ID);
                                                                                $('#CRM_ID').val(json.rows.CRM_ID);
                                                                                $('span#TL_Name').html('').append(json.rows.employee_id_TL);
                                                                                $('span#AM_Name').html('').append(json.rows.employee_id_AM);
                                                                                $('span#CCR_Name').html('').append(json.rows.name);
                                                                                $('#agents_id').val(json.rows.id);
                                                                                $('#TL_id').val(json.rows.TL_id);
                                                                                $('#AM_id').val(json.rows.AM_id);


                                                                            } else {
                                                                                $('#EMP_ID').val();
                                                                                $('#CRM_ID').val();
                                                                                $('span#TL_Name').html('').append();
                                                                                $('span#AM_Name').html('').append();
                                                                                $('span#CCR_Name').html('').append();
                                                                                $('#agents_id').val();
                                                                                $('#TL_id').val();
                                                                                $('#AM_id').val();
                                                                            }
                                                                        }
                                                                    });
                                                                    return false;
                                                                });





                                                                $('#category_main_id').on('change', function () {
                                                                    var category_main_id = $(this).val();

                                                                    if (category_main_id) {
                                                                        $.ajax({
                                                                            type: 'POST',
                                                                            url: '<?php echo site_url(); ?>/Category_sub/get_subcategories',
                                                                            data: {category_main_id: category_main_id},
                                                                            success: function (html) {
                                                                                $('#category_sub_id').html(html);
                                                                            }
                                                                        });


                                                                    }

                                                                });
                                                                $('#category_main_id_crr').on('change', function () {
                                                                    var category_main_id_crr = $(this).val();

                                                                    if (category_main_id_crr) {
                                                                        $.ajax({
                                                                            type: 'POST',
                                                                            url: '<?php echo site_url(); ?>/Category_sub/get_subcategories',
                                                                            data: {category_main_id: category_main_id_crr},
                                                                            success: function (html) {
                                                                                $('#category_sub_id_crr').html(html);
                                                                            }
                                                                        });


                                                                    }

                                                                });




                                                                $('#idForm').validate({

                                                                    rules: {
                                                                        Call_Date: "required",
                                                                        Time_Of_Call: "required",
                                                                        Calling_Number: {
                                                                            required: true,
                                                                            number: true,
                                                                            minlength: 10,
                                                                            maxlength: 12
                                                                        },
                                                                        CLI_Number: {
                                                                            required: true,
                                                                             minlength: 13,
                                                                        },
                                                                        Call_Dur_Min: {
                                                                            required: true,
                                                                            number: true,
                                                                            minlength: 1,
                                                                            maxlength: 2
                                                                        },
                                                                        Call_Dur_Sec: {
                                                                            required: true,
                                                                            number: true,
                                                                            minlength: 1,
                                                                            maxlength: 2
                                                                        },
                                                                        Call_Type: {
                                                                            required: true
                                                                        },
                                                                        category_main_id: {
                                                                            required: true
                                                                        },
                                                                        category_sub_id: {
                                                                            required: true
                                                                        },
                                                                        category_main_id_crr: {
                                                                            required: true
                                                                        },
                                                                        category_sub_id_crr: {
                                                                            required: true
                                                                        },
                                                                        Consumers_Concern: {
                                                                            required: true
                                                                        },
                                                                        r_g_y_a: {
                                                                            required: true
                                                                        },
                                                                        QME_Remarks: {
                                                                            required: true
                                                                        },
                                                                        p_s_if_a: {
                                                                            required: true
                                                                        },
                                                                        t_d_b_a: {
                                                                            required: true
                                                                        },
                                                                        c_t_a_p_p: {
                                                                            required: true
                                                                        }

                                                                    },
                                                                    messages: {
                                                                        Call_Date: "Please Enter Date",
                                                                        Time_Of_Call: "Please Enter Time",
                                                                        Calling_Number: {
                                                                            required: "Mobile Number 10-12 Digits",
                                                                            number: "Mobile Number 10-12 Digits",
                                                                            minlength: "Mobile Number 10-12 Digits",
                                                                            maxlength: "Mobile Number 10-12 Digits"
                                                                        },
                                                                        CLI_Number: {
                                                                            required: "CLI Number"
                                                                        },
                                                                        Call_Dur_Min: {
                                                                            required: "Min",
                                                                            number: "Min"
                                                                        },
                                                                        Call_Dur_Sec: {
                                                                            required: "Min",
                                                                            number: "Min"
                                                                        },
                                                                        Call_Type: {
                                                                            required: "Call Type"
                                                                        },
                                                                        category_main_id: {
                                                                            required: "category main"
                                                                        },
                                                                        category_sub_id: {
                                                                            required: "category Sub"
                                                                        },
                                                                        category_main_id_crr: {
                                                                            required: "category Main"
                                                                        },
                                                                        category_sub_id_crr: {
                                                                            required: "category Sub"
                                                                        },
                                                                        Consumers_Concern: {
                                                                            required: "Consumer's Concern"
                                                                        },
                                                                        r_g_y_a: {
                                                                            required: "Resolution Given By Adviser"
                                                                        },
                                                                        QME_Remarks: {
                                                                            required: "QME Remarks"
                                                                        },
                                                                        p_s_if_a: {
                                                                            required: "Process Suggestion If Any"
                                                                        },
                                                                        t_d_b_a: {
                                                                            required: "Disposition"
                                                                        },
                                                                        c_t_a_p_p: {
                                                                            required: "Correct Disposition"
                                                                        }
                                                                    },

                                                                    submitHandler: function (form) {

                                                                        form.preventDefault(); // avoid to execute the actual submit of the form.
                                                                        $.ajax({
                                                                            url: form.action,
                                                                            type: form.method,
                                                                            data: form.serialize(), // serializes the form's elements.
                                                                            success: function (data)
                                                                            {
                                                                                if (data === '1') {
//                        alert('Audit Done....');
//                        window.location.href = "<?php echo base_url('Gateway/login') ?>";
                                                                                } else {
//                        alert('Error...! ');
                                                                                }
                                                                            }
                                                                        });
                                                                    }
                                                                });


                                                                $('#CLI_Number').on('change', function () {
                                                                    var CLI_Number = this.value;

                                                                    $.ajax({
                                                                        type: "POST",
                                                                        url: "<?php echo site_url('Gateway/get_cliduplicate') ?>",
                                                                        //dataType : "JSON",
                                                                        data: {CLI_Number: CLI_Number},
                                                                        success: function (data) {

                                                                            var json = JSON.parse(data);
                                                                            if (json.rows != '') {
                                                                                alert('CLI Already Exist: ' + json.rows.CLI_Number);
                                                                                $('#CLI_Number').val("");
                                                                            } else {

                                                                            }
                                                                        }
                                                                    });
                                                                    return false;
                                                                });


                                                            });



</script>
<!--<script src="<?php echo base_url('assets/timepicker/js/bootstrap.min.js') ?>"></script>-->
