<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Bordered Table</h3>
        <input type="text" name="getvalue" id="getvalue" value="" style="float: right"/>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table class="table table-bordered">
            <tbody><tr>
                    <th>Call duration pattern</th>
                    <th>Count of Call</th>
                    <th>Target</th>
                    <th>Current %</th>
                    <th>Required</th>
                    <th>Pending</th>
                </tr>

                <?php
                 $CountofCall=0;
                foreach ($calldurationpattern_data as $value) {
                    $auditss = $this->Audit_model->get_count_bycallDuration($value->start_time, $value->end_time);
                // echo $this->db->last_query();
               //  die();
                       $CountofCall = $CountofCall + $auditss->total;
                 
                   
                }


               
                $Target = 0;
                 $count=0;
                foreach ($calldurationpattern_data as $value) {
                    
                    $audits = $this->Audit_model->get_count_bycallDuration($value->start_time, $value->end_time);
                  
                 
                    // $CountofCall = $CountofCall + $audits->total;
                    $Target = $Target + $value->target;
                    $Current = 0;
                    if($audits->total!=0){
                    $Current= round(($audits->total/$CountofCall)*100);
                    
                    }else{
                        
                    }
                    ?>
                    <tr>
                        <td><?php echo$value->name ?></td>
                        <td><input type="hidden" id="total_<?php echo$count;?>" name="total" value="<?php echo$audits->total?>"> <span class="badge bg-green"><?php echo$audits->total; ?></span></td>
                        <td> <input type="hidden" id="targetTextbox_<?php echo$count;?>" name="targetTextbox" value="<?php echo$value->target ?>"> <span class="badge bg-aqua target" id="target_<?php echo$count;?>"><?php echo$value->target ?>%</span></td>
                        <td><span class="badge bg-red"><?php echo $Current.'%';?></span></td>
                        <td><span class="badge bg-yellow" id="required_<?php echo$count;?>">0</span></td>
                        <td><span class="badge bg-yellow" id="pending_<?php echo$count;?>">0</span></td>
                    </tr>
                <?php  $count++; }
                ?>

                <tr style="background-color: #3c8dbc;color: #FFF;">
                    <th>Grand Total</th>
                    <th><?php echo$CountofCall; ?></th>
                    <th><?php echo$Target . '%'; ?></th>
                    <th>100%</th>
                    <th> <span id="total_req"></span> </th>
                    <th><span id="pending_total"></span></th>
                </tr>


            </tbody></table>
    </div>
    <!-- /.box-body -->

</div>