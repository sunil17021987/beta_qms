<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Audit_remarks List          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
              
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 4px"  id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <?php echo anchor(site_url('audit_remarks/create'), 'Create', 'class="btn btn-primary"'); ?>
		<?php echo anchor(site_url('audit_remarks/excel'), 'Excel', 'class="btn btn-primary"'); ?>
	    </div>
        </div>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>
		    <th>Audit Id</th>
		    <th>Agents Id</th>
		    <th>Employee Id TL</th>
		    <th>Employee Id AM</th>
		    <th>Call Date</th>
		    <th>Time Of Call</th>
		    <th>Calling Number</th>
		    <th>CLI Number</th>
		    <th>Call Dur Min</th>
		    <th>Call Dur Sec</th>
		    <th>Call Type</th>
		    <th>Todays Audit Count</th>
		    <th>Category Main Id</th>
		    <th>Category Sub Id</th>
		    <th>Category Main Id Crr</th>
		    <th>Category Sub Id Crr</th>
		    <th>Consumers Concern</th>
		    <th>R G Y A</th>
		    <th>QME Remarks</th>
		    <th>P S If A</th>
		    <th>C T A P P</th>
		    <th>Opening</th>
		    <th>ActiveListening</th>
		    <th>Probing</th>
		    <th>Customer Engagement</th>
		    <th>Empathy Where Required</th>
		    <th>Understanding</th>
		    <th>Professionalism</th>
		    <th>Politeness</th>
		    <th>Hold Procedure</th>
		    <th>Closing</th>
		    <th>Correct Smaller</th>
		    <th>Accurate Complete</th>
		    <th>Fatal Reason</th>
		    <th>Date</th>
		    <th>Status</th>
		    <th>Action</th>
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($audit_remarks_data as $audit_remarks)
            {
                ?>
                <tr>
		    <td><?php echo ++$start ?></td>
		    <td><?php echo $audit_remarks->audit_id ?></td>
		    <td><?php echo $audit_remarks->agents_id ?></td>
		    <td><?php echo $audit_remarks->employee_id_TL ?></td>
		    <td><?php echo $audit_remarks->employee_id_AM ?></td>
		    <td><?php echo $audit_remarks->Call_Date ?></td>
		    <td><?php echo $audit_remarks->Time_Of_Call ?></td>
		    <td><?php echo $audit_remarks->Calling_Number ?></td>
		    <td><?php echo $audit_remarks->CLI_Number ?></td>
		    <td><?php echo $audit_remarks->Call_Dur_Min ?></td>
		    <td><?php echo $audit_remarks->Call_Dur_Sec ?></td>
		    <td><?php echo $audit_remarks->Call_Type ?></td>
		    <td><?php echo $audit_remarks->Todays_Audit_Count ?></td>
		    <td><?php echo $audit_remarks->category_main_id ?></td>
		    <td><?php echo $audit_remarks->category_sub_id ?></td>
		    <td><?php echo $audit_remarks->category_main_id_crr ?></td>
		    <td><?php echo $audit_remarks->category_sub_id_crr ?></td>
		    <td><?php echo $audit_remarks->Consumers_Concern ?></td>
		    <td><?php echo $audit_remarks->r_g_y_a ?></td>
		    <td><?php echo $audit_remarks->QME_Remarks ?></td>
		    <td><?php echo $audit_remarks->p_s_if_a ?></td>
		    <td><?php echo $audit_remarks->c_t_a_p_p ?></td>
		    <td><?php echo $audit_remarks->Opening ?></td>
		    <td><?php echo $audit_remarks->ActiveListening ?></td>
		    <td><?php echo $audit_remarks->Probing ?></td>
		    <td><?php echo $audit_remarks->Customer_Engagement ?></td>
		    <td><?php echo $audit_remarks->Empathy_where_required ?></td>
		    <td><?php echo $audit_remarks->Understanding ?></td>
		    <td><?php echo $audit_remarks->Professionalism ?></td>
		    <td><?php echo $audit_remarks->Politeness ?></td>
		    <td><?php echo $audit_remarks->Hold_Procedure ?></td>
		    <td><?php echo $audit_remarks->Closing ?></td>
		    <td><?php echo $audit_remarks->Correct_smaller ?></td>
		    <td><?php echo $audit_remarks->Accurate_Complete ?></td>
		    <td><?php echo $audit_remarks->Fatal_Reason ?></td>
		    <td><?php echo $audit_remarks->date ?></td>
		    <td><?php echo $audit_remarks->status ?></td>
		    <td style="text-align:center" width="200px">
			<?php 
			echo anchor(site_url('audit_remarks/read/'.$audit_remarks->id),'<i class="fa fa-eye"></i>'); 
			echo ' | '; 
			echo anchor(site_url('audit_remarks/update/'.$audit_remarks->id),'<i class="fa fa-pencil-square-o"></i>'); 
			echo ' | '; 
			echo anchor(site_url('audit_remarks/delete/'.$audit_remarks->id),'<i class="fa fa-trash-o" aria-hidden="true"></i>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
			?>
		    </td>
	        </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#mytable").dataTable();
            });
        </script>
    <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>