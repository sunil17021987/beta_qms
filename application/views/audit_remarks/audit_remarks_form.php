<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Audit_remarks <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">Audit Id <?php echo form_error('audit_id') ?></label>
            <input type="text" class="form-control" name="audit_id" id="audit_id" placeholder="Audit Id" value="<?php echo $audit_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Agents Id <?php echo form_error('agents_id') ?></label>
            <input type="text" class="form-control" name="agents_id" id="agents_id" placeholder="Agents Id" value="<?php echo $agents_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Employee Id TL <?php echo form_error('employee_id_TL') ?></label>
            <input type="text" class="form-control" name="employee_id_TL" id="employee_id_TL" placeholder="Employee Id TL" value="<?php echo $employee_id_TL; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Employee Id AM <?php echo form_error('employee_id_AM') ?></label>
            <input type="text" class="form-control" name="employee_id_AM" id="employee_id_AM" placeholder="Employee Id AM" value="<?php echo $employee_id_AM; ?>" />
        </div>
	    <div class="form-group">
            <label for="date">Call Date <?php echo form_error('Call_Date') ?></label>
            <input type="text" class="form-control" name="Call_Date" id="Call_Date" placeholder="Call Date" value="<?php echo $Call_Date; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Time Of Call <?php echo form_error('Time_Of_Call') ?></label>
            <input type="text" class="form-control" name="Time_Of_Call" id="Time_Of_Call" placeholder="Time Of Call" value="<?php echo $Time_Of_Call; ?>" />
        </div>
	    <div class="form-group">
            <label for="bigint">Calling Number <?php echo form_error('Calling_Number') ?></label>
            <input type="text" class="form-control" name="Calling_Number" id="Calling_Number" placeholder="Calling Number" value="<?php echo $Calling_Number; ?>" />
        </div>
	    <div class="form-group">
            <label for="bigint">CLI Number <?php echo form_error('CLI_Number') ?></label>
            <input type="text" class="form-control" name="CLI_Number" id="CLI_Number" placeholder="CLI Number" value="<?php echo $CLI_Number; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Call Dur Min <?php echo form_error('Call_Dur_Min') ?></label>
            <input type="text" class="form-control" name="Call_Dur_Min" id="Call_Dur_Min" placeholder="Call Dur Min" value="<?php echo $Call_Dur_Min; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Call Dur Sec <?php echo form_error('Call_Dur_Sec') ?></label>
            <input type="text" class="form-control" name="Call_Dur_Sec" id="Call_Dur_Sec" placeholder="Call Dur Sec" value="<?php echo $Call_Dur_Sec; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Call Type <?php echo form_error('Call_Type') ?></label>
            <input type="text" class="form-control" name="Call_Type" id="Call_Type" placeholder="Call Type" value="<?php echo $Call_Type; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Todays Audit Count <?php echo form_error('Todays_Audit_Count') ?></label>
            <input type="text" class="form-control" name="Todays_Audit_Count" id="Todays_Audit_Count" placeholder="Todays Audit Count" value="<?php echo $Todays_Audit_Count; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Main Id <?php echo form_error('category_main_id') ?></label>
            <input type="text" class="form-control" name="category_main_id" id="category_main_id" placeholder="Category Main Id" value="<?php echo $category_main_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Id <?php echo form_error('category_sub_id') ?></label>
            <input type="text" class="form-control" name="category_sub_id" id="category_sub_id" placeholder="Category Sub Id" value="<?php echo $category_sub_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Main Id Crr <?php echo form_error('category_main_id_crr') ?></label>
            <input type="text" class="form-control" name="category_main_id_crr" id="category_main_id_crr" placeholder="Category Main Id Crr" value="<?php echo $category_main_id_crr; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Sub Id Crr <?php echo form_error('category_sub_id_crr') ?></label>
            <input type="text" class="form-control" name="category_sub_id_crr" id="category_sub_id_crr" placeholder="Category Sub Id Crr" value="<?php echo $category_sub_id_crr; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Consumers Concern <?php echo form_error('Consumers_Concern') ?></label>
            <input type="text" class="form-control" name="Consumers_Concern" id="Consumers_Concern" placeholder="Consumers Concern" value="<?php echo $Consumers_Concern; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">R G Y A <?php echo form_error('r_g_y_a') ?></label>
            <input type="text" class="form-control" name="r_g_y_a" id="r_g_y_a" placeholder="R G Y A" value="<?php echo $r_g_y_a; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">QME Remarks <?php echo form_error('QME_Remarks') ?></label>
            <input type="text" class="form-control" name="QME_Remarks" id="QME_Remarks" placeholder="QME Remarks" value="<?php echo $QME_Remarks; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">P S If A <?php echo form_error('p_s_if_a') ?></label>
            <input type="text" class="form-control" name="p_s_if_a" id="p_s_if_a" placeholder="P S If A" value="<?php echo $p_s_if_a; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">C T A P P <?php echo form_error('c_t_a_p_p') ?></label>
            <input type="text" class="form-control" name="c_t_a_p_p" id="c_t_a_p_p" placeholder="C T A P P" value="<?php echo $c_t_a_p_p; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Opening <?php echo form_error('Opening') ?></label>
            <input type="text" class="form-control" name="Opening" id="Opening" placeholder="Opening" value="<?php echo $Opening; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">ActiveListening <?php echo form_error('ActiveListening') ?></label>
            <input type="text" class="form-control" name="ActiveListening" id="ActiveListening" placeholder="ActiveListening" value="<?php echo $ActiveListening; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Probing <?php echo form_error('Probing') ?></label>
            <input type="text" class="form-control" name="Probing" id="Probing" placeholder="Probing" value="<?php echo $Probing; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Customer Engagement <?php echo form_error('Customer_Engagement') ?></label>
            <input type="text" class="form-control" name="Customer_Engagement" id="Customer_Engagement" placeholder="Customer Engagement" value="<?php echo $Customer_Engagement; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Empathy Where Required <?php echo form_error('Empathy_where_required') ?></label>
            <input type="text" class="form-control" name="Empathy_where_required" id="Empathy_where_required" placeholder="Empathy Where Required" value="<?php echo $Empathy_where_required; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Understanding <?php echo form_error('Understanding') ?></label>
            <input type="text" class="form-control" name="Understanding" id="Understanding" placeholder="Understanding" value="<?php echo $Understanding; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Professionalism <?php echo form_error('Professionalism') ?></label>
            <input type="text" class="form-control" name="Professionalism" id="Professionalism" placeholder="Professionalism" value="<?php echo $Professionalism; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Politeness <?php echo form_error('Politeness') ?></label>
            <input type="text" class="form-control" name="Politeness" id="Politeness" placeholder="Politeness" value="<?php echo $Politeness; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Hold Procedure <?php echo form_error('Hold_Procedure') ?></label>
            <input type="text" class="form-control" name="Hold_Procedure" id="Hold_Procedure" placeholder="Hold Procedure" value="<?php echo $Hold_Procedure; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Closing <?php echo form_error('Closing') ?></label>
            <input type="text" class="form-control" name="Closing" id="Closing" placeholder="Closing" value="<?php echo $Closing; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Correct Smaller <?php echo form_error('Correct_smaller') ?></label>
            <input type="text" class="form-control" name="Correct_smaller" id="Correct_smaller" placeholder="Correct Smaller" value="<?php echo $Correct_smaller; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Accurate Complete <?php echo form_error('Accurate_Complete') ?></label>
            <input type="text" class="form-control" name="Accurate_Complete" id="Accurate_Complete" placeholder="Accurate Complete" value="<?php echo $Accurate_Complete; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Fatal Reason <?php echo form_error('Fatal_Reason') ?></label>
            <input type="text" class="form-control" name="Fatal_Reason" id="Fatal_Reason" placeholder="Fatal Reason" value="<?php echo $Fatal_Reason; ?>" />
        </div>
	    <div class="form-group">
            <label for="date">Date <?php echo form_error('date') ?></label>
            <input type="text" class="form-control" name="date" id="date" placeholder="Date" value="<?php echo $date; ?>" />
        </div>
	    <div class="form-group">
            <label for="enum">Status <?php echo form_error('status') ?></label>
            <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('audit_remarks') ?>" class="btn btn-default">Cancel</a>
	</form>
     <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>