<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Category_main <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Category Main Name <?php echo form_error('category_main_name') ?></label>
            <input type="text" class="form-control" name="category_main_name" id="category_main_name" placeholder="Category Main Name" value="<?php echo $category_main_name; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Category Main Time Urban <?php echo form_error('category_main_time_urban') ?></label>
            <input type="text" class="form-control" name="category_main_time_urban" id="category_main_time_urban" placeholder="Category Main Time Urban" value="<?php echo $category_main_time_urban; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Category Main Time Rural <?php echo form_error('category_main_time_rural') ?></label>
            <input type="text" class="form-control" name="category_main_time_rural" id="category_main_time_rural" placeholder="Category Main Time Rural" value="<?php echo $category_main_time_rural; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Category Main Status <?php echo form_error('category_main_status') ?></label>
            <input type="text" class="form-control" name="category_main_status" id="category_main_status" placeholder="Category Main Status" value="<?php echo $category_main_status; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">Category Main Assign Officer Type <?php echo form_error('category_main_assign_officer_type') ?></label>
            <input type="text" class="form-control" name="category_main_assign_officer_type" id="category_main_assign_officer_type" placeholder="Category Main Assign Officer Type" value="<?php echo $category_main_assign_officer_type; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Main Created By <?php echo form_error('category_main_created_by') ?></label>
            <input type="text" class="form-control" name="category_main_created_by" id="category_main_created_by" placeholder="Category Main Created By" value="<?php echo $category_main_created_by; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Category Main Order <?php echo form_error('category_main_order') ?></label>
            <input type="text" class="form-control" name="category_main_order" id="category_main_order" placeholder="Category Main Order" value="<?php echo $category_main_order; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Merge Show <?php echo form_error('merge_show') ?></label>
            <input type="text" class="form-control" name="merge_show" id="merge_show" placeholder="Merge Show" value="<?php echo $merge_show; ?>" />
        </div>
	    <input type="hidden" name="category_main_id" value="<?php echo $category_main_id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('category_main') ?>" class="btn btn-default">Cancel</a>
	</form>
     <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>