<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Category_main List          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
              
            </div>
            <div class="col-md-4 text-center">
                <div style="margin-top: 4px"  id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
            <div class="col-md-4 text-right">
                <?php echo anchor(site_url('category_main/create'), 'Create', 'class="btn btn-primary"'); ?>
		<?php echo anchor(site_url('category_main/excel'), 'Excel', 'class="btn btn-primary"'); ?>
	    </div>
        </div>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>
		    <th>Category Main Name</th>
		    <th>Category Main Time Urban</th>
		    <th>Category Main Time Rural</th>
		    <th>Category Main Status</th>
		    <th>Category Main Assign Officer Type</th>
		    <th>Category Main Created By</th>
		    <th>Category Main Order</th>
		    <th>Merge Show</th>
		    <th>Action</th>
                </tr>
            </thead>
	    <tbody>
            <?php
            $start = 0;
            foreach ($category_main_data as $category_main)
            {
                ?>
                <tr>
		    <td><?php echo ++$start ?></td>
		    <td><?php echo $category_main->category_main_name ?></td>
		    <td><?php echo $category_main->category_main_time_urban ?></td>
		    <td><?php echo $category_main->category_main_time_rural ?></td>
		    <td><?php echo $category_main->category_main_status ?></td>
		    <td><?php echo $category_main->category_main_assign_officer_type ?></td>
		    <td><?php echo $category_main->category_main_created_by ?></td>
		    <td><?php echo $category_main->category_main_order ?></td>
		    <td><?php echo $category_main->merge_show ?></td>
		    <td style="text-align:center" width="200px">
			<?php 
			echo anchor(site_url('category_main/read/'.$category_main->category_main_id),'<i class="fa fa-eye"></i>'); 
			echo ' | '; 
			echo anchor(site_url('category_main/update/'.$category_main->category_main_id),'<i class="fa fa-pencil-square-o"></i>'); 
			echo ' | '; 
			echo anchor(site_url('category_main/delete/'.$category_main->category_main_id),'<i class="fa fa-trash-o" aria-hidden="true"></i>','onclick="javasciprt: return confirm(\'Are You Sure ?\')"'); 
			?>
		    </td>
	        </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js') ?>"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#mytable").dataTable();
            });
        </script>
    <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>