<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Category_main Read          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
        <table class="table">
	    <tr><td><b>Category Main Name</b></td><td><?php echo $category_main_name; ?></td></tr>
	    <tr><td><b>Category Main Time Urban</b></td><td><?php echo $category_main_time_urban; ?></td></tr>
	    <tr><td><b>Category Main Time Rural</b></td><td><?php echo $category_main_time_rural; ?></td></tr>
	    <tr><td><b>Category Main Status</b></td><td><?php echo $category_main_status; ?></td></tr>
	    <tr><td><b>Category Main Assign Officer Type</b></td><td><?php echo $category_main_assign_officer_type; ?></td></tr>
	    <tr><td><b>Category Main Created By</b></td><td><?php echo $category_main_created_by; ?></td></tr>
	    <tr><td><b>Category Main Order</b></td><td><?php echo $category_main_order; ?></td></tr>
	    <tr><td><b>Merge Show</b></td><td><?php echo $merge_show; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('category_main') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
         <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
    </div>