<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Process  <?php echo $button ?>          
            <small></small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!-- ******************/master header end ****************** -->
                        <form action="<?php echo $action; ?>" method="post">
                            <div class="form-group">
                                <label for="varchar">Process Name <?php echo form_error('name') ?></label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                            </div>
                            <div class="form-group">
                                <label for="varchar">User Id <?php echo form_error('user_id') ?></label>
                                <input type="text" class="form-control" name="user_id" id="user_id" placeholder="User Id" value="<?php echo $user_id; ?>" />
                            </div>
                            <div class="form-group">
                                <label for="varchar">Password <?php echo form_error('password') ?></label>
                                <input type="text" class="form-control" name="password" id="password" placeholder="Password" value="<?php echo $password; ?>" />
                            </div>
                            <div class="form-group">
<!--                                <label for="logo">Logo <?php echo form_error('logo') ?></label>-->
                                <input type="hidden" name="logo" value="logo"/>
                              
                            </div>
                            <div class="form-group">
<!--                                <label for="int">User Type <?php echo form_error('user_type') ?></label>-->
                                 <input type="hidden" name="user_type" value="2"/>
<!--                                <select class="form-control" name="user_type">
                                    <option value="">---  Select Category ---</option>
                                    <?php
                                    if (!empty($user_type_data)) {
                                        foreach ($user_type_data as $rl) {
                                            ?>
                                            <option value="<?php echo $rl->id ?>" <?php echo($user_type == $rl->id) ? 'selected' : ''; ?>><?php echo $rl->name ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>-->
                            </div>
                            <div class="form-group">
                                <label for="enum">Status <?php echo form_error('status') ?></label>
                                <select name="status" class="form-control select">
                                    <option value="">--- Select  ---</option>
                                    <option value="1" <?php if ($status == '1') echo 'selected'; ?>>Active</option>
                                    <option value="0" <?php if ($status == '0') echo 'selected'; ?>>Inactive</option>
                                </select>
                            </div>
                            <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
                            <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
                            <a href="<?php echo site_url('process_log') ?>" class="btn btn-default">Cancel</a>
                        </form>
                        <!-- ******************/master footer ****************** -->
                    </div>
                </div>
            </div>
    </section>
</div>