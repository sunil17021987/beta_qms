<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rbac extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Rbac_model');
         $this->load->model('Employee_model');
         $this->load->model('Menubar_model');
          $this->load->model('Menubar_model');
         
        $this->load->library('form_validation');
    }

    public function index()
    {
        $rbac = $this->Rbac_model->get_all_join();
        $employees = $this->Employee_model->get_all();
        $data = array(
            'rbac_data' => $rbac,
            'employees'=>$employees
        );

          $data['content'] = 'rbac/rbac_list';
        $this->load->view('common/master', $data);    
            
    }

    public function read($id) 
    {
        $row = $this->Rbac_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'employee_id' => $row->employee_id,
		'permission' => $row->permission,
	    );
             $data['content'] = 'rbac/rbac_read';
        $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('rbac'));
        }
    }

    public function create() 
    {
          $employees = $this->Employee_model->get_all();
            $menubar = $this->Menubar_model->get_all();
      
        $data = array(
            'button' => 'Create',
            'action' => site_url('rbac/create_action'),
	    'id' => set_value('id'),
	    'employee_id' => set_value('employee_id'),
	    'permission' => array(),
            'employees'=>$employees,
             'menubar_data' => $menubar
	);
        $data['content'] = 'rbac/rbac_form';
        $this->load->view('common/master', $data);       
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'employee_id' => $this->input->post('employee_id',TRUE),
		'permission' =>implode(",",$this->input->post('permission',TRUE)),
	    );

            $this->Rbac_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('rbac'));
        }
    }
    
    public function update($id) 
    {
         $employees = $this->Employee_model->get_all();
            $menubar = $this->Menubar_model->get_all();
        $row = $this->Rbac_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('rbac/update_action'),
		'id' => set_value('id', $row->id),
		'employee_id' => set_value('employee_id', $row->employee_id),
		'permission' => set_value('permission', explode(",",$row->permission)),
                  'employees'=>$employees,
             'menubar_data' => $menubar
	    );
            $data['content'] = 'rbac/rbac_form';
            $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('rbac'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'employee_id' => $this->input->post('employee_id',TRUE),
		'permission' => implode(",",$this->input->post('permission',TRUE)),
	    );

            $this->Rbac_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('rbac'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Rbac_model->get_by_id($id);

        if ($row) {
            $this->Rbac_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('rbac'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('rbac'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('employee_id', 'employee id', 'trim|required');
	$this->form_validation->set_rules('permission[]', 'permission', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "rbac.xls";
        $judul = "rbac";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Employee Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Permission");

	foreach ($this->Rbac_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->employee_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->permission);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Rbac.php */
/* Location: ./application/controllers/Rbac.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2019-09-09 08:44:28 */
