<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Process_log extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Process_log_model');
          $this->load->model('User_type_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
      
         $process_log = $this->Process_log_model->get_all_by_join();
       
        $data = array(
            
            'process_log_data' => $process_log
        );

          $data['content'] = 'process_log/process_log_list';
        $this->load->view('common/master', $data);    
            
    }

    public function read($id) 
    {
        $row = $this->Process_log_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'name' => $row->name,
		'user_id' => $row->user_id,
		'password' => $row->password,
		'logo' => $row->logo,
		'user_type' => $row->user_type,
		'status' => $row->status,
	    );
             $data['content'] = 'process_log/process_log_read';
        $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('process_log'));
        }
    }

    public function create() 
    {
         $user_type = $this->User_type_model->get_all();
        $data = array(
            'button' => 'Create',
            'action' => site_url('process_log/create_action'),
	    'id' => set_value('id'),
	    'name' => set_value('name'),
	    'user_id' => set_value('user_id'),
	    'password' => set_value('password'),
	    'logo' => set_value('logo'),
	    'user_type' => set_value('user_type'),
	    'status' => set_value('status'),
             'user_type_data' => $user_type,
	);
        $data['content'] = 'process_log/process_log_form';
        $this->load->view('common/master', $data);       
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'user_id' => $this->input->post('user_id',TRUE),
		'password' => $this->input->post('password',TRUE),
		'logo' => $this->input->post('logo',TRUE),
		'user_type' => $this->input->post('user_type',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Process_log_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('process_log'));
        }
    }
    
    public function update($id) 
    {
          $user_type = $this->User_type_model->get_all();
        $row = $this->Process_log_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('process_log/update_action'),
		'id' => set_value('id', $row->id),
		'name' => set_value('name', $row->name),
		'user_id' => set_value('user_id', $row->user_id),
		'password' => set_value('password', $row->password),
		'logo' => set_value('logo', $row->logo),
		'user_type' => set_value('user_type', $row->user_type),
		'status' => set_value('status', $row->status),
                  'user_type_data' => $user_type,
	    );
            $data['content'] = 'process_log/process_log_form';
            $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('process_log'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'user_id' => $this->input->post('user_id',TRUE),
		'password' => $this->input->post('password',TRUE),
		'logo' => $this->input->post('logo',TRUE),
		'user_type' => $this->input->post('user_type',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Process_log_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('process_log'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Process_log_model->get_by_id($id);

        if ($row) {
            $this->Process_log_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('process_log'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('process_log'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('user_id', 'user id', 'trim|required');
	$this->form_validation->set_rules('password', 'password', 'trim|required');
	$this->form_validation->set_rules('logo', 'logo', 'trim|required');
	$this->form_validation->set_rules('user_type', 'user type', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}

/* End of file Process_log.php */
/* Location: ./application/controllers/Process_log.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2020-06-04 11:55:08 */
