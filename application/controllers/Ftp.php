<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ftp extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Ftp_model');
        $this->load->library('form_validation');
        $this->load->helper(array('form', 'url'));
        $this->load->library('upload');
    }

    public function index() {
        $ftp = $this->Ftp_model->get_all_by_join();

        $data = array(
            'ftp_data' => $ftp
        );

        $data['content'] = 'ftp/ftp_list';
        $this->load->view('common/master', $data);
    }

    public function read($id) {
        $row = $this->Ftp_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'upload_by' => $row->upload_by,
                'file_name' => $row->file_name,
                'dateof' => $row->dateof,
                'status' => $row->status,
            );
            $data['content'] = 'ftp/ftp_read';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('ftp'));
        }
    }

    public function create() {
        $data = array(
            'button' => 'Create',
            'action' => site_url('ftp/create_action'),
            'id' => set_value('id'),
            'upload_by' => set_value('upload_by'),
            'file_name' => set_value('file_name'),
            'dateof' => set_value('dateof'),
            'status' => set_value('status'),
        );
        $data['content'] = 'ftp/ftp_form';
        $this->load->view('common/master', $data);
    }

    public function create_action() {
        $this->_rules_new();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {

            $config['upload_path'] = '././uploads/ftp/';
            $config['allowed_types'] = 'xls|xlsx|csv|pdf|txt|ods|doc|docx';

            $this->upload->initialize($config);
            // $this->load->library('upload', $config);
            if ($this->upload->do_upload('file')) {
                $udata = array('upload_data' => $this->upload->data());
                $reg_id = $this->session->userdata('reg_id');
                $data = array(
                    'upload_by' => $reg_id,
                    'file_name' => 'uploads/ftp/' . $udata['upload_data']['file_name'],
                    'filename' => $udata['upload_data']['file_name'],
                    'fileurl' => 'uploads/ftp/' . $udata['upload_data']['file_name'],
                );

                $this->Ftp_model->insert($data);
            } else {
                $error = array('error' => $this->upload->display_errors());


                $this->session->set_flashdata('message', 'File Type Not Allowed please Try Again...');
                redirect(site_url('ftp'));
            }






            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('ftp'));
        }
    }

    public function update($id) {
        $row = $this->Ftp_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('ftp/update_action'),
                'id' => set_value('id', $row->id),
                'upload_by' => set_value('upload_by', $row->upload_by),
                'file_name' => set_value('file_name', $row->file_name),
                'dateof' => set_value('dateof', $row->dateof),
                'status' => set_value('status', $row->status),
            );
            $data['content'] = 'ftp/ftp_form';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('ftp'));
        }
    }

    public function update_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'upload_by' => $this->input->post('upload_by', TRUE),
                'file_name' => $this->input->post('file_name', TRUE),
                'dateof' => $this->input->post('dateof', TRUE),
                'status' => $this->input->post('status', TRUE),
            );

            $this->Ftp_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('ftp'));
        }
    }

    public function delete($id) {
        $row = $this->Ftp_model->get_by_id($id);

        if ($row) {
            $file_pointer = $row->file_name;
            if (unlink($file_pointer)) {
                $this->Ftp_model->delete($id);
            }


            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('ftp'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('ftp'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('upload_by', 'upload by', 'trim|required');
        $this->form_validation->set_rules('file_name', 'file name', 'trim|required');
        $this->form_validation->set_rules('dateof', 'dateof', 'trim|required');
        $this->form_validation->set_rules('status', 'status', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function _rules_new() {
        // $this->form_validation->set_rules('upload_by', 'upload by', 'trim|required');
        //  $this->form_validation->set_rules('file', 'file name', 'trim|required');
        // $this->form_validation->set_rules('dateof', 'dateof', 'trim|required');
        // $this->form_validation->set_rules('status', 'status', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function donwload() {
        $id = $this->uri->segment(3);
        $row = $this->Ftp_model->get_by_id($id);

        $this->load->helper('download');
        $data = file_get_contents($row->file_name);
        force_download($row->filename, $data);
    }

}

/* End of file Ftp.php */
/* Location: ./application/controllers/Ftp.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2019-11-14 21:48:08 */
