<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agents_1 extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Agents_model');
         $this->load->model('Employee_model');
        $this->load->library('form_validation');
         $this->load->helper('url');
            $this->load->helper('file');
    }

    public function index()
    {
      

        $data['action'] = site_url('Agents_1/database_backup');
        $this->load->view('backup',$data);    
         
    }
    function database_backup()
    {
//        $this->db = $this->load->database('audit_soft', TRUE);
       // 'M:\xampp\htdocs\tt\text.txt'
            $this->load->dbutil();
           
            $prefs = array('format' => 'zip', 'filename' => 'Database-backup_' . date('Y-m-d_H-i'));
            $backup = $this->dbutil->backup($prefs);
           $folder = base_url() . 'uploads/';
            if (!write_file('M:\xampp\htdocs\audit_soft\BD-backup_' . date('Y-m-d_H-i') . '.zip', $backup)) {
                echo "Error while creating auto database backup!";
             } 
             else {
                echo "Database backup has been successfully Created";
            }
    }

    public function read($id) 
    {
        $row = $this->Agents_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'emp_id' => $row->emp_id,
		'name' => $row->name,
		'employee_id_TL' => $row->employee_id_TL,
		'employee_id_AM' => $row->employee_id_AM,
		'shift_id' => $row->shift_id,
		'DOJ' => $row->DOJ,
		'process' => $row->process,
		'gender' => $row->gender,
		'batch_no' => $row->batch_no,
		'status' => $row->status,
	    );
             $data['content'] = 'agents/agents_read';
        $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('agents'));
        }
    }

    public function create() 
    {
        
         $employee = $this->Employee_model->get_all();

      
        $data = array(
            'button' => 'Create',
            'action' => site_url('agents/create_action'),
	    'id' => set_value('id'),
	    'emp_id' => set_value('emp_id'),
	    'name' => set_value('name'),
	    'employee_id_TL' => set_value('employee_id_TL'),
	    'employee_id_AM' => set_value('employee_id_AM'),
	    'shift_id' => set_value('shift_id'),
	    'DOJ' => set_value('DOJ'),
	    'process' => set_value('process'),
	    'gender' => set_value('gender'),
	    'batch_no' => set_value('batch_no'),
	    'status' => set_value('status'),
             'employee_data' => $employee
	);
        $data['content'] = 'agents/agents_form';
        $this->load->view('common/master', $data);       
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'emp_id' => $this->input->post('emp_id',TRUE),
		'name' => $this->input->post('name',TRUE),
		'employee_id_TL' => $this->input->post('employee_id_TL',TRUE),
		'employee_id_AM' => $this->input->post('employee_id_AM',TRUE),
		'shift_id' => $this->input->post('shift_id',TRUE),
		'DOJ' => $this->input->post('DOJ',TRUE),
		'process' => $this->input->post('process',TRUE),
		'gender' => $this->input->post('gender',TRUE),
		'batch_no' => $this->input->post('batch_no',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Agents_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('agents'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Agents_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('agents/update_action'),
		'id' => set_value('id', $row->id),
		'emp_id' => set_value('emp_id', $row->emp_id),
		'name' => set_value('name', $row->name),
		'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
		'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
		'shift_id' => set_value('shift_id', $row->shift_id),
		'DOJ' => set_value('DOJ', $row->DOJ),
		'process' => set_value('process', $row->process),
		'gender' => set_value('gender', $row->gender),
		'batch_no' => set_value('batch_no', $row->batch_no),
		'status' => set_value('status', $row->status),
	    );
            $data['content'] = 'agents/agents_form';
            $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('agents'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'emp_id' => $this->input->post('emp_id',TRUE),
		'name' => $this->input->post('name',TRUE),
		'employee_id_TL' => $this->input->post('employee_id_TL',TRUE),
		'employee_id_AM' => $this->input->post('employee_id_AM',TRUE),
		'shift_id' => $this->input->post('shift_id',TRUE),
		'DOJ' => $this->input->post('DOJ',TRUE),
		'process' => $this->input->post('process',TRUE),
		'gender' => $this->input->post('gender',TRUE),
		'batch_no' => $this->input->post('batch_no',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Agents_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('agents'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Agents_model->get_by_id($id);

        if ($row) {
            $this->Agents_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('agents'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('agents'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('emp_id', 'emp id', 'trim|required');
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('employee_id_TL', 'employee id tl', 'trim|required');
	$this->form_validation->set_rules('employee_id_AM', 'employee id am', 'trim|required');
	$this->form_validation->set_rules('shift_id', 'shift id', 'trim|required');
	$this->form_validation->set_rules('DOJ', 'doj', 'trim|required');
	$this->form_validation->set_rules('process', 'process', 'trim|required');
	$this->form_validation->set_rules('gender', 'gender', 'trim|required');
	$this->form_validation->set_rules('batch_no', 'batch no', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "agents.xls";
        $judul = "agents";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Emp Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Name");
	xlsWriteLabel($tablehead, $kolomhead++, "Employee Id TL");
	xlsWriteLabel($tablehead, $kolomhead++, "Employee Id AM");
	xlsWriteLabel($tablehead, $kolomhead++, "Shift Id");
	xlsWriteLabel($tablehead, $kolomhead++, "DOJ");
	xlsWriteLabel($tablehead, $kolomhead++, "Process");
	xlsWriteLabel($tablehead, $kolomhead++, "Gender");
	xlsWriteLabel($tablehead, $kolomhead++, "Batch No");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");

	foreach ($this->Agents_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->emp_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->name);
	    xlsWriteNumber($tablebody, $kolombody++, $data->employee_id_TL);
	    xlsWriteNumber($tablebody, $kolombody++, $data->employee_id_AM);
	    xlsWriteNumber($tablebody, $kolombody++, $data->shift_id);
	    xlsWriteLabel($tablebody, $kolombody++, $data->DOJ);
	    xlsWriteLabel($tablebody, $kolombody++, $data->process);
	    xlsWriteNumber($tablebody, $kolombody++, $data->gender);
	    xlsWriteNumber($tablebody, $kolombody++, $data->batch_no);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Agents.php */
/* Location: ./application/controllers/Agents.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2019-08-02 12:35:42 */
