<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agentfeedback extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Agentfeedback_model');
        $this->load->library('form_validation');
    }

    public function index() {
        $agentfeedback = $this->Agentfeedback_model->get_all();

        $data = array(
            'agentfeedback_data' => $agentfeedback
        );

        $data['content'] = 'agentfeedback/agentfeedback_list';
        $this->load->view('common/master', $data);
    }

    public function read($id) {
        $row = $this->Agentfeedback_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'audit_id' => $row->audit_id,
                'Auditor' => $row->Auditor,
                'Accepted' => $row->Accepted,
                'date_of' => $row->date_of,
            );
            $data['content'] = 'agentfeedback/agentfeedback_read';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('agentfeedback'));
        }
    }

    public function create() {
        $data = array(
            'button' => 'Create',
            'action' => site_url('agentfeedback/create_action'),
            'id' => set_value('id'),
            'audit_id' => set_value('audit_id'),
            'Auditor' => set_value('Auditor'),
            'Accepted' => set_value('Accepted'),
            'date_of' => set_value('date_of'),
        );
        $data['content'] = 'agentfeedback/agentfeedback_form';
        $this->load->view('common/master', $data);
    }

    public function create_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'audit_id' => $this->input->post('audit_id', TRUE),
                'Auditor' => $this->input->post('Auditor', TRUE),
                'Accepted' => $this->input->post('Accepted', TRUE),
                'date_of' => $this->input->post('date_of', TRUE),
            );

            $this->Agentfeedback_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('agentfeedback'));
        }
    }

    public function create_action_ajax() {

        $data = array(
            'audit_id' => $this->input->post('audit_id', TRUE),
            'Auditor' => $this->input->post('Auditor', TRUE),
            'Accepted' => $this->input->post('Accepted', TRUE),
        );
        $row = $this->Agentfeedback_model->get_by_audit_id($this->input->post('audit_id', TRUE));
        if ($row) {
             $insert_id = $this->Agentfeedback_model->update_audit_id($this->input->post('audit_id', TRUE), $data);
        } else {
            $insert_id = $this->Agentfeedback_model->insert($data);
        }

        if ($insert_id > 0) {
            echo"Success";
        } else {
            echo"Error";
        }
    }

    public function update($id) {
        $row = $this->Agentfeedback_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('agentfeedback/update_action'),
                'id' => set_value('id', $row->id),
                'audit_id' => set_value('audit_id', $row->audit_id),
                'Auditor' => set_value('Auditor', $row->Auditor),
                'Accepted' => set_value('Accepted', $row->Accepted),
                'date_of' => set_value('date_of', $row->date_of),
            );
            $data['content'] = 'agentfeedback/agentfeedback_form';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('agentfeedback'));
        }
    }

    public function update_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
                'audit_id' => $this->input->post('audit_id', TRUE),
                'Auditor' => $this->input->post('Auditor', TRUE),
                'Accepted' => $this->input->post('Accepted', TRUE),
                'date_of' => $this->input->post('date_of', TRUE),
            );

            $this->Agentfeedback_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('agentfeedback'));
        }
    }

    public function delete($id) {
        $row = $this->Agentfeedback_model->get_by_id($id);

        if ($row) {
            $this->Agentfeedback_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('agentfeedback'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('agentfeedback'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('audit_id', 'audit id', 'trim|required');
        $this->form_validation->set_rules('Auditor', 'auditor', 'trim|required');
        $this->form_validation->set_rules('Accepted', 'accepted', 'trim|required');
        $this->form_validation->set_rules('date_of', 'date of', 'trim|required');

        $this->form_validation->set_rules('id', 'id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel() {
        $this->load->helper('exportexcel');
        $namaFile = "agentfeedback.xls";
        $judul = "agentfeedback";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Audit Id");
        xlsWriteLabel($tablehead, $kolomhead++, "Auditor");
        xlsWriteLabel($tablehead, $kolomhead++, "Accepted");
        xlsWriteLabel($tablehead, $kolomhead++, "Date Of");

        foreach ($this->Agentfeedback_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteNumber($tablebody, $kolombody++, $data->audit_id);
            xlsWriteLabel($tablebody, $kolombody++, $data->Auditor);
            xlsWriteLabel($tablebody, $kolombody++, $data->Accepted);
            xlsWriteLabel($tablebody, $kolombody++, $data->date_of);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Agentfeedback.php */
/* Location: ./application/controllers/Agentfeedback.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2019-08-29 15:07:40 */
