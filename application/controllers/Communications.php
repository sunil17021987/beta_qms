<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Communications extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Communications_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
         $process_log_id=$this->session->userdata('id');
        $communications = $this->Communications_model->get_all_by_process_log_id($process_log_id);

        $data = array(
            'communications_data' => $communications
        );

          $data['content'] = 'communications/communications_list';
        $this->load->view('common/master', $data);    
            
    }

    public function read($id) 
    {
        $row = $this->Communications_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'title' => $row->title,
		'options' => $row->options,
		'status' => $row->status,
	    );
             $data['content'] = 'communications/communications_read';
        $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('communications'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('communications/create_action'),
	    'id' => set_value('id'),
	    'title' => set_value('title'),
	    'options' => set_value('options'),
             'score' => set_value('score'),
	    'status' => set_value('status'),
            'option_type' => set_value('option_type'),
	);
        $data['content'] = 'communications/communications_form';
        $this->load->view('common/master', $data);       
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $process_log_id=$this->session->userdata('id');
            $data = array(
                'process_log_id' =>$process_log_id,
		'title' => $this->input->post('title',TRUE),
		'options' => $this->input->post('options',TRUE),
                'score' => $this->input->post('score',TRUE),
		'status' => $this->input->post('status',TRUE),
                'option_type' => $this->input->post('option_type', TRUE),
	    );

            $this->Communications_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('communications'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Communications_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('communications/update_action'),
		'id' => set_value('id', $row->id),
		'title' => set_value('title', $row->title),
		'options' => set_value('options', $row->options),
                'score' => set_value('score', $row->score),
		'status' => set_value('status', $row->status),
                  'option_type' => set_value('option_type', $row->option_type),
	    );
            $data['content'] = 'communications/communications_form';
            $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('communications'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'title' => $this->input->post('title',TRUE),
		'options' => $this->input->post('options',TRUE),
                'score' => $this->input->post('score',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Communications_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('communications'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Communications_model->get_by_id($id);

        if ($row) {
            $this->Communications_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('communications'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('communications'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('title', 'title', 'trim|required');
	$this->form_validation->set_rules('options', 'options', 'trim|required');
        $this->form_validation->set_rules('score', 'score', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "communications.xls";
        $judul = "communications";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Title");
	xlsWriteLabel($tablehead, $kolomhead++, "Options");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");

	foreach ($this->Communications_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->title);
	    xlsWriteLabel($tablebody, $kolombody++, $data->options);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Communications.php */
/* Location: ./application/controllers/Communications.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2019-08-07 12:19:33 */
