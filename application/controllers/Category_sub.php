<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category_sub extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('Category_sub_model');
        $this->load->library('form_validation');
    }

    public function index() {
        $category_sub = $this->Category_sub_model->get_all();

        $data = array(
            'category_sub_data' => $category_sub
        );

        $data['content'] = 'category_sub/category_sub_list';
        $this->load->view('common/master', $data);
    }

    public function read($id) {
        $row = $this->Category_sub_model->get_by_id($id);
        if ($row) {
            $data = array(
                'category_sub_id' => $row->category_sub_id,
                'category_sub_main_id' => $row->category_sub_main_id,
                'category_sub_name' => $row->category_sub_name,
                'category_sub_created_by' => $row->category_sub_created_by,
                'category_sub_time' => $row->category_sub_time,
                'category_sub_level1_urban' => $row->category_sub_level1_urban,
                'category_sub_level1_rural' => $row->category_sub_level1_rural,
                'category_sub_level2_urban' => $row->category_sub_level2_urban,
                'category_sub_level2_rural' => $row->category_sub_level2_rural,
                'category_sub_level3_urban' => $row->category_sub_level3_urban,
                'category_sub_level3_rural' => $row->category_sub_level3_rural,
                'category_sub_level4_urban' => $row->category_sub_level4_urban,
                'category_sub_level4_rural' => $row->category_sub_level4_rural,
                'category_sub_level1_urban_time' => $row->category_sub_level1_urban_time,
                'category_sub_level1_rural_time' => $row->category_sub_level1_rural_time,
                'category_sub_level2_urban_time' => $row->category_sub_level2_urban_time,
                'category_sub_level2_rural_time' => $row->category_sub_level2_rural_time,
                'category_sub_level3_urban_time' => $row->category_sub_level3_urban_time,
                'category_sub_level3_rural_time' => $row->category_sub_level3_rural_time,
                'category_sub_level4_urban_time' => $row->category_sub_level4_urban_time,
                'category_sub_level4_rural_time' => $row->category_sub_level4_rural_time,
                'escalate_upto' => $row->escalate_upto,
                'category_sub_app' => $row->category_sub_app,
                'category_sub_status' => $row->category_sub_status,
            );
            $data['content'] = 'category_sub/category_sub_read';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('category_sub'));
        }
    }

    public function create() {
        $data = array(
            'button' => 'Create',
            'action' => site_url('category_sub/create_action'),
            'category_sub_id' => set_value('category_sub_id'),
            'category_sub_main_id' => set_value('category_sub_main_id'),
            'category_sub_name' => set_value('category_sub_name'),
            'category_sub_created_by' => set_value('category_sub_created_by'),
            'category_sub_time' => set_value('category_sub_time'),
            'category_sub_level1_urban' => set_value('category_sub_level1_urban'),
            'category_sub_level1_rural' => set_value('category_sub_level1_rural'),
            'category_sub_level2_urban' => set_value('category_sub_level2_urban'),
            'category_sub_level2_rural' => set_value('category_sub_level2_rural'),
            'category_sub_level3_urban' => set_value('category_sub_level3_urban'),
            'category_sub_level3_rural' => set_value('category_sub_level3_rural'),
            'category_sub_level4_urban' => set_value('category_sub_level4_urban'),
            'category_sub_level4_rural' => set_value('category_sub_level4_rural'),
            'category_sub_level1_urban_time' => set_value('category_sub_level1_urban_time'),
            'category_sub_level1_rural_time' => set_value('category_sub_level1_rural_time'),
            'category_sub_level2_urban_time' => set_value('category_sub_level2_urban_time'),
            'category_sub_level2_rural_time' => set_value('category_sub_level2_rural_time'),
            'category_sub_level3_urban_time' => set_value('category_sub_level3_urban_time'),
            'category_sub_level3_rural_time' => set_value('category_sub_level3_rural_time'),
            'category_sub_level4_urban_time' => set_value('category_sub_level4_urban_time'),
            'category_sub_level4_rural_time' => set_value('category_sub_level4_rural_time'),
            'escalate_upto' => set_value('escalate_upto'),
            'category_sub_app' => set_value('category_sub_app'),
            'category_sub_status' => set_value('category_sub_status'),
        );
        $data['content'] = 'category_sub/category_sub_form';
        $this->load->view('common/master', $data);
    }

    public function create_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'category_sub_main_id' => $this->input->post('category_sub_main_id', TRUE),
                'category_sub_name' => $this->input->post('category_sub_name', TRUE),
                'category_sub_created_by' => $this->input->post('category_sub_created_by', TRUE),
                'category_sub_time' => $this->input->post('category_sub_time', TRUE),
                'category_sub_level1_urban' => $this->input->post('category_sub_level1_urban', TRUE),
                'category_sub_level1_rural' => $this->input->post('category_sub_level1_rural', TRUE),
                'category_sub_level2_urban' => $this->input->post('category_sub_level2_urban', TRUE),
                'category_sub_level2_rural' => $this->input->post('category_sub_level2_rural', TRUE),
                'category_sub_level3_urban' => $this->input->post('category_sub_level3_urban', TRUE),
                'category_sub_level3_rural' => $this->input->post('category_sub_level3_rural', TRUE),
                'category_sub_level4_urban' => $this->input->post('category_sub_level4_urban', TRUE),
                'category_sub_level4_rural' => $this->input->post('category_sub_level4_rural', TRUE),
                'category_sub_level1_urban_time' => $this->input->post('category_sub_level1_urban_time', TRUE),
                'category_sub_level1_rural_time' => $this->input->post('category_sub_level1_rural_time', TRUE),
                'category_sub_level2_urban_time' => $this->input->post('category_sub_level2_urban_time', TRUE),
                'category_sub_level2_rural_time' => $this->input->post('category_sub_level2_rural_time', TRUE),
                'category_sub_level3_urban_time' => $this->input->post('category_sub_level3_urban_time', TRUE),
                'category_sub_level3_rural_time' => $this->input->post('category_sub_level3_rural_time', TRUE),
                'category_sub_level4_urban_time' => $this->input->post('category_sub_level4_urban_time', TRUE),
                'category_sub_level4_rural_time' => $this->input->post('category_sub_level4_rural_time', TRUE),
                'escalate_upto' => $this->input->post('escalate_upto', TRUE),
                'category_sub_app' => $this->input->post('category_sub_app', TRUE),
                'category_sub_status' => $this->input->post('category_sub_status', TRUE),
            );

            $this->Category_sub_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('category_sub'));
        }
    }

    public function update($id) {
        $row = $this->Category_sub_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('category_sub/update_action'),
                'category_sub_id' => set_value('category_sub_id', $row->category_sub_id),
                'category_sub_main_id' => set_value('category_sub_main_id', $row->category_sub_main_id),
                'category_sub_name' => set_value('category_sub_name', $row->category_sub_name),
                'category_sub_created_by' => set_value('category_sub_created_by', $row->category_sub_created_by),
                'category_sub_time' => set_value('category_sub_time', $row->category_sub_time),
                'category_sub_level1_urban' => set_value('category_sub_level1_urban', $row->category_sub_level1_urban),
                'category_sub_level1_rural' => set_value('category_sub_level1_rural', $row->category_sub_level1_rural),
                'category_sub_level2_urban' => set_value('category_sub_level2_urban', $row->category_sub_level2_urban),
                'category_sub_level2_rural' => set_value('category_sub_level2_rural', $row->category_sub_level2_rural),
                'category_sub_level3_urban' => set_value('category_sub_level3_urban', $row->category_sub_level3_urban),
                'category_sub_level3_rural' => set_value('category_sub_level3_rural', $row->category_sub_level3_rural),
                'category_sub_level4_urban' => set_value('category_sub_level4_urban', $row->category_sub_level4_urban),
                'category_sub_level4_rural' => set_value('category_sub_level4_rural', $row->category_sub_level4_rural),
                'category_sub_level1_urban_time' => set_value('category_sub_level1_urban_time', $row->category_sub_level1_urban_time),
                'category_sub_level1_rural_time' => set_value('category_sub_level1_rural_time', $row->category_sub_level1_rural_time),
                'category_sub_level2_urban_time' => set_value('category_sub_level2_urban_time', $row->category_sub_level2_urban_time),
                'category_sub_level2_rural_time' => set_value('category_sub_level2_rural_time', $row->category_sub_level2_rural_time),
                'category_sub_level3_urban_time' => set_value('category_sub_level3_urban_time', $row->category_sub_level3_urban_time),
                'category_sub_level3_rural_time' => set_value('category_sub_level3_rural_time', $row->category_sub_level3_rural_time),
                'category_sub_level4_urban_time' => set_value('category_sub_level4_urban_time', $row->category_sub_level4_urban_time),
                'category_sub_level4_rural_time' => set_value('category_sub_level4_rural_time', $row->category_sub_level4_rural_time),
                'escalate_upto' => set_value('escalate_upto', $row->escalate_upto),
                'category_sub_app' => set_value('category_sub_app', $row->category_sub_app),
                'category_sub_status' => set_value('category_sub_status', $row->category_sub_status),
            );
            $data['content'] = 'category_sub/category_sub_form';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('category_sub'));
        }
    }

    public function update_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('category_sub_id', TRUE));
        } else {
            $data = array(
                'category_sub_main_id' => $this->input->post('category_sub_main_id', TRUE),
                'category_sub_name' => $this->input->post('category_sub_name', TRUE),
                'category_sub_created_by' => $this->input->post('category_sub_created_by', TRUE),
                'category_sub_time' => $this->input->post('category_sub_time', TRUE),
                'category_sub_level1_urban' => $this->input->post('category_sub_level1_urban', TRUE),
                'category_sub_level1_rural' => $this->input->post('category_sub_level1_rural', TRUE),
                'category_sub_level2_urban' => $this->input->post('category_sub_level2_urban', TRUE),
                'category_sub_level2_rural' => $this->input->post('category_sub_level2_rural', TRUE),
                'category_sub_level3_urban' => $this->input->post('category_sub_level3_urban', TRUE),
                'category_sub_level3_rural' => $this->input->post('category_sub_level3_rural', TRUE),
                'category_sub_level4_urban' => $this->input->post('category_sub_level4_urban', TRUE),
                'category_sub_level4_rural' => $this->input->post('category_sub_level4_rural', TRUE),
                'category_sub_level1_urban_time' => $this->input->post('category_sub_level1_urban_time', TRUE),
                'category_sub_level1_rural_time' => $this->input->post('category_sub_level1_rural_time', TRUE),
                'category_sub_level2_urban_time' => $this->input->post('category_sub_level2_urban_time', TRUE),
                'category_sub_level2_rural_time' => $this->input->post('category_sub_level2_rural_time', TRUE),
                'category_sub_level3_urban_time' => $this->input->post('category_sub_level3_urban_time', TRUE),
                'category_sub_level3_rural_time' => $this->input->post('category_sub_level3_rural_time', TRUE),
                'category_sub_level4_urban_time' => $this->input->post('category_sub_level4_urban_time', TRUE),
                'category_sub_level4_rural_time' => $this->input->post('category_sub_level4_rural_time', TRUE),
                'escalate_upto' => $this->input->post('escalate_upto', TRUE),
                'category_sub_app' => $this->input->post('category_sub_app', TRUE),
                'category_sub_status' => $this->input->post('category_sub_status', TRUE),
            );

            $this->Category_sub_model->update($this->input->post('category_sub_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('category_sub'));
        }
    }

    public function delete($id) {
        $row = $this->Category_sub_model->get_by_id($id);

        if ($row) {
            $this->Category_sub_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('category_sub'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('category_sub'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('category_sub_main_id', 'category sub main id', 'trim|required');
        $this->form_validation->set_rules('category_sub_name', 'category sub name', 'trim|required');
        $this->form_validation->set_rules('category_sub_created_by', 'category sub created by', 'trim|required');
        $this->form_validation->set_rules('category_sub_time', 'category sub time', 'trim|required');
        $this->form_validation->set_rules('category_sub_level1_urban', 'category sub level1 urban', 'trim|required');
        $this->form_validation->set_rules('category_sub_level1_rural', 'category sub level1 rural', 'trim|required');
        $this->form_validation->set_rules('category_sub_level2_urban', 'category sub level2 urban', 'trim|required');
        $this->form_validation->set_rules('category_sub_level2_rural', 'category sub level2 rural', 'trim|required');
        $this->form_validation->set_rules('category_sub_level3_urban', 'category sub level3 urban', 'trim|required');
        $this->form_validation->set_rules('category_sub_level3_rural', 'category sub level3 rural', 'trim|required');
        $this->form_validation->set_rules('category_sub_level4_urban', 'category sub level4 urban', 'trim|required');
        $this->form_validation->set_rules('category_sub_level4_rural', 'category sub level4 rural', 'trim|required');
        $this->form_validation->set_rules('category_sub_level1_urban_time', 'category sub level1 urban time', 'trim|required');
        $this->form_validation->set_rules('category_sub_level1_rural_time', 'category sub level1 rural time', 'trim|required');
        $this->form_validation->set_rules('category_sub_level2_urban_time', 'category sub level2 urban time', 'trim|required');
        $this->form_validation->set_rules('category_sub_level2_rural_time', 'category sub level2 rural time', 'trim|required');
        $this->form_validation->set_rules('category_sub_level3_urban_time', 'category sub level3 urban time', 'trim|required');
        $this->form_validation->set_rules('category_sub_level3_rural_time', 'category sub level3 rural time', 'trim|required');
        $this->form_validation->set_rules('category_sub_level4_urban_time', 'category sub level4 urban time', 'trim|required');
        $this->form_validation->set_rules('category_sub_level4_rural_time', 'category sub level4 rural time', 'trim|required');
        $this->form_validation->set_rules('escalate_upto', 'escalate upto', 'trim|required');
        $this->form_validation->set_rules('category_sub_app', 'category sub app', 'trim|required');
        $this->form_validation->set_rules('category_sub_status', 'category sub status', 'trim|required');

        $this->form_validation->set_rules('category_sub_id', 'category_sub_id', 'trim');
        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel() {
        $this->load->helper('exportexcel');
        $namaFile = "category_sub.xls";
        $judul = "category_sub";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Main Id");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Name");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Created By");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Time");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Level1 Urban");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Level1 Rural");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Level2 Urban");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Level2 Rural");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Level3 Urban");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Level3 Rural");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Level4 Urban");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Level4 Rural");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Level1 Urban Time");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Level1 Rural Time");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Level2 Urban Time");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Level2 Rural Time");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Level3 Urban Time");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Level3 Rural Time");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Level4 Urban Time");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Level4 Rural Time");
        xlsWriteLabel($tablehead, $kolomhead++, "Escalate Upto");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub App");
        xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Status");

        foreach ($this->Category_sub_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->category_sub_main_id);
            xlsWriteLabel($tablebody, $kolombody++, $data->category_sub_name);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_created_by);
            xlsWriteLabel($tablebody, $kolombody++, $data->category_sub_time);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_level1_urban);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_level1_rural);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_level2_urban);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_level2_rural);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_level3_urban);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_level3_rural);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_level4_urban);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_level4_rural);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_level1_urban_time);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_level1_rural_time);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_level2_urban_time);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_level2_rural_time);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_level3_urban_time);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_level3_rural_time);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_level4_urban_time);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_level4_rural_time);
            xlsWriteNumber($tablebody, $kolombody++, $data->escalate_upto);
            xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_app);
            xlsWriteLabel($tablebody, $kolombody++, $data->category_sub_status);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function get_subcategories() {

        $category_sub_main_id = $_POST['category_main_id'];


        $records = $this->Category_sub_model->get_all_by_category_sub_main_id($category_sub_main_id);
        $strig = '<option value="">Select Sub Categories</option>';
        if ($records) {

            foreach ($records as $row) {
                $strig = $strig . '<option value="' . $row->category_sub_id . '">' . $row->category_sub_name . '</option>';
            }
        } else {
            
        }
        echo $strig;
    }

   

}

/* End of file Category_sub.php */
/* Location: ./application/controllers/Category_sub.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2019-08-02 06:45:59 */
