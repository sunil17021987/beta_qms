<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Ftp_agents extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Ftp_agents_model');
        $this->load->library('form_validation');
         $this->load->helper('download');
    }

    public function index()
    {
        $ftp_agents = $this->Ftp_agents_model->get_all_by_join_id($id);

        $data = array(
            'ftp_agents_data' => $ftp_agents
        );

          $data['content'] = 'ftp_agents/ftp_agents_list';
        $this->load->view('common/master', $data);    
            
    }
     public function agentsFTP()
    {
        $ftp_agents = $this->Ftp_agents_model->get_all_by_join();

        $data = array(
            'ftp_agents_data' => $ftp_agents
        );

          $data['content'] = 'ftp_agents/ftp_agents_list';
        $this->load->view('common/master', $data);    
            
    }

    public function read($id) 
    {
        $row = $this->Ftp_agents_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'upload_by' => $row->upload_by,
		'file_name' => $row->file_name,
		'filename' => $row->filename,
		'fileurl' => $row->fileurl,
		'dateof' => $row->dateof,
		'status' => $row->status,
	    );
             $data['content'] = 'ftp_agents/ftp_agents_read';
        $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('ftp_agents'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('ftp_agents/create_action'),
	    'id' => set_value('id'),
	    'upload_by' => set_value('upload_by'),
	    'file_name' => set_value('file_name'),
	    'filename' => set_value('filename'),
	    'fileurl' => set_value('fileurl'),
	    'dateof' => set_value('dateof'),
	    'status' => set_value('status'),
	);
        $data['content'] = 'ftp_agents/ftp_agents_form';
        $this->load->view('common/master', $data);       
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'upload_by' => $this->input->post('upload_by',TRUE),
		'file_name' => $this->input->post('file_name',TRUE),
		'filename' => $this->input->post('filename',TRUE),
		'fileurl' => $this->input->post('fileurl',TRUE),
		'dateof' => $this->input->post('dateof',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Ftp_agents_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('ftp_agents'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Ftp_agents_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('ftp_agents/update_action'),
		'id' => set_value('id', $row->id),
		'upload_by' => set_value('upload_by', $row->upload_by),
		'file_name' => set_value('file_name', $row->file_name),
		'filename' => set_value('filename', $row->filename),
		'fileurl' => set_value('fileurl', $row->fileurl),
		'dateof' => set_value('dateof', $row->dateof),
		'status' => set_value('status', $row->status),
	    );
            $data['content'] = 'ftp_agents/ftp_agents_form';
            $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('ftp_agents'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'upload_by' => $this->input->post('upload_by',TRUE),
		'file_name' => $this->input->post('file_name',TRUE),
		'filename' => $this->input->post('filename',TRUE),
		'fileurl' => $this->input->post('fileurl',TRUE),
		'dateof' => $this->input->post('dateof',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Ftp_agents_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('ftp_agents'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Ftp_agents_model->get_by_id($id);

        if ($row) {
            $this->Ftp_agents_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('Ftp_agents/agentsFTP'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('Ftp_agents/agentsFTP'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('upload_by', 'upload by', 'trim|required');
	$this->form_validation->set_rules('file_name', 'file name', 'trim|required');
	$this->form_validation->set_rules('filename', 'filename', 'trim|required');
	$this->form_validation->set_rules('fileurl', 'fileurl', 'trim|required');
	$this->form_validation->set_rules('dateof', 'dateof', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }
      public function donwload() {
        $id = $this->uri->segment(3);
        $row = $this->Ftp_agents_model->get_by_id($id);

        $file= base_url().$row->file_name;
  
        $data = file_get_contents($file);
        force_download($row->filename, $data);
    }

}

/* End of file Ftp_agents.php */
/* Location: ./application/controllers/Ftp_agents.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2019-11-15 21:38:12 */
