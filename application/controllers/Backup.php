<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Backup extends CI_Controller {

    public function __construct() {

    }

    function index() {
      // $this->load->view('backup');
    }
    function database_backup()
    {
            $this->load->dbutil();
            $prefs = array('format' => 'zip', 'filename' => 'Database-backup_' . date('Y-m-d_H-i'));
            $backup = $this->dbutil->backup($prefs);
            if (!write_file('./uploads/' . date('Y-m-d_H-i') . '.zip', $backup)) {
                echo "Error while creating auto database backup!";
             } 
             else {
                echo "Database backup has been successfully Created";
            }
    }

}
  //$save = base_url() . 'backup/' . $db_name;