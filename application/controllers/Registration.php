<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Registration extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->model('Category_model');
        $this->load->model('Course_model');
        $this->load->model('Registration_model');
        $this->load->model('Batch_model');
        $this->load->model('Weapons_model');
        $this->load->model('Lane_model');
        $this->load->model('Time_slot_model');
        $this->load->model('Expense_model');
        $this->load->model('Fees_collect_model');
        $this->load->library('form_validation');
        $this->load->model('Demo_model');
        // load Pagination library
        $this->load->library('pagination');

        // load URL helper
        $this->load->helper('url');

        $this->check_auth();
    }

    public function check_auth() {
        $login_type = $this->session->userdata('validated');
        if (!$login_type == TRUE)
            redirect(base_url('login'));
    }

    public function get_avilable() {
        $time_slot = $_POST['time_slot'];
        $lane = $_POST['lane'];
        $aid = $_POST['aid'];
        $end_date = date('Y-m-d', strtotime($_POST['end_date']));
        echo $avilable = $this->Registration_model->get_avilable($time_slot, $lane, $end_date, $aid);
    }

    public function get_avilable_date() {
        $admission_date = date('Y-m-d', strtotime($_POST['admission_date']));
//        $lane = $_POST['lane'];
        $rid = $_POST['rid'];
//        $end_date = date('Y-m-d', strtotime($_POST['end_date']));
        echo $avilable = $this->Registration_model->get_avilable_date($admission_date, $rid);

        //echo 'ateek';
    }

    public function get_item() {

        $time = $_POST['time_slot'];
        $date = date('Y-m-d', strtotime($_POST['date']));
        $course = $this->input->post('course');

        $waponslist = $this->Course_model->get_by_id($course)->c_weapon;
        $weaponid = explode(",", $waponslist);
        // print_r($weaponid);
        // foreach ($weaponid as $id) {
        //    $weapons = $this->Weapons_model->get_by_id($id);
        //    print_r($weapons);
        // }
        $weapons = $this->Weapons_model->get_all();
        $bookedwapons = array();
        $bookedwapons = $this->Registration_model->booked_wapons($time, $date);

        //print_r($bookedwapons);
        //die();
        $dd = "";
        $dd = '<option value="0">Pending Admission</option>';
        $wapb = array();
        foreach ($weapons as $wID) {
            foreach ($bookedwapons as $bwapon) {
                if ($bwapon->weapon == $wID->weapon_id) {
                    array_push($wapb, $bwapon->weapon);
                }
            }
        }

        foreach ($weapons as $wID) {
            if ($wID->owner == 0) {
                foreach ($weaponid as $valueid) {
                    if ($valueid == $wID->weapon_id) {
                        if (in_array($wID->weapon_id, $wapb)) {
                            //check shared
                            if ($wID->shared == 1) {
                                // if shared check asign count
                                $bookedwapons_count = $this->Registration_model->booked_wapons_count($wID->weapon_id, $time, $date);
                                // if count<2 then add 
                                if ($bookedwapons_count < 2)
                                    $dd = $dd . '<option value="' . $wID->weapon_id . '" waponcat="' . $wID->weapon_primary_cat . '">' . $wID->weapon_name . '   (' . $wID->weapon_desc . ') - SHARED' . '</option>';
                            }
                        } else {
                            $shared = '';
                            if ($wID->shared == 1)
                                $shared = ' - SHARED';


                            $dd = $dd . '<option value="' . $wID->weapon_id . '" waponcat="' . $wID->weapon_primary_cat . '">' . $wID->weapon_name . '   (' . $wID->weapon_desc . ')' . $shared . '</option>';
                        }
                    }
                }
            }
        }


        echo $dd;
    }

    public function get_lane() {

        $time = $_POST['time_slot'];
        $admissiondate = date('Y-m-d', strtotime($_POST['date']));

        $batchs = $this->Registration_model->get_lane($time, $admissiondate);

        $lanes = $this->Lane_model->get_all();
        $q = array();
        foreach ($batchs as $value) {
            array_push($q, $value->lane_id);
        }
        $wapens = $this->Lane_model->get_by_id_not_equal($q);
        if ($wapens) {
            echo '<option value="0">Pending Admission</option>';
            foreach ($wapens as $row) {
                echo '<option value="' . $row['lane_id'] . '">' . $row['title'] . ' (' . $row['desc'] . ')' . '</option>';
            }
        } else {
            
        }
    }

    public function get_prime_fees() {

        $time = $_POST['time_slot'];

        $course = $this->input->post('course');

        $data = $this->Course_model->getprimecourse_feesByIdNtime($course, $time);



        echo json_encode($data);

        //echo $row['fee_amount'];
    }

    private function check_isvalidated() {
        if (!$this->session->userdata('validated')) {
            redirect(base_url());
        }
    }

    public function get_batch_time() {
        $batch_name = $this->input->post('value');
        $batchs = $this->Batch_model->get_batch_time($batch_name);
        echo json_encode(array('batch_time' => $batchs->b_timing));
    }

    public function get_increase_date() {
        $course = $_POST['course'];
        $registration_id = $_POST['registration_id'];
        $date = $_POST['date'];
        $getuser = $this->Registration_model->get_inrolled_user($registration_id, $course);
        if (empty($getuser)) {
            $row = $this->Course_model->get_by_id($course)->c_duration;
            $fee = $this->Course_model->get_by_id($course)->c_fee;
            $date_1 = strtotime("+$row", strtotime($date));
            $new_date = date("Y-m-d", $date_1);
            $data = array(
                'end_date' => date('d-m-Y', strtotime($new_date)),
                'fee' => $fee
            );
            echo json_encode($data);
        } else {
            echo'1';
        }
    }

    public function get_increase_date_lane() {
        $course = $_POST['course'];
        $registration_id = $_POST['registration_id'];
        $date = $_POST['date'];
        $getuser = $this->Registration_model->get_inrolled_user($registration_id, $course);
        if (!empty($getuser)) {
            $row = $this->Course_model->get_by_id($course)->c_duration;
            $fee = $this->Course_model->get_by_id($course)->c_fee;
            $date_1 = strtotime("+$row", strtotime($date));
            $new_date = date("Y-m-d", $date_1);
            $data = array(
                'end_date' => $new_date,
                'fee' => $fee
            );
            echo json_encode($data);
        } else {
            echo'1';
        }
    }

    public function get_date() {
        $start_date = '2009-06-17';
        $end_date = '2009-09-05';
        $date_from_user = '2009-10-28';
        $this->check_in_range($start_date, $end_date, $date_from_user);
    }

    public function check_in_range($start_date, $end_date, $date_from_user) {
        // Convert to timestamp
        $start_ts = strtotime($start_date);
        $end_ts = strtotime($end_date);
        $user_ts = strtotime($date_from_user);
        // Check that user date is between start & end
        if (($user_ts >= $start_ts) && ($user_ts <= $end_ts)) {
            echo 'avaiable';
        };
    }

    public function index() {
        $search_by_name = $this->input->post('search_by_name');
      if($search_by_name == "")
      {
          $search_by_name ="";
      }
        // init params
        $params = array();
        $limit_per_page = 50;
        $page = ($this->uri->segment(3)) ? ($this->uri->segment(3) - 1) : 0;
        $total_records = $this->Registration_model->get_total();

        if ($total_records > 0) {
            // get current page records
            $params["registration_data"] = $this->Registration_model->get_current_page_records($limit_per_page, $page * $limit_per_page,$search_by_name);
          // echo  $this->db->last_query();

            $config['base_url'] = base_url() . 'registration/index';
            $config['total_rows'] = $total_records;
            $config['per_page'] = $limit_per_page;
            $config["uri_segment"] = 3;

            // custom paging configuration
            $config['num_links'] = 2;
            $config['use_page_numbers'] = TRUE;
            $config['reuse_query_string'] = TRUE;

            $config['full_tag_open'] = '<ul class="pagination pagination-sm">';
            $config['full_tag_close'] = '</ul><!--pagination-->';

           $config['first_link'] = '&laquo; First';
    $config['first_tag_open'] = '<li class="prev page">';
    $config['first_tag_close'] = '</li>';

            $config['last_link'] = 'Last &raquo;';
    $config['last_tag_open'] = '<li class="next page">';
    $config['last_tag_close'] = '</li>';

          $config['next_link'] = 'Next &rarr;';
    $config['next_tag_open'] = '<li class="next page">';
    $config['next_tag_close'] = '</li>';
 
    $config['prev_link'] = '&larr; Previous';
    $config['prev_tag_open'] = '<li class="prev page">';
    $config['prev_tag_close'] = '</li>';;

            $config['cur_tag_open'] = '<li class="active"><a href="">';
    $config['cur_tag_close'] = '</a></li>';
 
    $config['num_tag_open'] = '<li class="page">';
    $config['num_tag_close'] = '</li>';
 
    $config['anchor_class'] = 'follow_link';

            $this->pagination->initialize($config);

            // build paging links
            $params["links"] = $this->pagination->create_links();
        }
        
       if($this->uri->segment(1) == 'registration'){
        $params['admission'] = "active" ;}

        $params['content'] = 'registration/registration_list';
        $this->load->view('common/master', $params);
    }

    public function pending_list() {
        $registration = $this->Registration_model->pending_registrations();
        // print_r($registration);

        $data = array(
            'registration_data' => $registration
        );
        $data['active'] = "active" ;
        if($this->uri->segment(1) == 'registration'){
        $data['admission'] = "active" ;}
        $data['content'] = 'registration/pending_list';
        $this->load->view('common/master', $data);
    }

    public function pending() {
        $registration = $this->Registration_model->get_pending();
        $data = array(
            'registration_data' => $registration
        );

        $data['content'] = 'registration/registration_pending';
        $this->load->view('common/master', $data);
    }

    public function r() {
        $registration = $this->Registration_model->get_all();
        $data = array(
            'registration_data' => $registration
        );

        $data['content'] = 'registration/registration_list';
        $this->load->view('common/master', $data);
    }

    public function stu_profile() {
        $data = array(
            'reg_id' => '',
            'button' => 'Create',
            'action' => site_url('registration/create_action'),
            'id' => set_value('id'),
            'title' => set_value('title'),
            'name' => set_value('name'),
            'gender' => set_value('gender'),
            'mob' => set_value('mob'),
            'email' => set_value('email'),
            'address' => set_value('address'),
            'dob' => set_value('dob'),
            'occupation' => set_value('occupation'),
            'admission_date' => set_value('admission_date'),
            'batch' => set_value('batch'),
            'time' => set_value('time'),
            'qualification' => set_value('qualification'),
            'course' => set_value('course'),
            'course_fee' => set_value('course_fee'),
            'created_at' => set_value('created_at'),
            'reg_date' => date('Y-m-d'),
            'status' => set_value('status'),
            'height' => set_value('height'),
            'weight' => set_value('weight'),
            'weapon' => set_value('weapon'),
            'weight' => set_value('weight'),
            'feet_size' => set_value('feet_size'),
            'blood_group' => set_value('blood_group'),
            'collage_name' => set_value('collage_name'),
            'guardian_name' => set_value('guardian_name'),
            'alternate_no' => set_value('alternate_no'),
            'vip' => set_value('vip'),
            'vip_remark' => set_value('vip_remark'),
            'certificate' => set_value('certificate'),
            'certificate_issue' => set_value('certificate_issue'),
            'medication' => set_value('medication'),
            'allergies' => set_value('allergies'),
            'diet' => set_value('diet'),
            'concern' => set_value('concern'),
            'hear_about' => set_value('hear_about'),
            't_shirt' => set_value('t_shirt'),
            'age' => set_value('age'),
            'nationality' => set_value('nationality'),
            'permanent_address' => set_value('permanent_address'),
            'scholarship' => set_value('scholarship'),
//            'receipttype' => set_value('receipttype'),
//            'receiptno' => set_value('receiptno'),
            'pname' => set_value('pname'),
            'cert_serialno' => set_value('cert_serialno'),
            'phone' => set_value('phone'),
            'level1score' => set_value('level1score'),
            'chequeorcash' => set_value('chequeorcash'),
            'bankbranch' => set_value('bankbranch'),
            'remarks' => set_value('remarks'),
            'additional_benefit' => set_value('additional_benefit'),
            'additional_benefit_remarks' => set_value('additional_benefit_remarks'),
            'scholarship_amount' => set_value('scholarship_amount', 0),
            'additional_benefit_amount' => set_value('additional_benefit_amount', 0),
            'chequeno' => set_value('chequeno'),
            'scholarship_type' => set_value('scholarship_type'),
            'scholarship_remarks' => set_value('scholarship_remarks'),
            'transactionid' => set_value('transactionid'),
            'h_d_y_h_a_u' => ''
        );
        // $data['occupation'] = $this->Category_model->get_occupation();

        $data['course'] = $this->Course_model->get_all();
        $data['time_slot'] = $this->Time_slot_model->get_all();
        $data['weapons'] = $this->Weapons_model->get_all();
        $data['lanes'] = $this->Lane_model->get_all();
        $data['batch'] = $this->Batch_model->get_time();
//      $data['batch'] = $this->Batch_model->get_batch1();
        $data['active'] = "active" ;
        $data['content'] = 'registration/stu_registration';
        $this->load->view('common/master', $data);
    }

    public function academicdetails($id) {
        $registerDetails = $this->Registration_model->getRegisterDetail($id);
        // echo "<pre>"; print_r($registerDetails); echo "</pre>";
        $data = array(
            'button' => 'Save Record',
            'action' => site_url('registration/create_academicdetails'),
            'id' => set_value('id'),
            'registration_id' => $registerDetails[0]->id,
            'title' => set_value('title'),
            'name' => set_value($registerDetails[0]->name),
            'gender' => set_value('gender'),
            'mob' => set_value('mob'),
            'email' => set_value('email'),
            'address' => set_value('address'),
            'dob' => set_value('dob'),
            'occupation' => set_value('occupation'),
            'admission_date' => set_value('admission_date'),
            'batch' => set_value('batch'),
            'time' => set_value('time'),
            'qualification' => set_value('qualification'),
            'course' => set_value('course'),
            'course_fee' => set_value('course_fee'),
            'created_at' => set_value('created_at'),
            'reg_date' => date('Y-m-d'),
            'status' => set_value('status'),
            'height' => set_value('height'),
            'weight' => set_value('weight'),
            'weapon' => set_value('weapon'),
            'weight' => set_value('weight'),
            'feet_size' => set_value('feet_size'),
            'blood_group' => set_value('blood_group'),
            'collage_name' => set_value('collage_name'),
            'guardian_name' => set_value('guardian_name'),
            'alternate_no' => set_value('alternate_no'),
            'vip' => set_value('vip'),
            'vip_remark' => set_value('vip_remark'),
            'certificate' => set_value('certificate'),
            'certificate_issue' => set_value('certificate_issue'),
            'medication' => set_value('medication'),
            'allergies' => set_value('allergies'),
            'diet' => set_value('diet'),
            'concern' => set_value('concern'),
            'hear_about' => set_value('hear_about'),
            't_shirt' => set_value('t_shirt'),
            'age' => set_value('age'),
            'nationality' => set_value('nationality'),
            'permanent_address' => set_value('permanent_address'),
            'scholarship' => set_value('scholarship'),
//            'receipttype' => set_value('receipttype'),
//            'receiptno' => set_value('receiptno'),
            'pname' => $registerDetails[0]->name,
            'cert_serialno' => set_value('cert_serialno'),
            'phone' => $registerDetails[0]->mob,
            'level1score' => set_value('level1score'),
            'chequeorcash' => set_value('chequeorcash'),
            'bankbranch' => set_value('bankbranch'),
            'remarks' => set_value('remarks'),
            'additional_benefit' => set_value('additional_benefit'),
            'additional_benefit_remarks' => set_value('additional_benefit_remarks'),
            'scholarship_amount' => set_value('scholarship_amount', 0),
            'additional_benefit_amount' => set_value('additional_benefit_amount', 0),
            'chequeno' => set_value('chequeno'),
            'scholarship_type' => set_value('scholarship_type'),
            'scholarship_remarks' => set_value('scholarship_remarks'),
            'transactionid' => set_value('transactionid'),
        );
        // $data['occupation'] = $this->Category_model->get_occupation();
        $data['courses'] = $this->Course_model->get_all();
        $data['time_slot'] = $this->Time_slot_model->get_all();
        $data['weapons'] = $this->Weapons_model->get_all();
        $data['personalweapons'] = $this->Weapons_model->get_all_personal($id);
        $data['lanes'] = $this->Lane_model->get_all();
        $data['batch'] = $this->Batch_model->get_time();
//      $data['batch'] = $this->Batch_model->get_batch1();
        $data['title_model'] = 'Add Personal Weapon..';
        $data['addmodel'] = 'registration/add_weapon_model';


        $data['content'] = 'registration/academicdetails';
        $this->load->view('common/master', $data);
    }

    public function pdf($id) {
        $row = $this->Registration_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'title' => $row->title,
                'name' => $row->name,
                'gender' => $row->gender,
                'mob' => $row->mob,
                'email' => $row->email,
                'address' => $row->address,
                'dob' => $row->dob,
                'occupation' => $row->occupation,
                'admission_date' => $row->admission_date,
                'batch' => $row->batch,
                'time' => $row->time,
                'qualification' => $row->qualification,
                'course' => $row->course,
                'course_fee' => $row->course_fee,
                'created_at' => $row->created_at,
                'status' => $row->status,
            );
            $html = $this->load->view('pdf/profile', $data, true);

            //this the the PDF filename that user will get to download
            $pdfFilePath = "output_pdf_name.pdf";

            //load mPDF library
            $this->load->library('m_pdf');

            //generate the PDF from the given html
            $this->m_pdf->pdf->WriteHTML($html);

            //download it.
            $this->m_pdf->pdf->Output($pdfFilePath, "D");
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('registration'));
        }
    }

    public function read($id) {
        $row = $this->Registration_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'title' => $row->title,
                'name' => $row->name,
                'gender' => $row->gender,
                'mob' => $row->mob,
                'email' => $row->email,
                'address' => $row->address,
                'dob' => $row->dob,
                'occupation' => $row->occupation,
                'admission_date' => $row->admission_date,
                'batch' => $row->batch,
                'time' => $row->time,
                'qualification' => $row->qualification,
                'course' => $row->course,
                'course_fee' => $row->course_fee,
                'created_at' => $row->created_at,
                'status' => $row->status,
                'end_date' => $row->end_date,
                'scholarship_amount' => $row->scholarship_amount,
                'scholarship' => $row->scholarship,
                'additional_benefit_remarks' => $row->additional_benefit_remarks,
            );
            $data['content'] = 'students/student_profile';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('registration'));
        }
    }

    public function create() {
        $data = array(
            'button' => 'Create',
            'action' => site_url('registration/create_action'),
            'id' => set_value('id'),
            'name' => set_value('name'),
            'mob' => set_value('mob'),
            'email' => set_value('email'),
            'address' => set_value('address'),
            'dob' => set_value('dob'),
            'batch' => set_value('batch'),
            'time' => set_value('time'),
            //'qualification' => set_value('qualification'),
            'weapon' => set_value('weapon'),
            'course' => set_value('course'),
            'course_fee' => set_value('course_fee'),
            'created_at' => set_value('created_at'),
            'vip' => set_value('vip'),
            'vip_remark' => set_value('vip_remark'),
            'certificate' => set_value('certificate'),
            'certificate_issue' => set_value('certificate_issue'),
            'medication' => set_value('medication'),
            'allergies' => set_value('allergies'),
            'diet' => set_value('diet'),
            'concern' => set_value('concern'),
            'hear_about' => set_value('hear_about'),
            't_shirt' => set_value('t_shirt'),
        );
        $this->load->view('registration/registration_form1', $data);
    }

    public function create_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->stu_profile();
        } else {
            $d = $this->input->post('dob');
            $d = str_replace('/', '-', $d);

            $data = array(
                'reg_code' => $this->regCode(),
                'title' => $this->input->post('title', TRUE),
                'name' => $this->input->post('name', TRUE),
                'gender' => $this->input->post('gender', TRUE),
                'mob' => $this->input->post('mob', TRUE),
                'email' => $this->input->post('email', TRUE),
                'address' => $this->input->post('address', TRUE),
                'dob' => date('Y-m-d', strtotime($d)),
                'occupation' => $this->input->post('occupation', TRUE),
                //'qualification' => $this->input->post('qualification', TRUE),
                'created_at' => date('d-m-Y H:i:s'),
                'status' => 0,
                'collage_name' => $this->input->post('collage_name', TRUE),
                'height' => ($this->input->post('height', TRUE))?$this->input->post('height', TRUE):$this->input->post('heightcm', TRUE),
                'weight' => $this->input->post('weight', TRUE),
                'feet_size' => $this->input->post('feet_size', TRUE),
                'alternate_no' => $this->input->post('alternate_no', TRUE),
                'guardian_name' => $this->input->post('guardian_name', TRUE),
                'blood_group' => $this->input->post('blood_group', TRUE),
                'vip' => $this->input->post('vip', TRUE),
                'vip_remark' => $this->input->post('vip_remark', TRUE),
                'medication' => $this->input->post('medication', TRUE),
                'allergies' => $this->input->post('allergies', TRUE),
                'diet' => $this->input->post('diet', TRUE),
                'concern' => $this->input->post('concern', TRUE),
                'hear_about' => $this->input->post('hear_about', TRUE),
                't_shirt' => $this->input->post('t_shirt', TRUE),
                'age' => $this->input->post('age', TRUE),
                'nationality' => $this->input->post('nationality', TRUE),
                'permanent_address' => $this->input->post('permanent_address', TRUE),
            );


            $insert_id_reg = $this->Registration_model->insert($data);
            $data1 = array('is_registred' => 1);
            $this->Demo_model->update($this->input->post('reg_id', TRUE), $data1);
            $mobileno = $this->clean($this->input->post('mob', TRUE));
            $message = "Congratulations your registration successful In GFG";
            $this->sendSMS($mobileno, $message);
            $this->session->set_flashdata('message', 'Create Record Success');

            redirect(site_url('registration/academicdetails/' . $insert_id_reg));
        }
    }

    function clean($string) {
        $string = str_replace('-', '', $string); // Replaces all spaces with hyphens.
        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    function sendSMS($to, $message) {
        $content = 'username=' . rawurlencode('GFGjbp') .
                '&password=' . rawurlencode('password') .
                '&sender=' . rawurlencode('GFGjbp') .
                '&to=' . rawurlencode($to) .
                '&message=' . rawurlencode($message) .
                '&reqid=' . rawurlencode(1) .
                '&format={json|text}' .
                '&route_id=' . rawurlencode('113');

        $ch = curl_init('http://login.heightsconsultancy.com/API/WebSMS/Http/v1.0a/index.php?' . $content);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function create_academicdetails() {
        $this->_rules1();

        if ($this->form_validation->run() == FALSE) {
            $this->academicdetails($this->input->post('registration_id', TRUE));
        } else {
            $data = array(
                'reg_code' => $this->regCode(),
                'registration_id' => $this->input->post('registration_id', TRUE),
                'admission_date' => date('Y-m-d', strtotime($this->input->post('admission_date', TRUE))),
                'batch' => 'no',
                'time' => ($this->input->post('time', TRUE)) ? $this->input->post('time', TRUE) : '0',
                'course' => $this->input->post('course', TRUE),
                'course_fee' => $this->input->post('course_fee', TRUE),
                'weapon' => ($this->input->post('aweapon', TRUE)) ? $this->input->post('aweapon', TRUE) : $this->input->post('pweapon', TRUE),
                'created_at' => date('d/m/Y'),
                'status' => $this->input->post('status', TRUE),
                'lane_id' => ($this->input->post('lane_id', TRUE)) ? $this->input->post('lane_id', TRUE) : '0',
                'end_date' => date('Y-m-d', strtotime($this->input->post('end_date', TRUE))),
                'scholarship' => $this->input->post('scholarship', TRUE),
                'kit_status' => '1',
                'receipttype' => '1',
                'receiptno' => '1',
                'pname' => $this->input->post('pname', TRUE),
                'phone' => $this->input->post('phone', TRUE),
                'additional_benefit' => $this->input->post('additional_benefit', TRUE),
                'additional_benefit_remarks' => $this->input->post('additional_benefit_remarks', TRUE),
                'scholarship_amount' => $this->input->post('scholarship_amount', TRUE),
                'additional_benefit_amount' => $this->input->post('additional_benefit_amount', TRUE),
                'scholarship_remarks' => $this->input->post('scholarship_remarks', TRUE),
                'scholarship_type' => $this->input->post('scholarship_type', TRUE),
                'status' => '1',
                'prime_type' => $this->input->post('check_time', TRUE),
            );
            $registration_id = $this->input->post('registration_id', TRUE);
            $course = $this->input->post('course', TRUE);
            if ($this->Registration_model->get_existuser($registration_id, $course) == '0') {
                $insert_id_reg = $this->Registration_model->insert_acadmic($data);
//**********************************pay mode***********************transactionid*****************************
                $paymentMode = $this->input->post('paymentMode', TRUE);
                if ($paymentMode == 'Pay') {
                    $data2 = array(
                        'programm' => $insert_id_reg,
                        'paid_amount' => $this->input->post('paid_amount', TRUE),
                        'pay_type' => $this->input->post('paymentMode', TRUE),
                        'pay_mode' => $this->input->post('chequeorcash1', TRUE),
                        'remarks' => $this->input->post('remarks1', TRUE),
                        'chequedate' => date('Y-m-d'),
                    );
                    $this->Fees_collect_model->insert($data2);
                } elseif ($paymentMode == 'pdc') {
                    $data2 = array(
                        'programm' => $insert_id_reg,
                        'paid_amount' => $this->input->post('fee_amount_pdc', TRUE),
                        'remarks' => $this->input->post('pdc_remarks_pdc', TRUE),
                        'pay_mode' => $this->input->post('chequeorcash_pdc_pdc', TRUE),
                        'chequedate' => date('Y-m-d'),
                    );
                    $this->Fees_collect_model->insert($data2);

                    $remarks = array();
                    $chequeDate = array();
                    $chequeamt = array();
                    $pay_type = array();
                    $chequeamt = $this->input->post('pdc_amount_pdc', TRUE);
                    $remarks = $this->input->post('remarks_pdc', TRUE);
                    $chequeDate = $this->input->post('chequeDate_pdc', TRUE);
                    $pay_type = $this->input->post('chequeorcash_pdc', TRUE);
                    $countintallment = count($chequeamt);

                    for ($i = 0; $i < $countintallment; $i++) {
                        $data3 = array(
                            'programm' => $insert_id_reg,
                            'pdc_amount' => $chequeamt[$i],
                            'paid_amount' => '0',
                            'pay_mode' => $paymentMode,
                            'pay_type' => $pay_type[$i],
                            'remarks' => $remarks[$i],
                            'chequedate' =>date('Y-m-d', strtotime($chequeDate[$i])) ,
                        );
                        $this->Fees_collect_model->insert($data3);
                    }
                } elseif ($paymentMode == 'Installment') {

                    $data2 = array(
                        'programm' => $insert_id_reg,
                        'paid_amount' => $this->input->post('fee_amount', TRUE),
                        'chequedate' => date('Y-m-d'),
                        'remarks' => $this->input->post('remarks_wpdc', TRUE),
                        'pay_mode' => $this->input->post('chequeorcash_pdc_wpdc', TRUE),
                    );
                    $this->Fees_collect_model->insert($data2);

                    $remarks = array();
                    $chequeDate = array();
                    $chequeamt = array();
                    $pay_type = array();
                    $chequeamt = $this->input->post('pdc_amount', TRUE);
                    $remarks = $this->input->post('remarks', TRUE);
                    $chequeDate = $this->input->post('chequeDate', TRUE);
                    $pay_type = $this->input->post('chequeorcash', TRUE);
                    $countintallment = count($chequeamt);

                    for ($i = 0; $i < $countintallment; $i++) {
                        $data3 = array(
                            'programm' => $insert_id_reg,
                            'pdc_amount' => $chequeamt[$i],
                            'paid_amount' => '0',
                            'pay_mode' => $paymentMode,
                            'pay_type' => $pay_type[$i],
                            'remarks' => $remarks[$i],
                            'chequedate' =>date('Y-m-d', strtotime($chequeDate[$i])) ,
                        );
                        $this->Fees_collect_model->insert($data3);
                    }
                } else {
                    
                }
                if ($this->input->post('lane_allot', TRUE) == '0') {

                    $parent_id = $this->input->post('registration_id', TRUE);
                    redirect(site_url('registration/advanced_registration/' . $parent_id));
                } else {
                    $parent_id = $this->input->post('registration_id', TRUE);
                    redirect(site_url('registration/academic_details_list/' . $parent_id));
                }
            } else {
                redirect(site_url('login/logout'));
                return;
            }
        }
    }

    public function advanced_registration($id) {
        $acadmics = $this->Registration_model->academic_details_advanced($id);



        $data = array(
            'registration' => $acadmics,
        );
        $data['content'] = 'registration/advanced_registration';
        $this->load->view('common/master', $data);
    }

    public function update($id) {
        $row = $this->Registration_model->get_by_id($id);
        //  print_r($row);
        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('registration/update_action'),
                'id' => set_value('id', $row->id),
                'title' => set_value('title', $row->title),
                'name' => set_value('name', $row->name),
                'gender' => set_value('gender', $row->gender),
                'mob' => set_value('mob', $row->mob),
                'email' => set_value('email', $row->email),
                'address' => set_value('address', $row->address),
                'dob' => set_value('dob', date('d-m-Y', strtotime($row->dob))),
                'occupation' => set_value('occupation', $row->occupation),
                'admission_date' => set_value('admission_date', $row->occupation),
                'batch' => set_value('batch'),
                'time' => set_value('time'),
                'qualification' => set_value('qualification'),
                'course' => set_value('course'),
                'course_fee' => set_value('course_fee'),
                'created_at' => set_value('created_at'),
                'reg_date' => date('Y-m-d'),
                'status' => set_value('status'),
                'height' => set_value('height', $row->height),
                'weight' => set_value('weight', $row->weight),
                'feet_size' => set_value('feet_size', $row->feet_size),
                'blood_group' => set_value('blood_group', $row->blood_group),
                'guardian_name' => set_value('guardian_name', $row->guardian_name),
                'collage_name' => set_value('collage_name', $row->collage_name),
                'alternate_no' => set_value('alternate_no', $row->alternate_no),
                'vip' => set_value('vip', $row->vip),
                'vip_remark' => set_value('vip_remark', $row->vip_remark),
//     'remark' => set_value('remark',$row->remark),
//            'certificate' => set_value('certificate',$row->certificate),
//            'certificate_issue'=>set_value('certificate_issue',$row->certificate_issue),
                'medication' => set_value('medication', $row->medication),
                'allergies' => set_value('allergies', $row->allergies),
                'diet' => set_value('diet', $row->diet),
                'concern' => set_value('concern', $row->concern),
                'hear_about' => set_value('hear_about', $row->hear_about),
                't_shirt' => set_value('t_shirt', $row->t_shirt),
                'age' => set_value('age', $row->age),
                'nationality' => set_value('nationality', $row->nationality),
                'permanent_address' => set_value('permanent_address', $row->permanent_address),
//            'scholarship' => set_value('scholarship',$row->scholarship),
                'receipttype' => set_value('receipttype'),
                'receiptno' => set_value('receiptno'),
                'pname' => set_value('pname'),
                'cert_serialno' => set_value('cert_serialno'),
                'phone' => set_value('phone'),
                'level1score' => set_value('level1score'),
                'chequeorcash' => set_value('chequeorcash'),
                'bankbranch' => set_value('bankbranch'),
                'remarks' => set_value('remarks'),
                'additional_benefit' => set_value('additional_benefit'),
                'additional_benefit_remarks' => set_value('additional_benefit_remarks'),
                'scholarship_amount' => set_value('scholarship_amount', 0),
                'additional_benefit_amount' => set_value('additional_benefit_amount', 0),
                'chequeno' => set_value('chequeno'),
                'scholarship_type' => set_value('scholarship_type'),
                'scholarship_remarks' => set_value('scholarship_remarks'),
                'transactionid' => set_value('transactionid'),
                'reg_id' => $row->id,
            );
            // $data['occupation'] = $this->Category_model->get_occupation();
            $data['time_slot'] = $this->Time_slot_model->get_all();
            $data['weapon'] = $this->Weapons_model->get_all();
            $data['course'] = $this->Course_model->get_all();
            $data['batch'] = $this->Batch_model->get_time();
            $data['content'] = 'registration/update_registration';
            $this->load->view('common/master', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('registration'));
        }
    }

    public function update_action() {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $d = $this->input->post('dob');
            $d = str_replace('/', '-', $d);
            $data = array(
                'title' => $this->input->post('title', TRUE),
                'name' => $this->input->post('name', TRUE),
                'gender' => $this->input->post('gender', TRUE),
                'mob' => $this->input->post('mob', TRUE),
                'email' => $this->input->post('email', TRUE),
                'address' => $this->input->post('address', TRUE),
                'dob' => date('Y-m-d', strtotime($d)),
                'occupation' => $this->input->post('occupation', TRUE),
//                'admission_date' => $this->input->post('admission_date', TRUE),
//                'batch' => $this->input->post('batch', TRUE),
//                'time' => $this->input->post('time', TRUE),
                //'qualification' => $this->input->post('qualification', TRUE),
//                'weapon' => $this->input->post('weapon', TRUE),
//                'course' => $this->input->post('course', TRUE),
//                'course_fee' => $this->input->post('course_fee', TRUE),
//                'created_at' => $this->input->post('created_at', TRUE),
//                'status' => $this->input->post('status', TRUE),
                'collage_name' => $this->input->post('collage_name', TRUE),
                'height' =>($this->input->post('height', TRUE))?$this->input->post('height', TRUE):$this->input->post('heightcm', TRUE),
                'weight' => $this->input->post('weight', TRUE),
                'feet_size' => $this->input->post('feet_size', TRUE),
                'alternate_no' => $this->input->post('alternate_no', TRUE),
                'guardian_name' => $this->input->post('guardian_name', TRUE),
                'blood_group' => $this->input->post('blood_group', TRUE),
//                'lane_id' => $this->input->post('lane', TRUE),
//                'end_date' => $this->input->post('end_date', TRUE),
                'vip' => $this->input->post('vip', TRUE),
                'vip_remark' => $this->input->post('vip_remark', TRUE),
//                'certificate' => $this->input->post('certificate', TRUE),
//                'certificate_issue' => $this->input->post('certificate_issue', TRUE),
                'medication' => $this->input->post('medication', TRUE),
                'allergies' => $this->input->post('allergies', TRUE),
                'diet' => $this->input->post('diet', TRUE),
                'concern' => $this->input->post('concern', TRUE),
                'hear_about' => $this->input->post('hear_about', TRUE),
                't_shirt' => $this->input->post('t_shirt', TRUE),
//                'scholarship' => $this->input->post('scholarship', TRUE),
                'age' => $this->input->post('age', TRUE),
                'nationality' => $this->input->post('nationality', TRUE),
                'permanent_address' => $this->input->post('permanent_address', TRUE),
//                'receipttype' => $this->input->post('receipttype',TRUE),
//                'receiptno' => $this->input->post('receiptno',TRUE),
            );

            $this->Registration_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('registration'));
        }
    }

    public function status($id) {
        $row = $this->Registration_model->get_by_id($id);
        if ($row) {
            $data = array(
                'status' => 1
            );
            $this->Registration_model->update($id, $data);
            redirect(site_url('registration'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('registration'));
        }
    }

    public function status_close($id) {
        $row = $this->Registration_model->get_by_id($id);
        if ($row) {
            $data = array(
                'status' => 2
            );
            $this->Registration_model->update($id, $data);
            redirect(site_url('registration'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('registration'));
        }
    }

    public function kit_status($id) {
        $row = $this->Registration_model->get_by_id($id);
        if ($row) {
            $data = array(
                'kit_status' => 1
            );
            $this->Registration_model->update($id, $data);
            redirect(site_url('registration'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('registration'));
        }
    }

    public function delete($id) {
        $row = $this->Registration_model->get_by_id($id);

        if ($row) {
            $this->Registration_model->delete($id);
            $programm = $this->Registration_model->get_academicdetails($id);
            $this->Registration_model->delete_academicdetails($id);
            $this->Registration_model->delete_fees_collect($programm->id);
            $this->Registration_model->delete_kit_issued($programm->id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('registration'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('registration'));
        }
    }

    public function _rules() {
        $this->form_validation->set_rules('name', 'name', 'trim|required');
//        $this->form_validation->set_rules('mob', 'mob', 'trim|required');
//        $this->form_validation->set_rules('email', 'email', 'trim|required');
//        $this->form_validation->set_rules('address', 'address', 'trim|required');
//        $this->form_validation->set_rules('dob', 'dob', 'trim|required');
////        $this->form_validation->set_rules('batch', 'batch', 'trim|required');
//        $this->form_validation->set_rules('time', 'time', 'trim|required');
//        $this->form_validation->set_rules('qualification', 'qualification', 'trim|required');
//        $this->form_validation->set_rules('course_fee', 'course_fee', 'trim|required');
//        $this->form_validation->set_rules('created_at', 'created at');
//        $this->form_validation->set_rules('id', 'id', 'trim');
//        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function _rules1() {
        $this->form_validation->set_rules('registration_id', 'registration_id', 'trim|required');
        $this->form_validation->set_rules('admission_date', 'admission_date', 'trim|required');
        $this->form_validation->set_rules('course', 'course', 'trim|required');
        $this->form_validation->set_rules('lane_allot', 'lane_allot', 'trim|required');
        //$this->form_validation->set_rules('check_time', 'check_time', 'trim|required');
        $this->form_validation->set_rules('course_fee', 'course_fee', 'trim|required');
        $this->form_validation->set_rules('pname', 'pname', 'trim|required');
        $this->form_validation->set_rules('phone', 'phone', 'trim|required');
        $this->form_validation->set_rules('paymentMode', 'paymentMode', 'trim|required');
//        $this->form_validation->set_rules('course_fee', 'course_fee', 'trim|required');
//        $this->form_validation->set_rules('created_at', 'created at');
//        $this->form_validation->set_rules('id', 'id', 'trim');
//        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function _rules2() {
        $this->form_validation->set_rules('id', 'id', 'trim|required');
        $this->form_validation->set_rules('admission_date', 'admission_date', 'trim|required');
        $this->form_validation->set_rules('time', 'time', 'trim|required');
        //$this->form_validation->set_rules('weapon', 'weapon', 'trim|required');
        $this->form_validation->set_rules('course', 'course', 'trim|required');

        $this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel() {
        $this->load->helper('exportexcel');
        $namaFile = "registration.xls";
        $judul = "registration";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
        xlsWriteLabel($tablehead, $kolomhead++, "Name");
        xlsWriteLabel($tablehead, $kolomhead++, "Mob");
        xlsWriteLabel($tablehead, $kolomhead++, "Email");
        xlsWriteLabel($tablehead, $kolomhead++, "Address");
        xlsWriteLabel($tablehead, $kolomhead++, "Dob");
        xlsWriteLabel($tablehead, $kolomhead++, "Batch");
        xlsWriteLabel($tablehead, $kolomhead++, "Time");
        //xlsWriteLabel($tablehead, $kolomhead++, "Qualification");
        xlsWriteLabel($tablehead, $kolomhead++, "Course");
        xlsWriteLabel($tablehead, $kolomhead++, "Course Fee");
        xlsWriteLabel($tablehead, $kolomhead++, "Created At");

        foreach ($this->Registration_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
            xlsWriteLabel($tablebody, $kolombody++, $data->name);
            xlsWriteLabel($tablebody, $kolombody++, $data->mob);
            xlsWriteLabel($tablebody, $kolombody++, $data->email);
            xlsWriteLabel($tablebody, $kolombody++, $data->address);
            xlsWriteLabel($tablebody, $kolombody++, $data->dob);
            xlsWriteLabel($tablebody, $kolombody++, $data->batch);
            xlsWriteLabel($tablebody, $kolombody++, $data->time);
            //xlsWriteLabel($tablebody, $kolombody++, $data->qualification);
            xlsWriteLabel($tablebody, $kolombody++, $data->course);
            xlsWriteLabel($tablebody, $kolombody++, $data->course_fee);

            xlsWriteLabel($tablebody, $kolombody++, $data->created_at);

            $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function regCode() {
        $rcode = '';
        $query = "select * from registration order by id DESC limit 1";
        $res = $this->db->query($query);
        if ($res->num_rows() > 0) {
            //return $res->result("array");
            $lastcode = $res->row()->reg_code;
            if ($lastcode != '') {
                //$increment=substr($lastcode,5,strlen($lastcode))+1;
                $increment = substr($lastcode, 11, strlen($lastcode) - 1) + 1;
                $rcode = 'GFG/JBP/' . date('y') . '/' . $increment;
            } else
                $rcode = 'GFG/JBP/' . date('y') . '/' . '1001';
        } else
            $rcode = 'GFG/JBP/' . date('y') . '/' . '1001';

        return $rcode;
    }

    public function academic_details_list($id) {
        $acadmics = $this->Registration_model->academic_details_list($id);
        $data['acadmics'] = $acadmics;
        $data['id'] = $id;
        $data['content'] = 'registration/academic_details_list';
        $this->load->view('common/master', $data);
    }

    public function edit_academicdetails($id) {
        $row = $this->Registration_model->academic_details_list_edit($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'registration_id' => $row->registration_id,
                'button' => 'Update',
                'action' => site_url('registration/update_academicdetails_action'),
                'admission_date' => $row->admission_date,
                'batch' => $row->batch,
                'time' => $row->time,
                'qualification' => $row->qualification,
                'course' => $row->course,
                'course_fee' => $row->course_fee,
                'weapon' => $row->weapon,
                'lane_id' => $row->lane_id,
                'created_at' => $row->created_at,
                'status' => $row->status,
                'end_date' => $row->end_date,
                'scholarship_amount' => $row->scholarship_amount,
                'scholarship' => $row->scholarship,
                'additional_benefit_remarks' => $row->additional_benefit_remarks,
                'scholarship_remarks' => $row->scholarship_remarks,
                'additional_benefit' => $row->additional_benefit,
                'pname' => $row->pname,
                'cert_serialno' => $row->cert_serialno,
                'phone' => $row->phone,
                'level1score' => $row->level1score,
                'chequeorcash' => $row->chequeorcash,
                'bankbranch' => $row->bankbranch,
                'remarks' => $row->remarks,
                'additional_benefit' => $row->additional_benefit,
                'additional_benefit_remarks' => $row->additional_benefit_remarks,
                'scholarship_amount' => $row->scholarship_amount,
                'additional_benefit_amount' => $row->additional_benefit_amount,
                'chequeno' => $row->chequeno,
                'scholarship_type' => $row->scholarship_type,
                'scholarship_remarks' => $row->scholarship_remarks,
                'transactionid' => $row->transactionid,
            );
            $data['coursedata'] = $this->Course_model->get_all();
            $data['time_slot'] = $this->Time_slot_model->get_all();
            $data['weapons'] = $this->Weapons_model->get_all();
            $data['lanes'] = $this->Lane_model->get_all();
            $data['batch'] = $this->Batch_model->get_time();
            $data['content'] = 'registration/edit_academicdetails';
            $this->load->view('common/master', $data);
        }
    }

    public function update_lane($id) {
        $row = $this->Registration_model->academic_details_list_edit($id);

        if ($row) {
            $data = array(
                'id' => $row->id,
                'registration_id' => $row->registration_id,
                'button' => 'Update',
                'action' => site_url('registration/update_lane_action'),
                'admission_date' => $row->admission_date,
                'batch' => $row->batch,
                'time' => $row->time,
                'qualification' => $row->qualification,
                'course' => $row->course,
                'course_fee' => $row->course_fee,
                'weapon' => $row->weapon,
                'lane_id' => $row->lane_id,
                'created_at' => $row->created_at,
                'status' => $row->status,
                'end_date' => $row->end_date,
                'scholarship_amount' => $row->scholarship_amount,
                'scholarship' => $row->scholarship,
                'additional_benefit_remarks' => $row->additional_benefit_remarks,
                'scholarship_remarks' => $row->scholarship_remarks,
                'additional_benefit' => $row->additional_benefit,
                'pname' => $row->pname,
                'cert_serialno' => $row->cert_serialno,
                'phone' => $row->phone,
                'level1score' => $row->level1score,
                'chequeorcash' => $row->chequeorcash,
                'bankbranch' => $row->bankbranch,
                'remarks' => $row->remarks,
                'additional_benefit' => $row->additional_benefit,
                'additional_benefit_remarks' => $row->additional_benefit_remarks,
                'scholarship_amount' => $row->scholarship_amount,
                'additional_benefit_amount' => $row->additional_benefit_amount,
                'chequeno' => $row->chequeno,
                'scholarship_type' => $row->scholarship_type,
                'scholarship_remarks' => $row->scholarship_remarks,
                'transactionid' => $row->transactionid,
                'check_time' => $row->prime_type
            );
            $data['personalweapons'] = $this->Weapons_model->get_all_personal($row->registration_id);
            $data['time_slot'] = $this->Course_model->check_time($row->prime_type, $row->course);
            $data['coursedata'] = $this->Course_model->get_all();
            //$data['time_slot'] = $this->Time_slot_model->get_all();
            $data['weapons'] = $this->Weapons_model->get_all();
            $data['lanes'] = $this->Lane_model->get_all();
            $data['batch'] = $this->Batch_model->get_time();
            $data['addmodel'] = 'registration/add_weapon_model';
            $data['content'] = 'registration/update_lane';
            $this->load->view('common/master', $data);
        }
    }

    public function update_academicdetails_action() {
        $this->_rules2();

        if ($this->form_validation->run() == FALSE) {
            $this->edit_academicdetails($this->input->post('id', TRUE));
        } else {
            $data = array(
                'admission_date' => date('Y-m-d', strtotime($this->input->post('admission_date', TRUE))),
                'course' => $this->input->post('course', TRUE),
                'time' => $this->input->post('time', TRUE),
                'end_date' => date('Y-m-d', strtotime($this->input->post('end_date', TRUE))),
                'weapon' => $this->input->post('weapon', TRUE),
                'lane_id' => $this->input->post('lane_id', TRUE),
            );

            $this->Registration_model->update_acadmic($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('registration/academic_details_list/' . $this->input->post('registration_id', TRUE)));
        }
    }

    public function update_lane_action() {
        $this->_rules2();
        $paid_amount = '';
        if ($this->form_validation->run() == FALSE) {
            $this->update_lane($this->input->post('id', TRUE));
        } else {
            $record = $this->Registration_model->get_by_id_acadmicdetails($this->input->post('id', TRUE));

            $paid_amount = $this->input->post('paid_amount', TRUE);

            if ($paid_amount) {
                $paid_amount = $paid_amount + $record->course_fee;
                $insert_id_reg = $this->input->post('id', TRUE);
                $data2 = array(
                    'programm' => $insert_id_reg,
                    'paid_amount' => $this->input->post('paid_amount', TRUE),
                    'pay_type' => 'Pay',
                    'pay_mode' => $this->input->post('chequeorcash1', TRUE),
                    'remarks' => $this->input->post('remarks1', TRUE),
                    'chequedate' => date('Y-m-d'),
                );
                $this->Fees_collect_model->insert($data2);
            } else {
                $paid_amount = $record->course_fee;
            }

            $data = array(
                'admission_date' => date('Y-m-d', strtotime($this->input->post('admission_date', TRUE))),
                'course' => $this->input->post('course', TRUE),
                'time' => $this->input->post('time', TRUE),
                'end_date' => date('Y-m-d', strtotime($this->input->post('end_date', TRUE))),
                'weapon' => ($this->input->post('aweapon', TRUE)) ? $this->input->post('aweapon', TRUE) : $this->input->post('pweapon', TRUE),
                'lane_id' => $this->input->post('lane_id', TRUE),
                'course_fee' => $paid_amount,
            );

            $this->Registration_model->update_acadmic($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('registration/academic_details_list/' . $this->input->post('registration_id', TRUE)));
        }
    }

    public function edit_status_academicdetails() {
        $id = $this->uri->segment(3);
        $status = $this->uri->segment(4);
        $registration_id = $this->uri->segment(5);
        $data = array(
            'status' => $status,
        );

        $this->Registration_model->update_acadmic($id, $data);
        $this->session->set_flashdata('message', 'Update Record Success');
        redirect(site_url('registration/academic_details_list/' . $registration_id));
    }
    
    function autocompleteData() {
        $returnData = array();
        
        // Get skills data
        $conditions['searchTerm'] = $this->input->get('term');
       // $conditions['conditions']['status'] = '1';
        $skillData = $this->Registration_model->getRows($conditions);
        //echo $this->db->last_query();
        // Generate array
        if(!empty($skillData)){
            foreach ($skillData as $row){
                $data['id'] = $row['id'];
                $data['value'] = $row['name'];
                array_push($returnData, $data);
            }
        }
        
        // Return results as json encoded array
        echo json_encode($returnData);die;
    }
    

}
