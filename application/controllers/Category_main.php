<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Category_main extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Category_main_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $category_main = $this->Category_main_model->get_all();

        $data = array(
            'category_main_data' => $category_main
        );

          $data['content'] = 'category_main/category_main_list';
        $this->load->view('common/master', $data);    
            
    }

    public function read($id) 
    {
        $row = $this->Category_main_model->get_by_id($id);
        if ($row) {
            $data = array(
		'category_main_id' => $row->category_main_id,
		'category_main_name' => $row->category_main_name,
		'category_main_time_urban' => $row->category_main_time_urban,
		'category_main_time_rural' => $row->category_main_time_rural,
		'category_main_status' => $row->category_main_status,
		'category_main_assign_officer_type' => $row->category_main_assign_officer_type,
		'category_main_created_by' => $row->category_main_created_by,
		'category_main_order' => $row->category_main_order,
		'merge_show' => $row->merge_show,
	    );
             $data['content'] = 'category_main/category_main_read';
        $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('category_main'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('category_main/create_action'),
	    'category_main_id' => set_value('category_main_id'),
	    'category_main_name' => set_value('category_main_name'),
	    'category_main_time_urban' => set_value('category_main_time_urban'),
	    'category_main_time_rural' => set_value('category_main_time_rural'),
	    'category_main_status' => set_value('category_main_status'),
	    'category_main_assign_officer_type' => set_value('category_main_assign_officer_type'),
	    'category_main_created_by' => set_value('category_main_created_by'),
	    'category_main_order' => set_value('category_main_order'),
	    'merge_show' => set_value('merge_show'),
	);
        $data['content'] = 'category_main/category_main_form';
        $this->load->view('common/master', $data);       
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'category_main_name' => $this->input->post('category_main_name',TRUE),
		'category_main_time_urban' => $this->input->post('category_main_time_urban',TRUE),
		'category_main_time_rural' => $this->input->post('category_main_time_rural',TRUE),
		'category_main_status' => $this->input->post('category_main_status',TRUE),
		'category_main_assign_officer_type' => $this->input->post('category_main_assign_officer_type',TRUE),
		'category_main_created_by' => $this->input->post('category_main_created_by',TRUE),
		'category_main_order' => $this->input->post('category_main_order',TRUE),
		'merge_show' => $this->input->post('merge_show',TRUE),
	    );

            $this->Category_main_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('category_main'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Category_main_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('category_main/update_action'),
		'category_main_id' => set_value('category_main_id', $row->category_main_id),
		'category_main_name' => set_value('category_main_name', $row->category_main_name),
		'category_main_time_urban' => set_value('category_main_time_urban', $row->category_main_time_urban),
		'category_main_time_rural' => set_value('category_main_time_rural', $row->category_main_time_rural),
		'category_main_status' => set_value('category_main_status', $row->category_main_status),
		'category_main_assign_officer_type' => set_value('category_main_assign_officer_type', $row->category_main_assign_officer_type),
		'category_main_created_by' => set_value('category_main_created_by', $row->category_main_created_by),
		'category_main_order' => set_value('category_main_order', $row->category_main_order),
		'merge_show' => set_value('merge_show', $row->merge_show),
	    );
            $data['content'] = 'category_main/category_main_form';
            $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('category_main'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('category_main_id', TRUE));
        } else {
            $data = array(
		'category_main_name' => $this->input->post('category_main_name',TRUE),
		'category_main_time_urban' => $this->input->post('category_main_time_urban',TRUE),
		'category_main_time_rural' => $this->input->post('category_main_time_rural',TRUE),
		'category_main_status' => $this->input->post('category_main_status',TRUE),
		'category_main_assign_officer_type' => $this->input->post('category_main_assign_officer_type',TRUE),
		'category_main_created_by' => $this->input->post('category_main_created_by',TRUE),
		'category_main_order' => $this->input->post('category_main_order',TRUE),
		'merge_show' => $this->input->post('merge_show',TRUE),
	    );

            $this->Category_main_model->update($this->input->post('category_main_id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('category_main'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Category_main_model->get_by_id($id);

        if ($row) {
            $this->Category_main_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('category_main'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('category_main'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('category_main_name', 'category main name', 'trim|required');
	$this->form_validation->set_rules('category_main_time_urban', 'category main time urban', 'trim|required');
	$this->form_validation->set_rules('category_main_time_rural', 'category main time rural', 'trim|required');
	$this->form_validation->set_rules('category_main_status', 'category main status', 'trim|required');
	$this->form_validation->set_rules('category_main_assign_officer_type', 'category main assign officer type', 'trim|required');
	$this->form_validation->set_rules('category_main_created_by', 'category main created by', 'trim|required');
	$this->form_validation->set_rules('category_main_order', 'category main order', 'trim|required');
	$this->form_validation->set_rules('merge_show', 'merge show', 'trim|required');

	$this->form_validation->set_rules('category_main_id', 'category_main_id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "category_main.xls";
        $judul = "category_main";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Category Main Name");
	xlsWriteLabel($tablehead, $kolomhead++, "Category Main Time Urban");
	xlsWriteLabel($tablehead, $kolomhead++, "Category Main Time Rural");
	xlsWriteLabel($tablehead, $kolomhead++, "Category Main Status");
	xlsWriteLabel($tablehead, $kolomhead++, "Category Main Assign Officer Type");
	xlsWriteLabel($tablehead, $kolomhead++, "Category Main Created By");
	xlsWriteLabel($tablehead, $kolomhead++, "Category Main Order");
	xlsWriteLabel($tablehead, $kolomhead++, "Merge Show");

	foreach ($this->Category_main_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->category_main_name);
	    xlsWriteLabel($tablebody, $kolombody++, $data->category_main_time_urban);
	    xlsWriteLabel($tablebody, $kolombody++, $data->category_main_time_rural);
	    xlsWriteLabel($tablebody, $kolombody++, $data->category_main_status);
	    xlsWriteLabel($tablebody, $kolombody++, $data->category_main_assign_officer_type);
	    xlsWriteNumber($tablebody, $kolombody++, $data->category_main_created_by);
	    xlsWriteNumber($tablebody, $kolombody++, $data->category_main_order);
	    xlsWriteNumber($tablebody, $kolombody++, $data->merge_show);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Category_main.php */
/* Location: ./application/controllers/Category_main.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2019-08-01 15:28:13 */
