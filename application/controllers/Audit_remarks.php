<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Audit_remarks extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Audit_remarks_model');
        $this->load->library('form_validation');
    }

    public function index()
    {
        $audit_remarks = $this->Audit_remarks_model->get_all();

        $data = array(
            'audit_remarks_data' => $audit_remarks
        );

          $data['content'] = 'audit_remarks/audit_remarks_list';
        $this->load->view('common/master', $data);    
            
    }

    public function read($id) 
    {
        $row = $this->Audit_remarks_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'audit_id' => $row->audit_id,
		'agents_id' => $row->agents_id,
		'employee_id_TL' => $row->employee_id_TL,
		'employee_id_AM' => $row->employee_id_AM,
		'Call_Date' => $row->Call_Date,
		'Time_Of_Call' => $row->Time_Of_Call,
		'Calling_Number' => $row->Calling_Number,
		'CLI_Number' => $row->CLI_Number,
		'Call_Dur_Min' => $row->Call_Dur_Min,
		'Call_Dur_Sec' => $row->Call_Dur_Sec,
		'Call_Type' => $row->Call_Type,
		'Todays_Audit_Count' => $row->Todays_Audit_Count,
		'category_main_id' => $row->category_main_id,
		'category_sub_id' => $row->category_sub_id,
		'category_main_id_crr' => $row->category_main_id_crr,
		'category_sub_id_crr' => $row->category_sub_id_crr,
		'Consumers_Concern' => $row->Consumers_Concern,
		'r_g_y_a' => $row->r_g_y_a,
		'QME_Remarks' => $row->QME_Remarks,
		'p_s_if_a' => $row->p_s_if_a,
		'c_t_a_p_p' => $row->c_t_a_p_p,
		'Opening' => $row->Opening,
		'ActiveListening' => $row->ActiveListening,
		'Probing' => $row->Probing,
		'Customer_Engagement' => $row->Customer_Engagement,
		'Empathy_where_required' => $row->Empathy_where_required,
		'Understanding' => $row->Understanding,
		'Professionalism' => $row->Professionalism,
		'Politeness' => $row->Politeness,
		'Hold_Procedure' => $row->Hold_Procedure,
		'Closing' => $row->Closing,
		'Correct_smaller' => $row->Correct_smaller,
		'Accurate_Complete' => $row->Accurate_Complete,
		'Fatal_Reason' => $row->Fatal_Reason,
		'date' => $row->date,
		'status' => $row->status,
	    );
             $data['content'] = 'audit_remarks/audit_remarks_read';
        $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('audit_remarks'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('audit_remarks/create_action'),
	    'id' => set_value('id'),
	    'audit_id' => set_value('audit_id'),
	    'agents_id' => set_value('agents_id'),
	    'employee_id_TL' => set_value('employee_id_TL'),
	    'employee_id_AM' => set_value('employee_id_AM'),
	    'Call_Date' => set_value('Call_Date'),
	    'Time_Of_Call' => set_value('Time_Of_Call'),
	    'Calling_Number' => set_value('Calling_Number'),
	    'CLI_Number' => set_value('CLI_Number'),
	    'Call_Dur_Min' => set_value('Call_Dur_Min'),
	    'Call_Dur_Sec' => set_value('Call_Dur_Sec'),
	    'Call_Type' => set_value('Call_Type'),
	    'Todays_Audit_Count' => set_value('Todays_Audit_Count'),
	    'category_main_id' => set_value('category_main_id'),
	    'category_sub_id' => set_value('category_sub_id'),
	    'category_main_id_crr' => set_value('category_main_id_crr'),
	    'category_sub_id_crr' => set_value('category_sub_id_crr'),
	    'Consumers_Concern' => set_value('Consumers_Concern'),
	    'r_g_y_a' => set_value('r_g_y_a'),
	    'QME_Remarks' => set_value('QME_Remarks'),
	    'p_s_if_a' => set_value('p_s_if_a'),
	    'c_t_a_p_p' => set_value('c_t_a_p_p'),
	    'Opening' => set_value('Opening'),
	    'ActiveListening' => set_value('ActiveListening'),
	    'Probing' => set_value('Probing'),
	    'Customer_Engagement' => set_value('Customer_Engagement'),
	    'Empathy_where_required' => set_value('Empathy_where_required'),
	    'Understanding' => set_value('Understanding'),
	    'Professionalism' => set_value('Professionalism'),
	    'Politeness' => set_value('Politeness'),
	    'Hold_Procedure' => set_value('Hold_Procedure'),
	    'Closing' => set_value('Closing'),
	    'Correct_smaller' => set_value('Correct_smaller'),
	    'Accurate_Complete' => set_value('Accurate_Complete'),
	    'Fatal_Reason' => set_value('Fatal_Reason'),
	    'date' => set_value('date'),
	    'status' => set_value('status'),
	);
        $data['content'] = 'audit_remarks/audit_remarks_form';
        $this->load->view('common/master', $data);       
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'audit_id' => $this->input->post('audit_id',TRUE),
		'agents_id' => $this->input->post('agents_id',TRUE),
		'employee_id_TL' => $this->input->post('employee_id_TL',TRUE),
		'employee_id_AM' => $this->input->post('employee_id_AM',TRUE),
		'Call_Date' => $this->input->post('Call_Date',TRUE),
		'Time_Of_Call' => $this->input->post('Time_Of_Call',TRUE),
		'Calling_Number' => $this->input->post('Calling_Number',TRUE),
		'CLI_Number' => $this->input->post('CLI_Number',TRUE),
		'Call_Dur_Min' => $this->input->post('Call_Dur_Min',TRUE),
		'Call_Dur_Sec' => $this->input->post('Call_Dur_Sec',TRUE),
		'Call_Type' => $this->input->post('Call_Type',TRUE),
		'Todays_Audit_Count' => $this->input->post('Todays_Audit_Count',TRUE),
		'category_main_id' => $this->input->post('category_main_id',TRUE),
		'category_sub_id' => $this->input->post('category_sub_id',TRUE),
		'category_main_id_crr' => $this->input->post('category_main_id_crr',TRUE),
		'category_sub_id_crr' => $this->input->post('category_sub_id_crr',TRUE),
		'Consumers_Concern' => $this->input->post('Consumers_Concern',TRUE),
		'r_g_y_a' => $this->input->post('r_g_y_a',TRUE),
		'QME_Remarks' => $this->input->post('QME_Remarks',TRUE),
		'p_s_if_a' => $this->input->post('p_s_if_a',TRUE),
		'c_t_a_p_p' => $this->input->post('c_t_a_p_p',TRUE),
		'Opening' => $this->input->post('Opening',TRUE),
		'ActiveListening' => $this->input->post('ActiveListening',TRUE),
		'Probing' => $this->input->post('Probing',TRUE),
		'Customer_Engagement' => $this->input->post('Customer_Engagement',TRUE),
		'Empathy_where_required' => $this->input->post('Empathy_where_required',TRUE),
		'Understanding' => $this->input->post('Understanding',TRUE),
		'Professionalism' => $this->input->post('Professionalism',TRUE),
		'Politeness' => $this->input->post('Politeness',TRUE),
		'Hold_Procedure' => $this->input->post('Hold_Procedure',TRUE),
		'Closing' => $this->input->post('Closing',TRUE),
		'Correct_smaller' => $this->input->post('Correct_smaller',TRUE),
		'Accurate_Complete' => $this->input->post('Accurate_Complete',TRUE),
		'Fatal_Reason' => $this->input->post('Fatal_Reason',TRUE),
		'date' => $this->input->post('date',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Audit_remarks_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('audit_remarks'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Audit_remarks_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('audit_remarks/update_action'),
		'id' => set_value('id', $row->id),
		'audit_id' => set_value('audit_id', $row->audit_id),
		'agents_id' => set_value('agents_id', $row->agents_id),
		'employee_id_TL' => set_value('employee_id_TL', $row->employee_id_TL),
		'employee_id_AM' => set_value('employee_id_AM', $row->employee_id_AM),
		'Call_Date' => set_value('Call_Date', $row->Call_Date),
		'Time_Of_Call' => set_value('Time_Of_Call', $row->Time_Of_Call),
		'Calling_Number' => set_value('Calling_Number', $row->Calling_Number),
		'CLI_Number' => set_value('CLI_Number', $row->CLI_Number),
		'Call_Dur_Min' => set_value('Call_Dur_Min', $row->Call_Dur_Min),
		'Call_Dur_Sec' => set_value('Call_Dur_Sec', $row->Call_Dur_Sec),
		'Call_Type' => set_value('Call_Type', $row->Call_Type),
		'Todays_Audit_Count' => set_value('Todays_Audit_Count', $row->Todays_Audit_Count),
		'category_main_id' => set_value('category_main_id', $row->category_main_id),
		'category_sub_id' => set_value('category_sub_id', $row->category_sub_id),
		'category_main_id_crr' => set_value('category_main_id_crr', $row->category_main_id_crr),
		'category_sub_id_crr' => set_value('category_sub_id_crr', $row->category_sub_id_crr),
		'Consumers_Concern' => set_value('Consumers_Concern', $row->Consumers_Concern),
		'r_g_y_a' => set_value('r_g_y_a', $row->r_g_y_a),
		'QME_Remarks' => set_value('QME_Remarks', $row->QME_Remarks),
		'p_s_if_a' => set_value('p_s_if_a', $row->p_s_if_a),
		'c_t_a_p_p' => set_value('c_t_a_p_p', $row->c_t_a_p_p),
		'Opening' => set_value('Opening', $row->Opening),
		'ActiveListening' => set_value('ActiveListening', $row->ActiveListening),
		'Probing' => set_value('Probing', $row->Probing),
		'Customer_Engagement' => set_value('Customer_Engagement', $row->Customer_Engagement),
		'Empathy_where_required' => set_value('Empathy_where_required', $row->Empathy_where_required),
		'Understanding' => set_value('Understanding', $row->Understanding),
		'Professionalism' => set_value('Professionalism', $row->Professionalism),
		'Politeness' => set_value('Politeness', $row->Politeness),
		'Hold_Procedure' => set_value('Hold_Procedure', $row->Hold_Procedure),
		'Closing' => set_value('Closing', $row->Closing),
		'Correct_smaller' => set_value('Correct_smaller', $row->Correct_smaller),
		'Accurate_Complete' => set_value('Accurate_Complete', $row->Accurate_Complete),
		'Fatal_Reason' => set_value('Fatal_Reason', $row->Fatal_Reason),
		'date' => set_value('date', $row->date),
		'status' => set_value('status', $row->status),
	    );
            $data['content'] = 'audit_remarks/audit_remarks_form';
            $this->load->view('common/master', $data);       
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('audit_remarks'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'audit_id' => $this->input->post('audit_id',TRUE),
		'agents_id' => $this->input->post('agents_id',TRUE),
		'employee_id_TL' => $this->input->post('employee_id_TL',TRUE),
		'employee_id_AM' => $this->input->post('employee_id_AM',TRUE),
		'Call_Date' => $this->input->post('Call_Date',TRUE),
		'Time_Of_Call' => $this->input->post('Time_Of_Call',TRUE),
		'Calling_Number' => $this->input->post('Calling_Number',TRUE),
		'CLI_Number' => $this->input->post('CLI_Number',TRUE),
		'Call_Dur_Min' => $this->input->post('Call_Dur_Min',TRUE),
		'Call_Dur_Sec' => $this->input->post('Call_Dur_Sec',TRUE),
		'Call_Type' => $this->input->post('Call_Type',TRUE),
		'Todays_Audit_Count' => $this->input->post('Todays_Audit_Count',TRUE),
		'category_main_id' => $this->input->post('category_main_id',TRUE),
		'category_sub_id' => $this->input->post('category_sub_id',TRUE),
		'category_main_id_crr' => $this->input->post('category_main_id_crr',TRUE),
		'category_sub_id_crr' => $this->input->post('category_sub_id_crr',TRUE),
		'Consumers_Concern' => $this->input->post('Consumers_Concern',TRUE),
		'r_g_y_a' => $this->input->post('r_g_y_a',TRUE),
		'QME_Remarks' => $this->input->post('QME_Remarks',TRUE),
		'p_s_if_a' => $this->input->post('p_s_if_a',TRUE),
		'c_t_a_p_p' => $this->input->post('c_t_a_p_p',TRUE),
		'Opening' => $this->input->post('Opening',TRUE),
		'ActiveListening' => $this->input->post('ActiveListening',TRUE),
		'Probing' => $this->input->post('Probing',TRUE),
		'Customer_Engagement' => $this->input->post('Customer_Engagement',TRUE),
		'Empathy_where_required' => $this->input->post('Empathy_where_required',TRUE),
		'Understanding' => $this->input->post('Understanding',TRUE),
		'Professionalism' => $this->input->post('Professionalism',TRUE),
		'Politeness' => $this->input->post('Politeness',TRUE),
		'Hold_Procedure' => $this->input->post('Hold_Procedure',TRUE),
		'Closing' => $this->input->post('Closing',TRUE),
		'Correct_smaller' => $this->input->post('Correct_smaller',TRUE),
		'Accurate_Complete' => $this->input->post('Accurate_Complete',TRUE),
		'Fatal_Reason' => $this->input->post('Fatal_Reason',TRUE),
		'date' => $this->input->post('date',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

            $this->Audit_remarks_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('audit_remarks'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Audit_remarks_model->get_by_id($id);

        if ($row) {
            $this->Audit_remarks_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('audit_remarks'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('audit_remarks'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('audit_id', 'audit id', 'trim|required');
	$this->form_validation->set_rules('agents_id', 'agents id', 'trim|required');
	$this->form_validation->set_rules('employee_id_TL', 'employee id tl', 'trim|required');
	$this->form_validation->set_rules('employee_id_AM', 'employee id am', 'trim|required');
	$this->form_validation->set_rules('Call_Date', 'call date', 'trim|required');
	$this->form_validation->set_rules('Time_Of_Call', 'time of call', 'trim|required');
	$this->form_validation->set_rules('Calling_Number', 'calling number', 'trim|required');
	$this->form_validation->set_rules('CLI_Number', 'cli number', 'trim|required');
	$this->form_validation->set_rules('Call_Dur_Min', 'call dur min', 'trim|required');
	$this->form_validation->set_rules('Call_Dur_Sec', 'call dur sec', 'trim|required');
	$this->form_validation->set_rules('Call_Type', 'call type', 'trim|required');
	$this->form_validation->set_rules('Todays_Audit_Count', 'todays audit count', 'trim|required');
	$this->form_validation->set_rules('category_main_id', 'category main id', 'trim|required');
	$this->form_validation->set_rules('category_sub_id', 'category sub id', 'trim|required');
	$this->form_validation->set_rules('category_main_id_crr', 'category main id crr', 'trim|required');
	$this->form_validation->set_rules('category_sub_id_crr', 'category sub id crr', 'trim|required');
	$this->form_validation->set_rules('Consumers_Concern', 'consumers concern', 'trim|required');
	$this->form_validation->set_rules('r_g_y_a', 'r g y a', 'trim|required');
	$this->form_validation->set_rules('QME_Remarks', 'qme remarks', 'trim|required');
	$this->form_validation->set_rules('p_s_if_a', 'p s if a', 'trim|required');
	$this->form_validation->set_rules('c_t_a_p_p', 'c t a p p', 'trim|required');
	$this->form_validation->set_rules('Opening', 'opening', 'trim|required');
	$this->form_validation->set_rules('ActiveListening', 'activelistening', 'trim|required');
	$this->form_validation->set_rules('Probing', 'probing', 'trim|required');
	$this->form_validation->set_rules('Customer_Engagement', 'customer engagement', 'trim|required');
	$this->form_validation->set_rules('Empathy_where_required', 'empathy where required', 'trim|required');
	$this->form_validation->set_rules('Understanding', 'understanding', 'trim|required');
	$this->form_validation->set_rules('Professionalism', 'professionalism', 'trim|required');
	$this->form_validation->set_rules('Politeness', 'politeness', 'trim|required');
	$this->form_validation->set_rules('Hold_Procedure', 'hold procedure', 'trim|required');
	$this->form_validation->set_rules('Closing', 'closing', 'trim|required');
	$this->form_validation->set_rules('Correct_smaller', 'correct smaller', 'trim|required');
	$this->form_validation->set_rules('Accurate_Complete', 'accurate complete', 'trim|required');
	$this->form_validation->set_rules('Fatal_Reason', 'fatal reason', 'trim|required');
	$this->form_validation->set_rules('date', 'date', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "audit_remarks.xls";
        $judul = "audit_remarks";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Audit Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Agents Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Employee Id TL");
	xlsWriteLabel($tablehead, $kolomhead++, "Employee Id AM");
	xlsWriteLabel($tablehead, $kolomhead++, "Call Date");
	xlsWriteLabel($tablehead, $kolomhead++, "Time Of Call");
	xlsWriteLabel($tablehead, $kolomhead++, "Calling Number");
	xlsWriteLabel($tablehead, $kolomhead++, "CLI Number");
	xlsWriteLabel($tablehead, $kolomhead++, "Call Dur Min");
	xlsWriteLabel($tablehead, $kolomhead++, "Call Dur Sec");
	xlsWriteLabel($tablehead, $kolomhead++, "Call Type");
	xlsWriteLabel($tablehead, $kolomhead++, "Todays Audit Count");
	xlsWriteLabel($tablehead, $kolomhead++, "Category Main Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Id");
	xlsWriteLabel($tablehead, $kolomhead++, "Category Main Id Crr");
	xlsWriteLabel($tablehead, $kolomhead++, "Category Sub Id Crr");
	xlsWriteLabel($tablehead, $kolomhead++, "Consumers Concern");
	xlsWriteLabel($tablehead, $kolomhead++, "R G Y A");
	xlsWriteLabel($tablehead, $kolomhead++, "QME Remarks");
	xlsWriteLabel($tablehead, $kolomhead++, "P S If A");
	xlsWriteLabel($tablehead, $kolomhead++, "C T A P P");
	xlsWriteLabel($tablehead, $kolomhead++, "Opening");
	xlsWriteLabel($tablehead, $kolomhead++, "ActiveListening");
	xlsWriteLabel($tablehead, $kolomhead++, "Probing");
	xlsWriteLabel($tablehead, $kolomhead++, "Customer Engagement");
	xlsWriteLabel($tablehead, $kolomhead++, "Empathy Where Required");
	xlsWriteLabel($tablehead, $kolomhead++, "Understanding");
	xlsWriteLabel($tablehead, $kolomhead++, "Professionalism");
	xlsWriteLabel($tablehead, $kolomhead++, "Politeness");
	xlsWriteLabel($tablehead, $kolomhead++, "Hold Procedure");
	xlsWriteLabel($tablehead, $kolomhead++, "Closing");
	xlsWriteLabel($tablehead, $kolomhead++, "Correct Smaller");
	xlsWriteLabel($tablehead, $kolomhead++, "Accurate Complete");
	xlsWriteLabel($tablehead, $kolomhead++, "Fatal Reason");
	xlsWriteLabel($tablehead, $kolomhead++, "Date");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");

	foreach ($this->Audit_remarks_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->audit_id);
	    xlsWriteNumber($tablebody, $kolombody++, $data->agents_id);
	    xlsWriteNumber($tablebody, $kolombody++, $data->employee_id_TL);
	    xlsWriteNumber($tablebody, $kolombody++, $data->employee_id_AM);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Call_Date);
	    xlsWriteNumber($tablebody, $kolombody++, $data->Time_Of_Call);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Calling_Number);
	    xlsWriteLabel($tablebody, $kolombody++, $data->CLI_Number);
	    xlsWriteNumber($tablebody, $kolombody++, $data->Call_Dur_Min);
	    xlsWriteNumber($tablebody, $kolombody++, $data->Call_Dur_Sec);
	    xlsWriteNumber($tablebody, $kolombody++, $data->Call_Type);
	    xlsWriteNumber($tablebody, $kolombody++, $data->Todays_Audit_Count);
	    xlsWriteNumber($tablebody, $kolombody++, $data->category_main_id);
	    xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_id);
	    xlsWriteNumber($tablebody, $kolombody++, $data->category_main_id_crr);
	    xlsWriteNumber($tablebody, $kolombody++, $data->category_sub_id_crr);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Consumers_Concern);
	    xlsWriteLabel($tablebody, $kolombody++, $data->r_g_y_a);
	    xlsWriteLabel($tablebody, $kolombody++, $data->QME_Remarks);
	    xlsWriteLabel($tablebody, $kolombody++, $data->p_s_if_a);
	    xlsWriteLabel($tablebody, $kolombody++, $data->c_t_a_p_p);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Opening);
	    xlsWriteLabel($tablebody, $kolombody++, $data->ActiveListening);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Probing);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Customer_Engagement);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Empathy_where_required);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Understanding);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Professionalism);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Politeness);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Hold_Procedure);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Closing);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Correct_smaller);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Accurate_Complete);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Fatal_Reason);
	    xlsWriteLabel($tablebody, $kolombody++, $data->date);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Audit_remarks.php */
/* Location: ./application/controllers/Audit_remarks.php */
/* Please DO NOT modify this information : */
/* Generated on Codeigniter2019-08-06 08:47:35 */
