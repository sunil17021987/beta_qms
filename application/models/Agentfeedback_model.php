<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Agentfeedback_model extends CI_Model
{

    public $table = 'agentfeedback';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
     function get_by_audit_id($audit_id)
    {
         $this->db->select('Auditor,Accepted');
        $this->db->where('audit_id',$audit_id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
	$this->db->or_like('audit_id', $q);
	$this->db->or_like('Auditor', $q);
	$this->db->or_like('Accepted', $q);
	$this->db->or_like('date_of', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
	$this->db->or_like('audit_id', $q);
	$this->db->or_like('Auditor', $q);
	$this->db->or_like('Accepted', $q);
	$this->db->or_like('date_of', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
         $insert_id = $this->db->insert_id();
          return  $insert_id;
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

     function update_audit_id($id, $data)
    {
        $this->db->where('audit_id', $id);
        $this->db->update($this->table, $data);
        return $id;
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Agentfeedback_model.php */
/* Location: ./application/models/Agentfeedback_model.php */
/* Please DO NOT modify this information : */
/* Generated On Codeigniter2019-08-29 15:07:40 */
