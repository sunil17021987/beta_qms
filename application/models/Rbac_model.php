<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rbac_model extends CI_Model {

    public $table = 'rbac';
    public $id = 'id';
    public $order = 'DESC';

    function __construct() {
        parent::__construct();
    }

    // get all
    function get_all() {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    function get_all_join() {
        $query = $this->db->query("SELECT r.id ,e.emp_name,r.permission FROM `rbac` as r INNER JOIN employee as e ON e.emp_id= r.employee_id");

        if ($query->num_rows() > 0) {
            return $query->result();
        }
    }

    // get data by id
    function get_by_id($id) {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    function get_by_employee_id($employee_id) {
        $this->db->select('employee_id,permission');
        $this->db->where('employee_id', $employee_id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
        $this->db->or_like('employee_id', $q);
        $this->db->or_like('permission', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
        $this->db->or_like('employee_id', $q);
        $this->db->or_like('permission', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data) {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data) {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id) {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Rbac_model.php */
/* Location: ./application/models/Rbac_model.php */
/* Please DO NOT modify this information : */
/* Generated On Codeigniter2019-09-09 08:44:28 */
