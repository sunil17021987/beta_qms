<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Process_log_model extends CI_Model
{

    public $table = 'process_log';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    function get_all_by_join()
    {
    $query="
        SELECT p.id AS id,p.name AS name,p.user_id AS user_id,p.password AS password,p.logo AS logo,u.name AS user_type,
        CASE
        WHEN p.status='1' THEN 'Active'
        WHEN p.status='0' THEN 'InActive'
        ELSE 'No'
        END  AS status
        FROM process_log AS p
        INNER JOIN user_type AS u ON u.id=p.user_type
        WHERE user_type IN(1,2)
            ";
            
         $query = $this->db->query($query);
        $rows = $query->result();
        return $rows;
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
     function get_all_process()
    {
        $this->db->where('user_type', 2);
        return $this->db->get($this->table)->result();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('id', $q);
	$this->db->or_like('name', $q);
	$this->db->or_like('user_id', $q);
	$this->db->or_like('password', $q);
	$this->db->or_like('logo', $q);
	$this->db->or_like('user_type', $q);
	$this->db->or_like('status', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('id', $q);
	$this->db->or_like('name', $q);
	$this->db->or_like('user_id', $q);
	$this->db->or_like('password', $q);
	$this->db->or_like('logo', $q);
	$this->db->or_like('user_type', $q);
	$this->db->or_like('status', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Process_log_model.php */
/* Location: ./application/models/Process_log_model.php */
/* Please DO NOT modify this information : */
/* Generated On Codeigniter2020-06-04 11:55:08 */
