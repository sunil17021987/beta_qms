<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Dashborad_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
    //   count all student for total registeration
//    public function count_all_student() {
//        $this->db->select('count(*)');
//        $query = $this->db->get('registration');
//        $cnt = $query->row_array();
//        return $cnt['count(*)'];
//    }
    //   count all student for total registeration



    //   count fees collection for student

    public function fees_collection() {
        $date= date("d/m/Y");
        $this->db->select('paid_amount');
        $this->db->where('created_at',$date);
        $query = $this->db->get('fees_collect');
        $cnt = $query->row_array();
        return $cnt;
        //return $cnt['count(*)'];
    }
    //end fees collection for student

//    public function get_all_dashboard_stats() {
//        $staffid=$this->session->userdata('reg_id');
//        $usertype=$this->session->userdata('type');
//        $sql = "select 
//        (SELECT count(id) FROM registration) as registrations,
//        (SELECT count(emp_id) FROM employee) as employees,
//        (SELECT count(id) FROM demo) as enquiries,
//        (SELECT sum(paid_amount) FROM fees_collect) as feescollected,
//        (SELECT count(inventory_id) FROM kit_inventory) as inventory,
//        (SELECT count(id) FROM time_slot) as schedules,
//        (SELECT count(weapon_id) FROM weapons) as weapons,
//        (SELECT count(task_id) FROM task where case when ".$usertype."=1 then 1=1 else staff_id in (".$staffid.") end) as tasks";
//
//        $query = $this->db->query($sql);
//        $row = $query->row(); 
//        //echo $this->db->last_query();
//        return $row;
//        //return $cnt['count(*)'];
//    }

    // count all enquiry
    public function count_all_enquiry() {
        $date= date("d/m/Y");
        $this->db->select('count(*)');
        $this->db->where('created_at',$date);
        $query = $this->db->get('demo');
        $cnt = $query->row_array();
        return $cnt['count(*)'];
    }
    //end count all enquiry


    // count all employee
    public function count_all_employee() {
        $this->db->select('count(*)');
        $query = $this->db->get('employee');
        $cnt = $query->row_array();
        return $cnt['count(*)'];
    }
    //end count all emplyee
    
     public function get_all_dashboard_stats() {
        $staffid=$this->session->userdata('reg_id');
        $usertype=$this->session->userdata('type');
        $sql = "select 
       
        (SELECT count(emp_id) FROM employee) as employees
       
       ";

        $query = $this->db->query($sql);
        $row = $query->row(); 
        //echo $this->db->last_query();
        return $row;
        //return $cnt['count(*)'];
    }

}
?>
